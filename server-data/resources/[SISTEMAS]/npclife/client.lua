-------------------------------------------------------------------------------------------------
---------------------------------- DON'T EDIT THESES LINES --------------------------------------
-------------------------------------------------------------------------------------------------

local generalLoaded = false
local PlayingAnim = false

-------------------------------------------------------------------------------------------------
----------------------------------- YOU CAN EDIT THIS LINES -------------------------------------
-------------------------------------------------------------------------------------------------

local ShopClerk = {
  -- ID: id of NPC | name: Name of Blip | BlipID: Icone of Blip | VoiceName: NPC Talk When near it | Ambiance: Ambiance of Shop | Weapon: Hash of Weapon | modelHash: Model | X: Position x | Y: Position Y | Z: Position Z | heading: Where Npc look
	{id = 1, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "mp_m_shopkeep_01", x = -2511.16479492188, y = 3616.90478515625, z = 13.6422147750854, heading = 245.000457763672}, 
	{id = 2, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "mp_m_shopkeep_01", x = 24.392505645752, y = -1345.41369628906, z = 29.4970207214355, heading = 264.900115966797},
	{id = 3, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "mp_m_shopkeep_01", x = -47.3110542297363, y = -1758.62475585938, z = 29.4209995269775, heading = 48.1558074951172},
	{id = 4, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = 841.843566894531, y = -1035.70556640625, z = 28.1948642730713, heading = 3.31448912620544},
	-- npc para pegar cartões clonáveis
	{id = 5, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "g_m_m_armboss_01", x = 261.61892700195, y = 204.29025268555, z = 110.28720855713, heading = 3.31448912620544},
	-- npc para clonar cartões
	{id = 6, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "g_m_m_armgoon_01", x = -1054.0205078125, y = -230.26893615723, z = 44.020957946777, heading = 220.000457763672},
	-- npc para vender cartões clonados
	{id = 7, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "a_m_y_vindouche_01", x = -621.35168457031, y = 36.309566497803, z = 43.566032409668, heading = 245.000457763672},
    -- npc para lavagem de dinheiro
	{id = 8, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "ig_lazlow", x = -1056.4289550781, y = -242.60389709473, z = 44.021060943604, heading = 3.31448912620544},
	-- npc processamento de maconha
	{id = 9, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "ig_claypain", x = -785.97308349609, y = 580.78576660156, z = 126.80484771729, heading = 48.1558074951172},
	-- npc processamento de cocaina
	{id = 10, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "g_m_y_famfor_01", x = 473.24905395508, y = -1686.6027832031, z = 29.381669998169, heading = 48.1558074951172},
	-- npc processamento de metanfetamina
	{id = 11, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0x5EF9FEC4, modelHash = "ig_rashcosvki", x = 1208.3706054688, y = -3116.1745605469, z = 5.5403265953064, heading = 48.1558074951172},
	
	-- MINERAÇÃO DIAMANTE:
	-- npc minerar diamante
	{id = 12, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "a_m_o_acult_02", x = -593.01672363281, y = 2091.3586425781, z = 132.28964233398, heading = 48.1558074951172},
	-- npc processar diamante
	{id = 13, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "a_m_m_farmer_01", x = 864.07440185547, y = 2133.8083496094, z = 52.206802368164, heading = 264.900115966797},
	-- npc venda diamante
	{id = 14, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "a_m_m_hasjew_01", x = 2717.4016113281, y = 1496.3310546875, z = 24.500703811646, heading = 48.1558074951172},
	
	-- MINERAÇÃO OURO:
	-- npc minerar ouro
	{id = 15, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "a_m_o_acult_02", x = 2936.3679199219, y = 2743.2658691406, z = 43.681480407715, heading = 48.1558074951172},
	-- npc processar ouro
	{id = 16, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "a_m_m_farmer_01", x = 3334.580078125, y = 5162.70703125, z = 18.316473007202, heading = 48.1558074951172},
	-- npc vender ouro
	{id = 17, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "a_m_m_mexcntry_01", x = -1143.8641357422, y = 4909.201171875, z = 220.96876525879, heading = 48.1558074951172},
	
	-- MINERAÇÃO OURO:
	-- npc minerar cobre
	{id = 18, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "a_m_o_acult_02", x = -900.81585693359, y = 4865.6313476563, z = 287.74951171875, heading = 48.1558074951172},
	-- npc processar cobre
	{id = 19, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "a_m_m_farmer_01", x = 814.57116699219, y = 1176.7609863281, z = 330.70367431641, heading = 3.31448912620544},
	-- npc vender cobre
	{id = 20, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "a_m_m_mexcntry_01", x = 2731.8955078125, y = 1564.7276611328, z = 24.500968933105, heading = 48.1558074951172},
	
	-- npc pescador
	{id = 21, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "s_m_m_trucker_01", x = 1510.8807373047, y = 3911.8522949219, z = 30.624593734741, heading = 264.900115966797},
	
	-- peixes
	{id = 22, modelHash = "a_c_fish", x = 743.19586181641, y = 3895.3967285156, z = 30.5, heading = 48.1558074951172},
	{id = 23, modelHash = "a_c_fish", x = 740.42614746094, y = 3895.1125488281, z = 29.396677017212, heading = 48.1558074951172},
	{id = 24, modelHash = "a_c_fish", x = 740.75592041016, y = 3898.4409179688, z = 29.366970062256, heading = 48.1558074951172},
	{id = 25, modelHash = "a_c_fish", x = 744.0576171875, y = 3898.2326660156, z = 29.636159896851, heading = 48.1558074951172},
	{id = 26, modelHash = "a_c_fish", x = 745.40576171875, y = 3896.0083007813, z = 29.785148620605, heading = 48.1558074951172},
	{id = 27, modelHash = "a_c_fish", x = 740.59533691406, y = 3899.8366699219, z = 29.693857192993, heading = 48.1558074951172},
	-- npcs ammunation2
	{id = 28, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = -662.46533203125, y = -933.16906738281, z = 21.829219818115, heading = 170.900115966797},
	{id = 29, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = -1305.8991699219, y = -396.64086914063, z = 36.718090057373, heading = 340.900115966797},
	{id = 30, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = 250.90794372559, y = -45.527194976807, z = 69.941116333008, heading = 170.900115966797},
	{id = 31, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = -3169.3493652344, y = 1089.5281982422, z = 20.838729858398, heading = 170.900115966797},
	{id = 32, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = -1116.6945800781, y = 2691.4897460938, z = 18.554132461548, heading = 320.900115966797},
	{id = 33, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = -334.5888671875, y = 6081.7709960938, z = 31.45477104187, heading = 310.900115966797},
	{id = 34, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = 1700.9011230469, y = 3756.3525390625, z = 34.705341339111, heading = 130.900115966797},
	{id = 35, VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = 816.65783691406, y = -2159.60546875, z = 29.619007110596, heading = 30.900115966797},
	-- npc venda tartaruga
	{id = 36, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "s_m_m_trucker_01", x = -119.17678833008, y = -1486.1040039063, z = 36.98205947876, heading = 30.900115966797},
	-- npc venda peixes
	{id = 37, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "s_m_m_trucker_01", x = -378.66812133789, y = 6034.96875, z = 31.526931762695, heading = 140.900115966797},
	-- npc entrega pacotes
	{id = 38, VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0xDD5DF8D9, modelHash = "s_m_m_trucker_01", x = -274.54306030273, y = 6146.1020507813, z = 31.51989364624, heading = 140.900115966797},
}

-------------------------------------------------------------------------------------------------
---------------------------------- DON'T EDIT THESES LINES --------------------------------------
-------------------------------------------------------------------------------------------------

-- Blip For NPC
Citizen.CreateThread(function()
	for k,v in pairs(ShopClerk)do
		local blip = AddBlipForCoord(v.x, v.y, v.z)
		SetBlipSprite(blip, v.BlipID)
		SetBlipScale(blip, 0.8)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString(v.name)
		EndTextCommandSetBlipName(blip)
	end
end)

-------------------------------------------------------------------------------------------------
---------------------------------- DON'T EDIT THESES LINES --------------------------------------
-------------------------------------------------------------------------------------------------

-- Spawn NPC
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
	
	if (not generalLoaded) then
	  
	  for i=1, #ShopClerk do
        RequestModel(GetHashKey(ShopClerk[i].modelHash))
        while not HasModelLoaded(GetHashKey(ShopClerk[i].modelHash)) do
          Wait(1)
        end
		
        ShopClerk[i].id = CreatePed(2, ShopClerk[i].modelHash, ShopClerk[i].x, ShopClerk[i].y, ShopClerk[i].z, ShopClerk[i].heading, true, true)
        SetPedFleeAttributes(ShopClerk[i].id, 0, 0)
		SetAmbientVoiceName(ShopClerk[i].id, ShopClerk[i].Ambiance)
		SetPedDropsWeaponsWhenDead(ShopClerk[i].id, false)
		SetPedDiesWhenInjured(ShopClerk[i].id, false)
		GiveWeaponToPed(ShopClerk[i].id, ShopClerk[i].Weapon, 2800, false, true)
		
      end
      generalLoaded = true
		
    end
	
  end
end)

-------------------------------------------------------------------------------------------------
---------------------------------- DON'T EDIT THESES LINES --------------------------------------
-------------------------------------------------------------------------------------------------

-- Action when player Near NPC (or not)
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
		RequestAnimDict("random@shop_gunstore")
		while (not HasAnimDictLoaded("random@shop_gunstore")) do 
			Citizen.Wait(0) 
		end
		
		for i=1, #ShopClerk do
			distance = GetDistanceBetweenCoords(ShopClerk[i].x, ShopClerk[i].y, ShopClerk[i].z, GetEntityCoords(GetPlayerPed(-1)))
			if distance < 4 and PlayingAnim ~= true then
				TaskPlayAnim(ShopClerk[i].id,"random@shop_gunstore","_greeting", 1.0, -1.0, 4000, 0, 1, true, true, true)
				PlayAmbientSpeech1(ShopClerk[i].id, ShopClerk[i].VoiceName, "SPEECH_PARAMS_FORCE", 1)
				PlayingAnim = true
				Citizen.Wait(4000)
				if PlayingAnim == true then
					TaskPlayAnim(ShopClerk[i].id,"random@shop_gunstore","_idle_b", 1.0, -1.0, -1, 0, 1, true, true, true)
					Citizen.Wait(40000)
				end
			else
				--don't touch this
				--TaskPlayAnim(ShopClerk[i].id,"random@shop_gunstore","_idle", 1.0, -1.0, -1, 0, 1, true, true, true)
			end
			if distance > 5.5 and distance < 6 then
				PlayingAnim = false
			end


		end
  end
end)