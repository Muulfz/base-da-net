--[[
~r~ = Red
~b~ = Blue
~g~ = Green
~y~ = Yellow
~p~ = Purple
~o~ = Orange
~c~ = Grey?
~m~ = Darker Grey
~u~ = Black
~n~ = New Line
~s~ = Default White
~h~ = Bold Text
]]


local garagem1 = {}
garagem1[0] = {["x"] =215.124,["y"]=-791.377,["z"]=30.646}
local garagem2 = {}
garagem2[1] = {["x"] =-334.685,["y"]=289.773,["z"]=85.705}
local garagem3 = {}
garagem3[2] = {["x"] =-55.272,["y"]=-1838.71,["z"]=26.442}
local garagem4 = {}
garagem4[3] = {["x"] =126.434,["y"]=6610.04,["z"]=31.750}
local garagem5 = {}
garagem5[4] = {["x"] =-2142.8828125,["y"]=-437.17086791992,["z"]=14.059371948242}
local garagem6 = {}
garagem6[5] = {["x"] =-2147.9475097656,["y"]=-434.65875244141,["z"]=14.06137752533}
local garagem7 = {}
garagem7[6] = {["x"] =-2151.2485351563,["y"]=-430.37326049805,["z"]=14.060933113098}
local garagem8 = {}
garagem8[7] = {["x"] =-2155.5234375,["y"]=-427.50015258789,["z"]=14.062129020691}
local garagem9 = {}
garagem9[8] = {["x"] =-2117.478515625,["y"]=-414.0192565918,["z"]=14.025656700134}
local garagem10 = {}
garagem10[9] = {["x"] =-2120.8837890625,["y"]=-410.30358886719,["z"]=14.025660514832}
local garagem11 = {}
garagem11[10] = {["x"] =-2124.2880859375,["y"]=-406.22689819336,["z"]=14.025433540344}
local garagem12 = {}
garagem12[11] = {["x"] =-2127.9787597656,["y"]=-402.5966796875,["z"]=14.02569770813}
local mecanicoe = {}
mecanicoe[0] = {["x"] =472.08847045898,["y"]=-1310.7618408203,["z"]=29.218572616577}
local gamec = {}
gamec [0] ={["x"] = 491.99514770508,["y"]=-1333.4526367188,["z"]=29.331750869751}
local loja0 = {} 
loja0 [0] ={["x"]=1960.50793457031,["y"]=3741.84008789063,["z"]=32.3437385559082}
local loja1 = {} 
loja1 [1] ={["x"]=1166.18151855469,["y"]=2709.35327148438,["z"]=38.15771484375} 
local loja2 = {} 
loja2 [2] ={["x"]=547.987609863281,["y"]=2669.7568359375,["z"]=42.1565132141113}
local loja3 = {} 
loja3 [3] ={["x"]=1698.30737304688,["y"]=4924.37939453125,["z"]=42.0636749267578}
local loja4 = {} 
loja4 [4] ={["x"]=1729.54443359375,["y"]=6415.76513671875,["z"]=35.0372200012207}
local loja5 = {} 
loja5 [5] ={["x"]=-3243.9013671875,["y"]=1001.40405273438,["z"]=12.8307056427002}
local loja6 = {} 
loja6 [6] ={["x"]=-2967.8818359375,["y"]=390.78662109375,["z"]=15.0433149337769}
local loja7 = {} 
loja7 [7] ={["x"]=-3041.17456054688,["y"]=585.166198730469,["z"]=7.90893363952637}
local loja8 = {} 
loja8 [8] ={["x"]=-1820.55725097656,["y"]=792.770568847656,["z"]=138.113250732422}
local loja9 = {} 
loja9 [9] ={["x"]=-1486.76574707031,["y"]=-379.553985595703,["z"]=40.163387298584}
local loja10 = {} 
loja10 [10] ={["x"]=-1223.18127441406,["y"]=-907.385681152344,["z"]=12.3263463973999}
local loja11 = {} 
loja11 [11] ={["x"]=-47.522762298584,["y"]=-1756.85717773438,["z"]=29.4210109710693}
local loja12 = {} 
loja12 [12] ={["x"]=25.7454013824463,["y"]=-1345.26232910156,["z"]=29.4970207214355} 
local loja13 = {} 
loja13 [13] ={["x"]=1135.57678222656,["y"]=-981.78125,["z"]=46.4157981872559} 
local loja14 = {} 
loja14 [14] ={["x"]=1163.53820800781,["y"]=-323.541320800781,["z"]=69.2050552368164} 
local loja15 = {} 
loja15 [15] ={["x"]=374.190032958984,["y"]=327.506713867188,["z"]=103.566368103027} 
local loja16 = {} 
loja16 [16] ={["x"]=2555.35766601563,["y"]=382.16845703125,["z"]=108.622947692871}
local loja17 = {} 
loja17 [17] ={["x"]=2676.76733398438,["y"]=3281.57788085938,["z"]=55.2411231994629}
local loja18 = {} 
loja18 [18] ={["x"]=-707.408996582031,["y"]=-913.681701660156,["z"]=19.2155857086182}
local lojama0 = {}
lojama0 [0] ={["x"]=841.1220703125,["y"]=-1031.0849609375,["z"]=28.194864273071}
local lojama1 = {}
lojama1 [1] ={["x"]=22.106534957886,["y"]=-1110.1981201172,["z"]=29.797025680542}
local lojama2 = {}
lojama2 [2] ={["x"]=-660.62573242188,["y"]=-937.91595458984,["z"]=21.829217910767}
local lojama3 = {}  
lojama3 [3] ={["x"]=-1308.5821533203,["y"]=-395.20498657227,["z"]=36.695774078369}
local lojama4 = {}  
lojama4 [4] ={["x"]=249.24723815918,["y"]=-50.527931213379,["z"]=69.941055297852}
local lojama5 = {}  
lojama5 [5] ={["x"]=-3169.0581054688,["y"]=1087.947265625,["z"]=20.838748931885}
local lojama6 = {}  
lojama6 [6] ={["x"]=-1115.0887451172,["y"]=2697.7473144531,["z"]=18.554132461548}
local lojama7 = {}  
lojama7 [7] ={["x"]=-327.87289428711,["y"]=6083.1767578125,["z"]=31.45477104187}
local lojama8 = {} 
lojama8 [8] ={["x"]=1696.3393554688,["y"]=3759.3461914063,["z"]=34.705318450928}
local lojama9 = {}  
lojama9 [9] ={["x"]=808.87762451172,["y"]=-2155.171875,["z"]=29.61901473999}

local lojabike = {}
lojabike [0] ={["x"]=-1032.7554931641,["y"]=-2730.7731933594,["z"]=20.076156616211}

local hospital = {}  
hospital [0] ={["x"]=294.63272094727,["y"]=-1448.5067138672,["z"]=29.966617584229}

local legal = {}  
legal [0] ={["x"]=-267.92422485352,["y"]=-957.95263671875,["z"]=31.22313117981}

local ilegal = {}  
ilegal [0] ={["x"]=707.02239990234,["y"]=-966.8125,["z"]=30.41284942627}

Citizen.CreateThread(function ()
    while true do
        Citizen.Wait(0)
            DrawText3DTag(garagem1[0]["x"], garagem1[0]["y"], garagem1[0]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem2[1]["x"], garagem2[1]["y"], garagem2[1]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem3[2]["x"], garagem3[2]["y"], garagem3[2]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem4[3]["x"], garagem4[3]["y"], garagem4[3]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem5[4]["x"], garagem5[4]["y"], garagem5[4]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem6[5]["x"], garagem6[5]["y"], garagem6[5]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem7[6]["x"], garagem7[6]["y"], garagem7[6]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem8[7]["x"], garagem8[7]["y"], garagem8[7]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem9[8]["x"], garagem9[8]["y"], garagem9[8]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem10[9]["x"], garagem10[9]["y"], garagem10[9]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem11[10]["x"], garagem11[10]["y"], garagem11[10]["z"], "~b~Garagem ~s~Publica")
			DrawText3DTag(garagem12[11]["x"], garagem12[11]["y"], garagem12[11]["z"], "~b~Garagem ~s~Publica")
            DrawText3DTag(mecanicoe[0]["x"], mecanicoe[0]["y"], mecanicoe[0]["z"] , "~b~Equipamentos  ~r~Mecanico")
			DrawText3DTag(gamec[0]["x"], gamec[0]["y"], gamec[0]["z"] , "~b~Garagem  ~r~Mecanico")
			DrawText3DTag(loja0[0]["x"], loja0[0]["y"], loja0[0]["z"] , "~g~Mercado")
			DrawText3DTag(loja1[1]["x"], loja1[1]["y"], loja1[1]["z"] , "~g~Mercado")
			DrawText3DTag(loja2[2]["x"], loja2[2]["y"], loja2[2]["z"] , "~g~Mercado")
			DrawText3DTag(loja3[3]["x"], loja3[3]["y"], loja3[3]["z"] , "~g~Mercado")
			DrawText3DTag(loja4[4]["x"], loja4[4]["y"], loja4[4]["z"] , "~g~Mercado")
			DrawText3DTag(loja5[5]["x"], loja5[5]["y"], loja5[5]["z"] , "~g~Mercado")
			DrawText3DTag(loja6[6]["x"], loja6[6]["y"], loja6[6]["z"] , "~g~Mercado")
			DrawText3DTag(loja7[7]["x"], loja7[7]["y"], loja7[7]["z"] , "~g~Mercado")
			DrawText3DTag(loja8[8]["x"], loja8[8]["y"], loja8[8]["z"] , "~g~Mercado")
			DrawText3DTag(loja9[9]["x"], loja9[9]["y"], loja9[9]["z"] , "~g~Mercado")
			DrawText3DTag(loja10[10]["x"], loja10[10]["y"], loja10[10]["z"] , "~g~Mercado")
			DrawText3DTag(loja11[11]["x"], loja11[11]["y"], loja11[11]["z"] , "~g~Mercado")
			DrawText3DTag(loja12[12]["x"], loja12[12]["y"], loja12[12]["z"] , "~g~Mercado")
			DrawText3DTag(loja13[13]["x"], loja13[13]["y"], loja13[13]["z"] , "~g~Mercado")
			DrawText3DTag(loja14[14]["x"], loja14[14]["y"], loja14[14]["z"] , "~g~Mercado")
			DrawText3DTag(loja15[15]["x"], loja15[15]["y"], loja15[15]["z"] , "~g~Mercado")
			DrawText3DTag(loja16[16]["x"], loja16[16]["y"], loja16[16]["z"] , "~g~Mercado")
			DrawText3DTag(loja17[17]["x"], loja17[17]["y"], loja17[17]["z"] , "~g~Mercado")
			DrawText3DTag(loja18[18]["x"], loja18[18]["y"], loja18[18]["z"] , "~g~Mercado")
			DrawText3DTag(lojama0[0]["x"], lojama0[0]["y"], lojama0[0]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojama1[1]["x"], lojama1[1]["y"], lojama1[1]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojama2[2]["x"], lojama2[2]["y"], lojama2[2]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojama3[3]["x"], lojama3[3]["y"], lojama3[3]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojama4[4]["x"], lojama4[4]["y"], lojama4[4]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojama5[5]["x"], lojama5[5]["y"], lojama5[5]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojama6[6]["x"], lojama6[6]["y"], lojama6[6]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojama7[7]["x"], lojama7[7]["y"], lojama7[7]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojama8[8]["x"], lojama8[8]["y"], lojama8[8]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojama9[9]["x"], lojama9[9]["y"], lojama9[9]["z"] , "~g~Loja de Materiais")
			DrawText3DTag(lojabike[0]["x"], lojabike[0]["y"], lojabike[0]["z"] , "~b~Aluguel ~s~Bikes")
			DrawText3DTag(hospital[0]["x"], hospital[0]["y"], hospital[0]["z"], "~r~Hospital  ~s~Central")
			DrawText3DTag(legal[0]["x"], legal[0]["y"], legal[0]["z"], "~b~Agencia ~s~de ~b~Empregos")
			DrawText3DTag(ilegal[0]["x"], ilegal[0]["y"], ilegal[0]["z"], "~r~Empregos  Ilegais")
			
			
			
    end
end)

function DrawText3DTag(x,y,z, text) 
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)

    local scale = (1/dist)*1
    local fov = (1/GetGameplayCamFov())*100
    local scale = scale*fov
    
    if onScreen then
        SetTextScale(0.7*scale, 1.2*scale)
        SetTextFont(2)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, 255)
        SetTextDropshadow(0, 0, 0, 0, 255)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        World3dToScreen2d(x,y,z, 0)
        DrawText(_x,_y)
    end
end