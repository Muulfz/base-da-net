client_scripts {
	'@vrp/lib/utils.lua',
	'map.lua',
	'client.lua',
	'GUI.lua',
	'models_c.lua'
}

server_scripts {
    '@vrp/lib/utils.lua',
    'server.lua'
}