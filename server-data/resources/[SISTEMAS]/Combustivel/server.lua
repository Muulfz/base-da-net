-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIÁVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Zezao = {}
Tunnel.bindInterface("Combustivel",Zezao)

local players = {}
local ArrayGasolina = {}
local PrecoBombas = {}

RegisterServerEvent("Combustivel:AdicionarCidadao")
AddEventHandler("Combustivel:AdicionarCidadao", function()
	local _source = source
	TriggerClientEvent("Combustivel:EnviarPreco", source, PrecoBombas)
	table.insert(players,_source)
end)

AddEventHandler("playerDropped", function(reason)
	local _source = source
	local newPlayers = {}
	for _,k in pairs(players) do
		if(k~=_source) then
			table.insert(newPlayers, k)
		end
	end
	players = {}
	players = newPlayers
end)

RegisterServerEvent("Combustivel:JogadorNasceu")
AddEventHandler("Combustivel:JogadorNasceu", function()
	local _source = source
	SetTimeout(2000, function()
		TriggerClientEvent("Combustivel:EnviarPreco", _source, PrecoBombas)
		TriggerClientEvent("Combustivel:EnviarCombustivel", _source, ArrayGasolina)
	end)
end)

RegisterServerEvent("Combustivel:TodosOsJogadores")
AddEventHandler("Combustivel:TodosOsJogadores", function(gasolina, _placa, _modelo)
	local _source = source
	local bool, ind = ProcurarModeloPlaca(_placa, _modelo)
	if(bool and ind ~= nil) then
		ArrayGasolina[ind].es = gasolina
	else
		if(_placa ~= nil and _modelo ~= nil and gasolina ~= nil) then
			table.insert(ArrayGasolina,{plate=_placa,model=_modelo,es=gasolina})
		end
	end
	TriggerClientEvent('Combustivel:SincronizarPlayers', -1, gasolina, _placa, _modelo)
end)

RegisterServerEvent("Combustivel:Comprar")
AddEventHandler("Combustivel:Comprar", function(valor, index,e)
	local _source = source
	local preco = PrecoBombas[index]
	if(e) then
		preco = index
	end
	local ValorTotal = round(valor*preco,2)
	local user_id = vRP.getUserId(_source)
	if(vRP.tryPayment(user_id,ValorTotal)) then
		TriggerClientEvent("Combustivel:EstaComprando", _source, valor)
	else
		TriggerClientEvent("pNotify:SendNotification", _source, {
			text = "Você não possui dinheiro suficiente",
			type = "error",progressBar = false,timeout = 3000,layout = "bpa",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end)

RegisterServerEvent("Combustivel:PedirPreco")
AddEventHandler("Combustivel:PedirPreco",function()
	TriggerClientEvent("Combustivel:EnviarPreco", source, PrecoBombas)
end)

RegisterServerEvent("vehicule:PegarCombustivel")
AddEventHandler("vehicule:PegarCombustivel", function(plate,model)
	local _source = source
	local bool, ind = ProcurarModeloPlaca(plate, model)
	if(bool) then
		TriggerClientEvent("vehicule:EnviarCombustivel", _source, 1, ArrayGasolina[ind].es)
	else
		TriggerClientEvent("vehicule:EnviarCombustivel", _source, 0, 0)
	end
end)

RegisterServerEvent("Combustivel:GasolinaServer")
AddEventHandler("Combustivel:GasolinaServer", function(percent, _placa, _modelo)
	local bool, ind = ProcurarModeloPlaca(_placa, _modelo)

	local percentToEs = (percent/100)*0.142

	if(bool) then
		ArrayGasolina[ind].es = percentToEs
	else
		table.insert(ArrayGasolina,{plate=_placa,model=_modelo,es=percentToEs})
	end
end)

RegisterServerEvent("Combustivel:ComprarGalao")
AddEventHandler("Combustivel:ComprarGalao", function()
	local _source = source
	local ValorTotal = math.random(100,500)
	local user_id = vRP.getUserId(_source)
	if(vRP.tryPayment(user_id,ValorTotal)) then
		TriggerClientEvent("Combustivel:ComprarGalao", _source)
		TriggerClientEvent("pNotify:SendNotification", _source, {
			text = "Você pagou ".. ValorTotal .. " reais pelo galão de gasolina",
			type = "success",progressBar = false,timeout = 3000,layout = "bpa",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	else
		TriggerClientEvent("pNotify:SendNotification", _source, {
			text = "Você não possui dinheiro suficiente",
			type = "error",progressBar = false,timeout = 3000,layout = "bpa",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end)

function round(num, dec)
  local mult = 10^(dec or 0)
  return math.floor(num * mult + 0.5) / mult
end

function MostrarPrecos()
    for i=0,34 do
        PrecoBombas[i] = round(math.random(),2) + math.random(2,3) + .09
    end
end
MostrarPrecos()

function ProcurarModeloPlaca(plate, model)
	for i,k in pairs(ArrayGasolina) do
		if(k.plate == plate and k.model == model) then
			return true, i
		end
	end
	return false, -1
end