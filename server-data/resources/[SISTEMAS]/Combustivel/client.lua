local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Zezaoserver = Tunnel.getInterface("zezao")

gasolina = 0.142
local quantidade = 0
local UltimoModelo = 0
local VeiculosUsados = {}
local GaloesAtuais = 0

Citizen.CreateThread(function()
	TriggerServerEvent("Combustivel:AdicionarCidadao")
	while true do
		Citizen.Wait(0)
		ChecarVeiculo()
		renderBoxes()
		if(GaloesAtuais > 0) then
			local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
			local veh = GetClosestVehicle(x, y, z, 4.001, 0, 70)
			if(veh ~= nil and GetVehicleNumberPlateText(veh) ~= nil) then
				local nul, numero = GetCurrentPedWeapon(GetPlayerPed(-1))
				if(numero == 883325847) then
					Info("Pressione ~INPUT_CONTEXT~ para abastecer")
					if(IsControlJustPressed(1, 38)) then
						local Porcentagem = gasolina/0.142

						RequestAnimDict("weapon@w_sp_jerrycan")
							while not HasAnimDictLoaded("weapon@w_sp_jerrycan") do
								Citizen.Wait(100)
							end
							TaskPlayAnim(GetPlayerPed(-1),"weapon@w_sp_jerrycan","fire", 8.0, -8, -1, 49, 0, -1, -1, -1)
							SetEntityCollision(GetPlayerPed(-1),true,true)
						    FreezeEntityPosition(GetPlayerPed(-1),false)
							exports.pNotify:SendNotification({
						    text = "Aguarde enquanto seu carro é abastecido",
						    type = "info",timeout = 3000,progressBar = false, layout = "bpa",queue = "left",
						    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
							})
							Wait(7000)
							ClearPedTasks(GetPlayerPed(-1))
							exports.pNotify:SendNotification({
						    	text = "Carro reabastecido",
						    	type = "success",timeout = 3000,progressBar = false, layout = "bpa",queue = "left",
						    	animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
							})

						local completo = false
						local QuantidadeAbastecer = 0.142-gasolina
						while completo == false do
							Wait(0)
							local _gasolina = gasolina
							if(QuantidadeAbastecer-0.0005 > 0) then
								QuantidadeAbastecer = QuantidadeAbastecer-0.0005
								gasolina = _gasolina + 0.0005
								_gasolina = gasolina
								if(_gasolina > 0.142) then
									gasolina = 0.142
									TriggerEvent("Combustivel:SetarGasolina", 100, GetVehicleNumberPlateText(veh), GetDisplayNameFromVehicleModel(GetEntityModel(veh)))
									completo = true
								end
								local GasolinaParaPorcentagem = (gasolina/0.142)*65
								SetVehicleFuelLevel(veh,round(GasolinaParaPorcentagem))
								Wait(100)
							else
								gasolina = gasolina + QuantidadeAbastecer
								local GasolinaParaPorcentagem = (gasolina/0.142)*65
								SetVehicleFuelLevel(veh,round(GasolinaParaPorcentagem))
								TriggerEvent("Combustivel:SetarGasolina", 100, GetVehicleNumberPlateText(veh), GetDisplayNameFromVehicleModel(GetEntityModel(veh)))
								completo = true
							end
						end
						GaloesAtuais = GaloesAtuais-1
						if(GaloesAtuais == 0) then
							RemoveWeaponFromPed(GetPlayerPed(-1),  0x34A67B97)
						end
					end
				end
			end
		end
	end
end)

Citizen.CreateThread(function()
	local menu = false
	local bool = false
	local int = 0
	local position = 1
	while true do
		Citizen.Wait(0)
		local isNearFuelStation, stationNumber = isNearStation()
		local isNearFuelPStation, stationPlaneNumber = isNearPlaneStation()
		local isNearFuelHStation, stationHeliNumber = isNearHeliStation()
		local isNearFuelBStation, stationBoatNumber = isNearBoatStation()
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CARROS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		if(isNearFuelStation and IsPedInAnyVehicle(GetPlayerPed(-1), -1) and not IsPedInAnyHeli(GetPlayerPed(-1)) and not isBlackListedModel() and not isElectricModel() and GetPedVehicleSeat(GetPlayerPed(-1)) == -1) then
			TriggerEvent("GUI:Title", "Posto de Combustível")
			local maxEscense = 60-(gasolina/0.142)*60
			TriggerEvent("GUI:Int","Quantos litros deseja comprar? ", int, 0, maxEscense, function(cb)
				int = cb
			end)
			TriggerEvent("GUI:Option", "Pagar", function(cb)
				if(cb) then
					menu = not menu
					TriggerServerEvent("Combustivel:Comprar", int, stationNumber,false)
				else
				end
			end)
			TriggerEvent("GUI:Update")
		end		
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BARCOS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		if(isNearFuelBStation and IsPedInAnyVehicle(GetPlayerPed(-1), -1) and not IsPedInAnyHeli(GetPlayerPed(-1)) and not isBlackListedModel() and GetPedVehicleSeat(GetPlayerPed(-1)) == -1) then
			TriggerEvent("GUI:Title", "Posto de Combustível")
			local maxEssence = 60-(gasolina/0.142)*60
			TriggerEvent("GUI:Int", "Quantos litros deseja comprar? ", int, 0, maxEssence, function(cb)
				int = cb
			end)
			TriggerEvent("GUI:Option", "Pagar", function(cb)
				if(cb) then
					menu = not menu
					TriggerServerEvent("Combustivel:Comprar", int, stationBoatNumber,false)
				else
				end
			end)
			TriggerEvent("GUI:Update")
		end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- AVIÕES
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		if(isNearFuelPStation and IsPedInAnyVehicle(GetPlayerPed(-1), -1) and not isBlackListedModel() and isPlaneModel() and GetPedVehicleSeat(GetPlayerPed(-1)) == -1) then
			TriggerEvent("GUI:Title", "Posto de Combustível")
			local maxEssence = 60-(gasolina/0.142)*60
			TriggerEvent("GUI:Int", "Quantos litros deseja comprar? ", int, 0, maxEssence, function(cb)
				int = cb
			end)
			TriggerEvent("GUI:Option", "Pagar", function(cb)
				if(cb) then
					menu = not menu
					TriggerServerEvent("Combustivel:Comprar", int, stationPlaneNumber,false)
				else
				end
			end)
			TriggerEvent("GUI:Update")
		end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- HELICOPTEROS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		if(isNearFuelHStation and IsPedInAnyVehicle(GetPlayerPed(-1), -1) and not isBlackListedModel() and isHeliModel() and GetPedVehicleSeat(GetPlayerPed(-1)) == -1) then
			TriggerEvent("GUI:Title", "Posto de Combustível")
			local maxEssence = 60-(gasolina/0.142)*60
			TriggerEvent("GUI:Int", "Quantos litros deseja comprar? ", int, 0, maxEssence, function(cb)
				int = cb
			end)
			TriggerEvent("GUI:Option", "Pagar", function(cb)
				if(cb) then
					menu = not menu
					TriggerServerEvent("Combustivel:Comprar", int, stationHeliNumber,false)
				else					
				end
			end)
			TriggerEvent("GUI:Update")
		end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- COMPRAR GALÃO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		if((isNearFuelStation or isNearFuelPStation or isNearFuelHStation or isNearFuelBStation) and not IsPedInAnyVehicle(GetPlayerPed(-1))) then
			Info("Pressione ~INPUT_CONTEXT~ para comprar um galão")
			if(IsControlJustPressed(1, 38)) then
				TriggerServerEvent("Combustivel:ComprarGalao")
			end
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1000)
		local MotorLigado = Citizen.InvokeNative(0xAE31E7DF9B5B132E, GetVehiclePedIsIn(GetPlayerPed(-1)))
		if(IsPedInAnyVehicle(GetPlayerPed(-1), -1) and MotorLigado and GetPedVehicleSeat(GetPlayerPed(-1)) == -1 and not isBlackListedModel()) then
			local kmh = GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false)) * 3.6
			local velocidade = math.ceil(kmh)
			if(velocidade > 0 and velocidade <20) then
				quantidade = 0.00004
			elseif(velocidade >= 20 and velocidade <50) then
				quantidade = 0.00008
			elseif(velocidade >= 50 and velocidade < 70) then
				quantidade = 0.00012
			elseif(velocidade >= 70 and velocidade <90) then
				quantidade = 0.00016
			elseif(velocidade >=90 and velocidade <130) then
				quantidade = 0.00020
			elseif(velocidade >= 130) then
				quantidade = 0.00030
			elseif(velocidade == 0 and IsVehicleEngineOn(veh)) then
				quantidade = 0.0000001
			end
			if(gasolina - quantidade > 0) then
				gasolina = gasolina - quantidade
				local GasolinaParaPorcentagem = (gasolina/0.142)*65
				SetVehicleFuelLevel(GetVehiclePedIsIn(GetPlayerPed(-1)),round(GasolinaParaPorcentagem))
			else
				gasolina = 0
				SetVehicleFuelLevel(GetVehiclePedIsIn(GetPlayerPed(-1)),0)
				SetVehicleUndriveable(GetVehiclePedIsUsing(GetPlayerPed(-1)), true)
			end			
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUNÇÕES
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local UltimaPlaca = 0
local atualizar = true
function ChecarVeiculo()
	if(IsPedInAnyVehicle(GetPlayerPed(-1)) and not isBlackListedModel()) then
		if(atualizar) then
			TriggerServerEvent("vehicule:PegarCombustivel", GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))
			UltimoModelo = GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1))))
			UltimaPlaca = GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1)))
			atualizar = false
		end
	else
		if(not atualizar) then
			TriggerServerEvent("Combustivel:TodosOsJogadores", gasolina, UltimaPlaca, UltimoModelo)
			atualizar = true
		end
	end
	if(gasolina == 0 and GetVehiclePedIsUsing(GetPlayerPed(-1)) ~= nil) then
		SetVehicleEngineOn(GetVehiclePedIsUsing(GetPlayerPed(-1)), false, false, false)
	end
end

function renderBoxes()
	if(IsPedInAnyVehicle(GetPlayerPed(-1), -1) and GetPedVehicleSeat(GetPlayerPed(-1)) == -1 and not isBlackListedModel()) then
		porcentagem = (gasolina/0.142)*100
		if porcentagem > 60 then
			DrawAdvancedText(0.222, 0.858, 0.082, 0.0018, 0.60,"~g~" .. math.ceil(porcentagem), 255, 255, 255, 200, 0, 1)
		elseif porcentagem < 59 and porcentagem > 21 then
			DrawAdvancedText(0.222, 0.858, 0.082, 0.0018, 0.60,"~y~" .. math.ceil(porcentagem), 255, 255, 255, 200, 0, 1)
		elseif porcentagem < 20 then
			DrawAdvancedText(0.222, 0.858, 0.082, 0.0018, 0.60,"~r~" .. math.ceil(porcentagem), 255, 255, 255, 200, 0, 1)
		end
	end
end

function isNearStation()
	local ped = GetPlayerPed(-1)
	local plyCoords = GetEntityCoords(GetPlayerPed(-1), 0)
	for _,items in pairs(station) do
		if(GetDistanceBetweenCoords(items.x, items.y, items.z, plyCoords["x"], plyCoords["y"], plyCoords["z"], true) < 2) then
			return true, items.s
		end
	end

	return false
end

function isNearPlaneStation()
	local ped = GetPlayerPed(-1)
	local plyCoords = GetEntityCoords(GetPlayerPed(-1), 0)
	for _,items in pairs(avioes) do
		if(GetDistanceBetweenCoords(items.x, items.y, items.z, plyCoords["x"], plyCoords["y"], plyCoords["z"], true) < 10) then
			return true, items.s
		end
	end
	return false
end

function isNearHeliStation()
	local ped = GetPlayerPed(-1)
	local plyCoords = GetEntityCoords(GetPlayerPed(-1), 0)
	for _,items in pairs(helicopteros) do
		if(GetDistanceBetweenCoords(items.x, items.y, items.z, plyCoords["x"], plyCoords["y"], plyCoords["z"], true) < 10) then
			return true, items.s
		end
	end
	return false
end

function isNearBoatStation()
	local ped = GetPlayerPed(-1)
	local plyCoords = GetEntityCoords(GetPlayerPed(-1), 0)
	for _,items in pairs(barcos) do
		if(GetDistanceBetweenCoords(items.x, items.y, items.z, plyCoords["x"], plyCoords["y"], plyCoords["z"], true) < 10) then
			return true, items.s
		end
	end
	return false
end

function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end

function round(num, dec)
  local mult = 10^(dec or 0)
  return math.floor(num * mult + 0.5 * mult) / mult
end

function isBlackListedModel()
	local model = GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1))))
	local isBL = false
	for _,k in pairs(blacklistedModels) do
		if(k==model) then
			isBL = true
			break;
		end
	end
	return isBL
end

function isElectricModel()
	local model = GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1))))
	local isEL = false
	for _,k in pairs(electric_model) do
		if(k==model) then
			isEL = true
			break;
		end
	end
	return isEL
end

function isHeliModel()
	local model = GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1))))
	local isHe = false
	for _,k in pairs(heli_model) do
		if(k==model) then
			isHe = true
			break;
		end
	end
	return isHe
end

function isPlaneModel()
	local model = GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1))))
	local isPl = false
	for _,k in pairs(plane_model) do
		if(k==model) then
			isPl = true
			break;
		end
	end
	return isPl
end

function DrawAdvancedText(x,y ,w,h,sc, text, r,g,b,a,font,jus)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(sc, sc)
	N_0x4e096588b13ffeca(jus)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(2, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
	DrawText(x - 0.1+w, y - 0.02+h)
end

function drawTxt(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(2, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

function drawRct(x,y,width,height,r,g,b,a)
	DrawRect(x + width/2, y + height/2, width, height, r, g, b, a)
end

function GetPedVehicleSeat(ped)
    local vehicle = GetVehiclePedIsIn(ped, false)
    for i=-2,GetVehicleMaxNumberOfPassengers(vehicle) do
        if(GetPedInVehicleSeat(vehicle, i) == ped) then return i end
    end
    return -2
end

function IsVehInArray()
	for i,k in pairs(VeiculosUsados) do
		if(k.plate == GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))) and k.model == GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1))))) then
			return true
		end
	end
	return false
end

function searchByModelAndPlate(plate, model)
	for i,k in pairs(VeiculosUsados) do
		if(k.plate == plate and k.model == model) then
			return true, i
		end
	end
	return false, nil
end

function getVehIndex()
	local index = -1
	for i,k in pairs(VeiculosUsados) do
		if(k.plate == GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))) and k.model == GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1))))) then
			index = i
		end
	end
	return index
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EVENTOS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
AddEventHandler("playerSpawned", function()
	TriggerServerEvent("Combustivel:JogadorNasceu")
	TriggerServerEvent("Combustivel:AdicionarCidadao")
end)

RegisterNetEvent("Combustivel:SetarGasolina")
AddEventHandler("Combustivel:SetarGasolina", function(porcentagem, plate, model)
	local toEssence = (porcentagem/100)*0.142
	if(GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))) == plate and model == GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1))))) then
		gasolina = toEssence
		local GasolinaParaPorcentagem = (gasolina/0.142)*65
		SetVehicleFuelLevel(GetVehiclePedIsIn(GetPlayerPed(-1)),round(GasolinaParaPorcentagem))
	end
	TriggerServerEvent("Combustivel:GasolinaServer",porcentagem,plate,model)
end)

RegisterNetEvent('Combustivel:EnviarCombustivel')
AddEventHandler('Combustivel:EnviarCombustivel', function(array)
	for i,k in pairs(array) do
		VeiculosUsados[i] = {plate=k.plate,model=k.model,es=k.es}
	end
end)

RegisterNetEvent('Combustivel:SincronizarPlayers')
AddEventHandler('Combustivel:SincronizarPlayers', function(fuel, vplate, vmodel)
	if(IsPedInAnyVehicle(GetPlayerPed(-1))) then
		currentPedVModel = GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1))))
		currentPedVPlate = GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1)))
		if(currentPedVModel == vmodel and currentPedVPlate == vmodel) then
			gasolina = fuel
		end
	end
end)

RegisterNetEvent("Combustivel:SetarGasolina")
AddEventHandler("Combustivel:SetarGasolina", function(ess,vplate,vmodel)
	if(ess ~= nil and vplate ~= nil and vmodel ~=nil) then
		local bool,index = searchByModelAndPlate(vplate,vmodel)
		if(bool and index ~=nil) then
			VeiculosUsados[index].es = ess
		else
			table.insert(VeiculosUsados, {plate = vplate, model = vmodel, es = ess})
		end
	end
end)

RegisterNetEvent("Combustivel:EstaComprando")
AddEventHandler("Combustivel:EstaComprando", function(valor)
	exports.pNotify:SendNotification({
    	text = "Você comprou " .. string.format("%.0f", valor) .. " litros de combustível",
    	type = "success",timeout = 3000,progressBar = false,layout = "bpa",queue = "left",
    	animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
	})
	local QuantidadeAbastecer = (valor/60)*0.142
	local completo = false
	while completo == false do
		Wait(0)
		local _gasolina = gasolina
		if(QuantidadeAbastecer-0.0005 > 0) then
			QuantidadeAbastecer = QuantidadeAbastecer-0.0005
			gasolina = _gasolina + 0.0005
			_gasolina = gasolina
			if(_gasolina > 0.142) then
				gasolina = 0.142
				completo = true
			end
			SetVehicleUndriveable(GetVehiclePedIsUsing(GetPlayerPed(-1)), true)
			SetVehicleEngineOn(GetVehiclePedIsUsing(GetPlayerPed(-1)), false, false, false)
			local GasolinaParaPorcentagem = (gasolina/0.142)*65
			SetVehicleFuelLevel(GetVehiclePedIsIn(GetPlayerPed(-1)),round(GasolinaParaPorcentagem))
			Wait(100)
		else
			gasolina = gasolina + QuantidadeAbastecer
			local GasolinaParaPorcentagem = (gasolina/0.142)*65
			SetVehicleFuelLevel(GetVehiclePedIsIn(GetPlayerPed(-1)),round(GasolinaParaPorcentagem))
			completo = true
		end
	end

	TriggerServerEvent("Combustivel:TodosOsJogadores", gasolina, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))
	SetVehicleUndriveable(GetVehiclePedIsUsing(GetPlayerPed(-1)), false)
	SetVehicleEngineOn(GetVehiclePedIsUsing(GetPlayerPed(-1)), true, false, false)
end)

RegisterNetEvent("Combustivel:ComprarGalao")
AddEventHandler("Combustivel:ComprarGalao", function()
	RequestAnimDict("amb@world_human_stand_impatient@male@no_sign@idle_a")
	while not HasAnimDictLoaded("amb@world_human_stand_impatient@male@no_sign@idle_a") do
		Citizen.Wait(100)
	end
	TaskPlayAnim(GetPlayerPed(-1),"amb@world_human_stand_impatient@male@no_sign@idle_a","idle_a", 8.0, -8, -1, 0, 0, -1, -1, -1)
	SetEntityCollision(GetPlayerPed(-1),true,true)
    FreezeEntityPosition(GetPlayerPed(-1),false)
	exports.pNotify:SendNotification({
    text = "Aguarde enquanto seu galão é abastecido",
    type = "info",timeout = 3000,progressBar = false, layout = "bpa",queue = "left",
    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
	})
	Wait(7000)
	ClearPedTasks(GetPlayerPed(-1))
	exports.pNotify:SendNotification({
    	text = "Galão abastecido e entregue",
    	type = "success",timeout = 3000,progressBar = false, layout = "bpa",queue = "left",
    	animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
	})

	GiveWeaponToPed(GetPlayerPed(-1), 0x34A67B97, GaloesAtuais+1,  0, true)
	GaloesAtuais = GaloesAtuais +1
end)

RegisterNetEvent("vehicule:EnviarCombustivel")
AddEventHandler("vehicule:EnviarCombustivel", function(bool, ess)
	if(bool == 1) then
		gasolina = ess
	else
		gasolina = (math.random(20,100)/100)*0.142
		vehicle = GetVehiclePedIsUsing(GetPlayerPed(-1))
		TriggerServerEvent("Combustivel:TodosOsJogadores", gasolina, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))
	end
end)