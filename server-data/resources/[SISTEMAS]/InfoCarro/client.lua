-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIAVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local HUD = {
    localizacao = true, 
}

local UI = {
    x = 0.000, 
    y = -0.001, 
}
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUNÇÕES DE FUNCIONAMENTO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function VidaVeiculo()
    local Jogador = GetPlayerPed(-1)
    local MeuCarro = GetVehiclePedIsUsing(Jogador)
    local vehiclehealth = GetEntityHealth(MeuCarro) - 100
    local maxhealth = GetEntityMaxHealth(MeuCarro) - 100
    local procentage = (vehiclehealth / maxhealth) * 100
    return procentage
end

function NaoMostrarMensagemPassageiro(Jogador)
    local MeuCarro = GetVehiclePedIsIn(Jogador, false)
    for i = -2, GetVehicleMaxNumberOfPassengers(MeuCarro) do
        if(GetPedInVehicleSeat(MeuCarro, i) == Jogador) then return i end
    end
    return - 2
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUNÇÕES DA HUD
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Texto(x, y, width, height, scale, text, r, g, b, a)
    SetTextFont(6)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(2, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function Forma(x, y, width, height, r, g, b, a)
    DrawRect(x + width / 2, y + height / 2, width, height, r, g, b, a)
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THREADS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do 
        Citizen.Wait(1)
        local Jogador = GetPlayerPed(-1)
        if (IsPedInAnyVehicle(Jogador, false)) then
            local MeuCarro = GetVehiclePedIsIn(GetPlayerPed(-1), false)
            local Ligado = Citizen.InvokeNative(0xAE31E7DF9B5B132E, MeuCarro)

            velocidade = GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false)) * 3.6
            dano = VidaVeiculo(vehicle)
            MeuCarro = GetVehiclePedIsUsing(Jogador)

            if HUD.localizacao then
                if(IsPedInAnyVehicle(GetPlayerPed(-1), false)) and NaoMostrarMensagemPassageiro(GetPlayerPed(-1)) == -1 then
                    --Texto(UI.x + 0.617, UI.y + 1.260, 1.0, 1.0, 0.45, "~w~ G:", 255, 255, 255, 200)
                    Texto(UI.x + 0.721, UI.y + 1.375, 1.0, 1.0, 0.45, "~w~ %", 255, 255, 255, 200)
                    --Texto(UI.x + 0.516, UI.y + 1.262, 1.0, 1.0, 0.45, "~w~V:", 255, 255, 255, 200)
                    
                    if dano >= 50 then
                        Texto(UI.x + 0.704, UI.y + 1.369, 1.0, 1.0, 0.60, "~g~" .. math.ceil(dano), 255, 255, 255, 200)
                        Texto(UI.x + 0.724, UI.y + 1.342, 1.0, 1.0, 0.45, "~w~%", 255, 255, 255, 200)
                    end

                    if dano <= 49 and dano >= 21 then
                        Texto(UI.x + 0.524, UI.y + 1.255, 1.0, 1.0, 0.60, "~y~" .. math.ceil(dano), 255, 255, 255, 200)
                        Texto(UI.x + 0.542, UI.y + 1.265, 1.0, 1.0, 0.40, "~w~%", 255, 255, 255, 200)
                    end

                    if dano <= 20 then
                        Texto(UI.x + 0.524, UI.y + 1.255, 1.0, 1.0, 0.60, "~r~" .. math.ceil(dano), 255, 255, 255, 200)
                        Texto(UI.x + 0.542, UI.y + 1.265, 1.0, 1.0, 0.40, "~w~%", 255, 255, 255, 200)
                    end

                    if dano <= 0 then  
                        
                        SetVehicleUndriveable(MeuCarro, true)                 
                        Texto(UI.x + 0.524, UI.y + 1.255, 1.0, 1.0, 0.60, "~r~" .. math.ceil(dano), 255, 255, 255, 200)
                        Texto(UI.x + 0.542, UI.y + 1.265, 1.0, 1.0, 0.40, "~w~%", 255, 255, 255, 200)
                    end
                end
                    Texto(UI.x + 0.704, UI.y + 1.302, 1.0, 1.0, 0.60, "~w~" .. math.ceil(velocidade), 255, 255, 255, 200)
                    Texto(UI.x + 0.724, UI.y + 1.308, 1.0, 1.0, 0.45, "~w~ KM/H", 255, 255, 255, 200)
            end
        end
    end
end)