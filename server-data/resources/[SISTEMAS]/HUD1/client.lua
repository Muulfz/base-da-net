local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
HUDserver = Tunnel.getInterface("Barras")
vRPhud = {}
Tunnel.bindInterface("Barras",vRPhud)

fome = 0
sede = 0
dormir = 0
banheiro = 0



Citizen.CreateThread(function()
	while true do 
		Citizen.Wait(1)	
		drawRct(0.82620, 0.88500, 0.065,0.040,21,21,21,255) -- FUNDO PRETO FOME
		drawRct(0.82520, 0.92500, 0.065,0.040,21,21,21,255) -- FUNDO PRETO FOME
		drawRct(0.90700, 0.921000, 0.062,0.016,21,21,21,255) -- FUNDO PRETO FOME
		drawRct(0.90900, 0.880900, 0.062,0.016,21,21,21,255) -- FUNDO PRETO FOME
		drawRct(0.90700, 0.905000, 0.062,0.040,21,21,21,255) -- FUNDO PRETO FOME
		drawRct(0.90700, 0.925000, 0.062,0.040,21,21,21,255) -- FUNDO PRETO FOME
		
		--Vida
		local health = GetEntityHealth(GetPlayerPed(-1)) - 100			
		local varSet = 0.065 * (health / 100)			
		drawRct(0.82620, 0.88500, varSet,0.030,55,115,55,255)			
		--Colete
		armor = GetPedArmour(GetPlayerPed(-1))
		if armor > 100.0 then armor = 100.0 end
		local varSet = 0.065 * (armor / 100)			
		drawRct(0.82520, 0.92500, varSet,0.032,75,75,255,250)

		--Fome
		if fome > 100.0 then fome = 100.0 end
		local varSet = 0.062 * (fome / 100)			
		drawRct(0.90900, 0.880900, varSet,0.016,236, 137, 10, 255)
		--Sede
		if sede > 100.0 then sede = 100.0 end
		local varSet = 0.062 * (sede / 100)		
		drawRct(0.90700, 0.905000, varSet,0.016,50,153,204,250)

		--Sono
		if dormir > 100.0 then dormir = 100.0 end
		local varSet = 0.062 * (dormir / 100)            
		drawRct(0.90700, 0.921000, varSet,0.016,255,255,255,255)
	  
		--Necessidades
		if banheiro > 100.0 then banheiro = 100.0 end
		local varSet = 0.062 * (banheiro / 100)            
		drawRct(0.91700, 0.941000, varSet,0.016,184,115,51,250)
	   
	end
end)

function vRPhud.setHunger(hunger)
    fome = hunger
end

function vRPhud.setThirst(thirst)
    sede = thirst
end

function vRPhud.setSono(sono)
    dormir = sono
end

function vRPhud.setNecessidades(necessidades)
    banheiro = necessidades
end

function drawRct(x,y,width,height,r,g,b,a)
	DrawRect(x + width/2, y + height/2, width, height, r, g, b, a)
end



function drawTxt(x,y ,width,height,scale, text, r,g,b,a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

-- Script feito em imagens para serem alterados vc pode personalizar do jeito que vc quiser usando photoshop etc..

Citizen.CreateThread(function()
    Citizen.Wait(1)
    local playerPed = GetPlayerPed(-1)
         css = [[
			.div_icone_fundo{
				position: absolute;
				content: url(https://i.imgur.com/K9KpwCH.png);
				width: 35%;
				height: 27%;
				top: 80.40%;
				right: -5%;
			 }
			 
	]]
			vRP.setDiv("icone_fundo",css)
	end
end)