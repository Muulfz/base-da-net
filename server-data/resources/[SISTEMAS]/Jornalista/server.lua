local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")


AddEventHandler('chatMessage', function(source, n, message)
local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"jornal.on") then
    else
        return
    end
    command = stringsplit(message, " ")
	if(command[1] == "/camera") then
    CancelEvent()
   	TriggerClientEvent("Cam:ToggleCam", source)
    end
	
	if(command[1] == "/mic") then
    CancelEvent()
   	TriggerClientEvent("Mic:ToggleMic", source)
    end
end)


function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end