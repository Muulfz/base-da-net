/* edit chang7 - EliteBR // synn screen */

var config =
{    
    enableImage: false,
 
    image: "img/logo.png",

    cursorImage: "img/cursor.png",
 
    imageSize: [578, 236],
 
    progressBarType: 0,
 
    progressBars:
    {
        "INIT_CORE": {
            enabled: false,
        },
 
        "INIT_BEFORE_MAP_LOADED": {
            enabled: true,
        },
 
        "MAP": {
            enabled: true,
        },
 
        "INIT_AFTER_MAP_LOADED": {
            enabled: true,
        },
 
        "INIT_SESSION": {
            enabled: true,
        }
    },
 
    loadingMessages:
    [
		"",
        "",
    ],
 
    loadingMessageSpeed: 6 * 1000,
 
    music:
    [
        "8qLL2Gx3I_k",
    ],

    enableMusic: true,
 
    musicVolume: 15,
 
    staticBackground: true,
   
    background:
    [
        "img/bg1.jpg",
    ],
 
    backgroundSpeed: 6 * 1000,

    backgroundStyle: "zoom",

    enableLog: true,
}
