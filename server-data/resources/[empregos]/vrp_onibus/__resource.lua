description "vRP onibus"

dependency "vrp"

client_scripts{ 
 -- "lib/Tunnel.lua",
 '@vrp/lib/utils.lua',
  "client.lua"
}

server_scripts{ 
  "@vrp/lib/utils.lua",
  "server.lua"
}