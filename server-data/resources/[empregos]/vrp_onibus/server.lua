local Proxy = module("vrp", "lib/Proxy")
local Tunnel = module("vrp", "lib/Tunnel")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_onibus")

RegisterServerEvent("gd_jobs_bus:tryStartJob")
AddEventHandler("gd_jobs_bus:tryStartJob", function(location, tier)
        TriggerClientEvent("gd_jobs_bus:startJob", source, location, tier)
end)

RegisterServerEvent("gd_jobs_bus:finishJob")
AddEventHandler("gd_jobs_bus:finishJob", function(total_fares, payment, tier)
    local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
    local pay = (4 * payment) * tier
    local money = math.floor((total_fares * pay) / 10)
	vRP.giveMoney(user_id,money,total_fares)
   TriggerClientEvent('chatMessage',player , "[Cobrador]", {232, 9, 9}, "Recebeu " .. money .." por ".. total_fares .." passagens")
end)

RegisterServerEvent("gd_jobs_bus:pickupJob")
AddEventHandler("gd_jobs_bus:pickupJob", function(fares, payment, tier)
    local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
    local pay = (4 * payment) * tier
    local money = math.floor(fares * pay)
	vRP.giveMoney(user_id,money,fares)
	TriggerClientEvent('chatMessage',player, "[Cobrador]", {232, 9, 9}, "Recebeu " .. money .." por ".. fares .." passagens")
end)

AddEventHandler("chatMessage",function(source,name,msg)
	if msg:sub(1,4) == "/pos" then
		TriggerClientEvent("gd:pos",source,msg)
		CancelEvent()
	end
end)

RegisterServerEvent("gd:pos")
AddEventHandler("gd:pos", function(text, pos)
    local name = text:sub(6)
    local str = string.format('{name = "%s", x = %f, y = %f, z = %f},', name, pos.x, pos.y, pos.z)
    print(str)
    local file = io.open("markers.dev", "a+")
    file:write(str .. "\n")
    file:close()
end)