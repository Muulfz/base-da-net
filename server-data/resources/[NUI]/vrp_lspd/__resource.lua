resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

ui_page "nui/ui.html"

files {
	"nui/ui.html",
	"nui/ui.js", 
	"nui/ui.css",
	"nui/Roboto.ttf"
}

client_script {
  '@vrp/lib/utils.lua',
  'cl_action.lua',
}

server_script {
  '@vrp/lib/utils.lua',
  'server.lua'
}
