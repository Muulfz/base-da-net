-- Credits: Sighmir and Shadow --
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
vRPlspd = {}
Tunnel.bindInterface("vrp_lspd",vRPlspd)

function vRPlspd.checkPermission()
local user_id = vRP.getUserId(source)
  return vRP.hasPermission(user_id, "police.vehicle")
end