-- ###################################
--
--       Base Solzinho
--
-- ###################################
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRPjbilegal = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Tunnel.bindInterface("vrp_ilegal",vRPjbilegal)

function vRPjbilegal.jobMaconha()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Traficante de Maconha")
	vRPclient._notify(source, "Agora você é um Traficante de Maconha")
  end
end

function vRPjbilegal.jobMetanfetamina()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Traficante de Metanfetamina")
	vRPclient._notify(source, "Agora você é um Traficante de Metanfetamina")
  end
end

function vRPjbilegal.jobCocaina()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Traficante de Cocaina")
	vRPclient._notify(source, "Agora você é um Traficante de Cocaina")
  end
end

function vRPjbilegal.jobTartaruga()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Traficante de Tartaruga")
	vRPclient._notify(source, "Agora você é um Traficante de Tartaruga")
  end
end

function vRPjbilegal.jobArmas()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Traficante de Armas")
	vRPclient._notify(source, "Agora você é um Traficante de Armas")
  end
end

function vRPjbilegal.jobOrgaos()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Traficante de Orgaos")
	vRPclient._notify(source, "Agora você é um Traficante de Orgaos")
  end
end

function vRPjbilegal.jobDesempregado()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Desempregado")
	vRPclient._notify(source, "Agora você é um Desempregado")
  end
end
