-- ###################################
--
--      Base Solzinho
--
-- ###################################
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
JBserverilegal = Tunnel.getInterface("vrp_ilegal","vrp_ilegal")

local menuEnabled = false

RegisterNetEvent("ToggleActionmenu")
AddEventHandler("ToggleActionmenu", function()
	ToggleActionMenu()
end)

function ToggleActionMenu()
	Citizen.Trace("tutorial launch")
	menuEnabled = not menuEnabled
	if ( menuEnabled ) then
		SetNuiFocus( true, true )
		SendNUIMessage({
			showPlayerMenu = true
		})
	else
		SetNuiFocus( false )
		SendNUIMessage({
			showPlayerMenu = false
		})
	end
end

function killTutorialMenu()
SetNuiFocus( false )
		SendNUIMessage({
			showPlayerMenu = false
		})
		menuEnabled = false

end


RegisterNUICallback('close', function(data, cb)
  ToggleActionMenu()
  cb('ok')
end)

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(0)
		DrawMarker(27, 707.02239990234,-966.8125,30.41284942627-1.0001, 0, 0, 0, 0, 0, 0, 1.0001,1.0001,1.0001, 0, 232, 255, 155, 0, 0, 0, 0, 0, 0, 0)
		if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), 707.02239990234,-966.8125,30.41284942627, true ) < 1 then
			DisplayHelpText("Pressione ~g~E~s~ para acessar a agencia de empregos")
		 if (IsControlJustReleased(1, 51)) then
			SetNuiFocus( true, true )
			SendNUIMessage({
				showPlayerMenu = true
			})
		 end
		end
	end
end)

RegisterNUICallback('Maconha', function(data, cb)
	JBserverilegal.jobMaconha()
  	cb('ok')
end)

RegisterNUICallback('Metanfetamina', function(data, cb)
	JBserverilegal.jobMetanfetamina()
  	cb('ok')
end)

RegisterNUICallback('Cocaina', function(data, cb)
	JBserverilegal.jobCocaina()
  	cb('ok')
end)

RegisterNUICallback('Tartaruga', function(data, cb)
	JBserverilegal.jobTartaruga()
  	cb('ok')
end)

RegisterNUICallback('Armas', function(data, cb)
	JBserverilegal.jobArmas()
  	cb('ok')
end)

RegisterNUICallback('Orgaos', function(data, cb)
	JBserverilegal.jobOrgaos()
  	cb('ok')
end)

RegisterNUICallback('Desempregado', function(data, cb)
	JBserverilegal.jobDesempregado()
  	cb('ok')
end)


RegisterNUICallback('closeButton', function(data, cb)
	killTutorialMenu()
  	cb('ok')
end)

function DrawSpecialText(m_text, showtime)
	SetTextEntry_2("STRING")
	AddTextComponentString(m_text)
	DrawSubtitleTimed(showtime, 1)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end