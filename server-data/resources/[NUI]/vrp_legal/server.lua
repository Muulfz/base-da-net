-- ###################################
--
--       Base Solzinho
--
-- ###################################
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRPjb = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Tunnel.bindInterface("vrp_legal",vRPjb)

function vRPjb.jobCitizen()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Desempregado")
	vRPclient._notify(source, "Agora você é um Desempregado")
  end
end

function vRPjb.jobPescador()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Pescador")
	vRPclient._notify(source, "Agora você é um Pescador")
  end
end

function vRPjb.jobMecanico()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Mecânico")
	vRPclient._notify(source, "Agora você é um Mecânico")
  end
end

function vRPjb.jobsedex()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Sedex")
	vRPclient._notify(source, "Agora você é um Sedex")
  end
end

function vRPjb.jobUber()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Uber")
	vRPclient._notify(source, "Agora você é um Uber")
  end
end

function vRPjb.jobOnibus()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "user") then
	vRP.addUserGroup(user_id, "Motorista de Onibus")
	vRPclient._notify(source, "Agora você é um Motorista de Onibus")
  end
end

function vRPjb.jobJornalista()
  local source = source
  local user_id = vRP.getUserId(source)
  if vRP.hasGroup(user_id, "user") and
    vRP.hasPermission(user_id,"emprego.jornalista") then
	vRP.addUserGroup(user_id, "Jornalista")
	vRPclient._notify(source, "Agora você é um Jornalista") 
    else 
      vRPclient._notify(source," ~r~ Você não tem permissao para ser jornalista ")
    end
end


function vRPjb.jobAdvogado()
  local source = source
  local user_id = vRP.getUserId(source)
  if vRP.hasGroup(user_id, "user") and
    vRP.hasPermission(user_id,"emprego.Advogado") then
	vRP.addUserGroup(user_id, "Advogado")
	vRPclient._notify(source, "Agora você é um Advogado") 
    else 
      vRPclient._notify(source," ~r~ Você não tem permissao para ser Advogado ")
    end
end
