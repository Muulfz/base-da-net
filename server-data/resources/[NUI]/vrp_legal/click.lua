-- ###################################
--
--       Base Solzinho
--
-- ###################################
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
JBserver = Tunnel.getInterface("vrp_legal","vrp_legal")

local menuEnabled = false

RegisterNetEvent("ToggleActionmenu")
AddEventHandler("ToggleActionmenu", function()
	ToggleActionMenu()
end)

function ToggleActionMenu()
	Citizen.Trace("tutorial launch")
	menuEnabled = not menuEnabled
	if ( menuEnabled ) then
		SetNuiFocus( true, true )
		SendNUIMessage({
			showPlayerMenu = true
		})
	else
		SetNuiFocus( false )
		SendNUIMessage({
			showPlayerMenu = false
		})
	end
end

function killTutorialMenu()
SetNuiFocus( false )
		SendNUIMessage({
			showPlayerMenu = false
		})
		menuEnabled = false

end


RegisterNUICallback('close', function(data, cb)
  ToggleActionMenu()
  cb('ok')
end)

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(0)
		DrawMarker(27, -267.92422485352,-957.95263671875,31.22313117981-1.0001, 0, 0, 0, 0, 0, 0, 1.0001,1.0001,1.0001, 0, 232, 255, 155, 0, 0, 0, 0, 0, 0, 0)
		if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), -267.92422485352,-957.95263671875,31.22313117981, true ) < 1 then
			DisplayHelpText("Pressione ~g~E~s~ para acessar a agencia de empregos")
		 if (IsControlJustReleased(1, 51)) then
			SetNuiFocus( true, true )
			SendNUIMessage({
				showPlayerMenu = true
			})
		 end
		end
	end
end)

RegisterNUICallback('desempregado', function(data, cb)
	JBserver.jobCitizen()
  	cb('ok')
end)

RegisterNUICallback('Pescador', function(data, cb)
	JBserver.jobPescador()
  	cb('ok')
end)

RegisterNUICallback('Mecanico', function(data, cb)
	JBserver.jobMecanico()
  	cb('ok')
end)

RegisterNUICallback('sedex', function(data, cb)
	JBserver.jobsedex()
  	cb('ok')
end)

RegisterNUICallback('Uber', function(data, cb)
	JBserver.jobUber()
  	cb('ok')
end)

RegisterNUICallback('Jornalista', function(data, cb)
	JBserver.jobJornalista()
  	cb('ok')
end)

RegisterNUICallback('Advogado', function(data, cb)
	JBserver.jobAdvogado()
  	cb('ok')
end)

RegisterNUICallback('Onibus', function(data, cb)
	JBserver.jobOnibus()
  	cb('ok')
end)


RegisterNUICallback('closeButton', function(data, cb)
	killTutorialMenu()
  	cb('ok')
end)

function DrawSpecialText(m_text, showtime)
	SetTextEntry_2("STRING")
	AddTextComponentString(m_text)
	DrawSubtitleTimed(showtime, 1)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end