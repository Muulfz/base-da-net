local Proxy = module("vrp", "lib/Proxy")
local Tunnel = module("vrp", "lib/Tunnel")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_salario")

RegisterServerEvent('salario:profissao')
AddEventHandler('salario:profissao', function()
  	local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
	if vRP.hasPermission(user_id,"recruta.paycheck") then
		vRP.giveMoney(user_id,3100)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$3100")
	end	
	if vRP.hasPermission(user_id,"soldado.paycheck") then
		vRP.giveMoney(user_id,5000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$5000")
	end	
	if vRP.hasPermission(user_id,"cabo.paycheck") then
		vRP.giveMoney(user_id,6500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$6500")
	end	
	if vRP.hasPermission(user_id,"3sargento.paycheck") then
		vRP.giveMoney(user_id,8500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$8500")
	end	
	if vRP.hasPermission(user_id,"2sargento.paycheck") then
		vRP.giveMoney(user_id,10000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$10000")
	end	
	if vRP.hasPermission(user_id,"1sargento.paycheck") then
		vRP.giveMoney(user_id,11500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$11500")
	end	
	if vRP.hasPermission(user_id,"2tenente.paycheck") then
		vRP.giveMoney(user_id,13000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$13000")
	end	
	if vRP.hasPermission(user_id,"1tenente.paycheck") then
		vRP.giveMoney(user_id,15000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$15000")
	end
	if vRP.hasPermission(user_id,"tcoronel.paycheck") then
		vRP.giveMoney(user_id,17000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$17000")
	end	
	if vRP.hasPermission(user_id,"coronel.paycheck") then
		vRP.giveMoney(user_id,20000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$20000")
	end
	if vRP.hasPermission(user_id,"aguia.paycheck") then
		vRP.giveMoney(user_id,6500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$6500")
	end
	if vRP.hasPermission(user_id,"agtpf.paycheck") then
		vRP.giveMoney(user_id,16500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$16500")
	end	
	
	if vRP.hasPermission(user_id,"delpf.paycheck") then
		vRP.giveMoney(user_id,22000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$22000")
	end	
	
	if vRP.hasPermission(user_id,"samu.paycheck") then
		vRP.giveMoney(user_id,12000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$12000")
	end	
	
	if vRP.hasPermission(user_id,"chefesamu.paycheck") then
		vRP.giveMoney(user_id,18000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$18000")
	end	
	
	if vRP.hasPermission(user_id,"bolacha.paycheck") then
		vRP.giveMoney(user_id,1500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$1500")
	end	
	
	if vRP.hasPermission(user_id,"cmdbolacha.paycheck") then
		vRP.giveMoney(user_id,3000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$3000")
	end	
	if vRP.hasPermission(user_id,"ppp.paycheck") then
		vRP.giveMoney(user_id,9500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$9500")
	end	
	
	if vRP.hasPermission(user_id,"ppg.paycheck") then
		vRP.giveMoney(user_id,7000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$7000")
	end	
	
	if vRP.hasPermission(user_id,"bolsafamilia.paycheck") then
		vRP.giveMoney(user_id,500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$500")
	end	
	
	if vRP.hasPermission(user_id,"auxiliogasolina.paycheck") then
		vRP.giveMoney(user_id,500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$500")
	end	
	
	if vRP.hasPermission(user_id,"pescador.paycheck") then
		vRP.giveMoney(user_id,3000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$3000")
	end	
	
	if vRP.hasPermission(user_id,"sedex.paycheck") then
		vRP.giveMoney(user_id,2700)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$2700")
	end	
	
	if vRP.hasPermission(user_id,"taxi.paycheck") then
		vRP.giveMoney(user_id,2300)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$2300")
	end	
	if vRP.hasPermission(user_id,"repair.paycheck") then
		vRP.giveMoney(user_id,2800)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$2800")
	end	
	if vRP.hasPermission(user_id,"bankdriver.paycheck") then
		vRP.giveMoney(user_id,3100)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$3100")
	end	
	if vRP.hasPermission(user_id,"jornalista.paycheck") then
		vRP.giveMoney(user_id,8000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$8000")
	end	
	if vRP.hasPermission(user_id,"vipgold.paycheck") then
		vRP.giveMoney(user_id,15000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$15000")
	end	
	if vRP.hasPermission(user_id,"vipdiamond.paycheck") then
		vRP.giveMoney(user_id,25000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$25000")
	end
	if vRP.hasPermission(user_id,"vipplatinum.paycheck") then
		vRP.giveMoney(user_id,35000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$35000")
	end	
	if vRP.hasPermission(user_id,"delivery.paycheck") then
		vRP.giveMoney(user_id,2500)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$2500")
	end	
	if vRP.hasPermission(user_id,"streamer.paycheck") then
		vRP.giveMoney(user_id,5000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$5000")
	end	
	if vRP.hasPermission(user_id,"dj.paycheck") then
		vRP.giveMoney(user_id,1900)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$1900")
	end	
	if vRP.hasPermission(user_id,"divulgador.paycheck") then
		vRP.giveMoney(user_id,4000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$4000")
	end	
	if vRP.hasPermission(user_id,"prefeito.paycheck") then
		vRP.giveMoney(user_id,10000)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$10000")
	end	
	if vRP.hasPermission(user_id,"prostituta.paycheck") then
		vRP.giveMoney(user_id,900)
		vRPclient.notifyPicture(player,"CHAR_BANK_MAZE","9","Maze Bank","Salario: R$900")
	end		
	
end)
--==========================================================================================
RegisterServerEvent('salario:retira')
AddEventHandler('salario:retira', function()
  	local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
	if vRP.hasPermission(user_id,"recruta.paycheck") then
		vRP.tryFullPayment(user_id,450)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$450")
	end	
		if vRP.hasPermission(user_id,"soldado.paycheck") then
		vRP.tryFullPayment(user_id,600)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$600")
	end	
	if vRP.hasPermission(user_id,"cabo.paycheck") then
		vRP.tryFullPayment(user_id,800)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$800")
	end	
	if vRP.hasPermission(user_id,"3sargento.paycheck") then
		vRP.tryFullPayment(user_id,1000)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$1000")
	end	
	if vRP.hasPermission(user_id,"2sargento.paycheck") then
		vRP.tryFullPayment(user_id,1200)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$1200")
	end	
	if vRP.hasPermission(user_id,"1sargento.paycheck") then
		vRP.tryFullPayment(user_id,1400)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$1400")
	end	
	if vRP.hasPermission(user_id,"2tenente.paycheck") then
		vRP.tryFullPayment(user_id,1600)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$1600")
	end	
	if vRP.hasPermission(user_id,"1tenente.paycheck") then
		vRP.tryFullPayment(user_id,1800)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$1800")
	end	
	if vRP.hasPermission(user_id,"tcoronel.paycheck") then
		vRP.tryFullPayment(user_id,2000)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$2000")
	end
	if vRP.hasPermission(user_id,"coronel.paycheck") then
		vRP.tryFullPayment(user_id,2200)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$2200")
	end	
	if vRP.hasPermission(user_id,"aguia.paycheck") then
		vRP.tryFullPayment(user_id,600)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$600")
	end
	if vRP.hasPermission(user_id,"agtpf.paycheck") then
		vRP.tryFullPayment(user_id,1400)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$1400")
	end	
	if vRP.hasPermission(user_id,"delpf.paycheck") then
		vRP.tryFullPayment(user_id,2500)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$2500")
	end	
	
	if vRP.hasPermission(user_id,"bolacha.paycheck") then
		vRP.tryFullPayment(user_id,100)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$100")
	end	
	
	if vRP.hasPermission(user_id,"cmdbolacha.paycheck") then
		vRP.tryFullPayment(user_id,100)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$100")
	end	
	if vRP.hasPermission(user_id,"ppp.paycheck") then
		vRP.tryFullPayment(user_id,100)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$100")
	end	
	
	if vRP.hasPermission(user_id,"ppg.paycheck") then
		vRP.tryFullPayment(user_id,100)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$100")
	end	
	
	if vRP.hasPermission(user_id,"pescador.paycheck") then
		vRP.tryFullPayment(user_id,250)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$250")
	end	
	
	if vRP.hasPermission(user_id,"sedex.paycheck") then
		vRP.tryFullPayment(user_id,100)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$100")
	end	
	
	if vRP.hasPermission(user_id,"taxi.paycheck") then
		vRP.tryFullPayment(user_id,170)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$170")
	end	
	if vRP.hasPermission(user_id,"repair.paycheck") then
		vRP.tryFullPayment(user_id,165)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$165")
	end	
	if vRP.hasPermission(user_id,"bankdriver.paycheck") then
		vRP.tryFullPayment(user_id,205)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$205")
	end	
	if vRP.hasPermission(user_id,"jornalista.paycheck") then
		vRP.tryFullPayment(user_id,450)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$450")
	end	
	if vRP.hasPermission(user_id,"delivery.paycheck") then
		vRP.tryFullPayment(user_id,175)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$175")
	end	
	if vRP.hasPermission(user_id,"streamer.paycheck") then
		vRP.tryFullPayment(user_id,175)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$175")
	end	
	if vRP.hasPermission(user_id,"dj.paycheck") then
		vRP.tryFullPayment(user_id,175)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$175")
	end	
	if vRP.hasPermission(user_id,"divulgador.paycheck") then
		vRP.tryFullPayment(user_id,175)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$175")
	end	
	if vRP.hasPermission(user_id,"prefeito.paycheck") then
		vRP.tryFullPayment(user_id,175)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$175")
	end	
	if vRP.hasPermission(user_id,"prostituta.paycheck") then
		vRP.tryFullPayment(user_id,175)
		vRPclient.notifyPicture(player,"CHAR_MP_MORS_MUTUAL","9","Maze Bank","Imposto Comunitário: R$175")
	end		
	
end)
