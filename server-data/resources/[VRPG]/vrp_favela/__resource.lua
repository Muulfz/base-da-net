
description "vRP favela"
--ui_page "ui/index.html"

dependency "vrp"

client_scripts{ 
'@vrp/lib/utils.lua',
  "cfg/favela.lua",
  "client.lua"
}

server_scripts{ 
 '@vrp/lib/utils.lua',
  "cfg/favela.lua",
  "server.lua"
}
