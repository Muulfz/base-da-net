cfg = {}

cfg.blips = true 

cfg.seconds = 900 

cfg.cooldown = 172800 

cfg.bands = 2 
cfg.permission = "favela.dom"

cfg.favelas = { 
	["Favela"] = {
		position = { ['x'] = 740.07739257813,['y'] =-292.95120239258,['z'] =63.202430725098 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Campinho",
		lastdommed = 0
	},
	["Favela1"] = {
		position = { ['x'] = 1272.1625976563,['y'] =-268.61929321289,['z'] =91.037734985352 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Helipa",
		lastdommed = 0
	},
	["Favela2"] = {
		position = { ['x'] = 1362.8059082031,['y'] =-815.15014648438,['z'] =88.641792297363 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Helipa 2",
		lastdommed = 0
	},
	["Favela3"] = {
		position = { ['x'] = 307.76138305664,['y'] = -1963.3492431641,['z'] = 28.033750534058 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Morro do Dende",
		lastdommed = 0
	},
	["Favela4"] = {
		position = { ['x'] = -154.54740905762,['y'] = -1589.6705322266,['z'] =44.658893585205 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Baile da 17",
		lastdommed = 0
	},
	["Favela5"] = {
		position = { ['x'] = -847.81195068359,['y'] =575.80444335938,['z'] =111.03495788574 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Morro do Marcola",
		lastdommed = 0
	}
}