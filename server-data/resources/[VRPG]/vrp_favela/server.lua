local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_favela")

local favelas = cfg.favelas

local dommers = {}

function get3DDistance(x1, y1, z1, x2, y2, z2)
	return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2) + math.pow(z1 - z2, 2))
end

RegisterServerEvent('es_favela:toofar')
AddEventHandler('es_favela:toofar', function(domm)
	if(dommers[source])then
		TriggerClientEvent('es_favela:toofarlocal', source)
		dommers[source] = nil
		TriggerClientEvent('chatMessage', -1, 'TWITTER', { 0, 0x99, 255 }, "A invasão foi cancelada, peidou na farofa: ^2" .. favelas[domm].nameoffavela)
	end
end)

RegisterServerEvent('es_favela:playerdied')
AddEventHandler('es_favela:playerdied', function(domm)
	if(dommers[source])then
		TriggerClientEvent('es_favela:playerdiedlocal', source)
		dommers[source] = nil
		TriggerClientEvent('chatMessage', -1, 'TWITTER', { 0, 0x99, 255 }, "A invasão foi cancelada, peidou na farofa: ^2" .. favelas[domm].nameoffavela)
	end
end)

RegisterServerEvent('es_favela:dom')
AddEventHandler('es_favela:dom', function(domm)
  local user_id = vRP.getUserId(source)
  local player = vRP.getUserSource(user_id)
  local bands = vRP.getUsersByPermission(cfg.permission)
    if #bands >= cfg.bands then
	  if favelas[domm] then
		  local favela = favelas[domm]

		  if (os.time() - favela.lastdommed) < cfg.seconds+cfg.cooldown and favela.lastdommed ~= 0 then
			  TriggerClientEvent('chatMessage', player, 'Dominação', { 0, 0x99, 255 }, "Essa favela já foi tomada, aguarda no sapatinho: ^2" .. (cfg.seconds+cfg.cooldown - (os.time() - favela.lastdommed)) .. "^0 segundos.")
			  return
		  end
		  TriggerClientEvent('chatMessage', -1, 'TWITTER', { 0, 0x99, 255 }, "Invasão rolando na ^2" .. favela.nameoffavela)
		  TriggerClientEvent('chatMessage', player, 'SISTEMA', { 0, 0x99, 255 }, "Voce começou uma invasão: ^2" .. favela.nameoffavela .. "^0, não vai muito pra longe não em!")
		  TriggerClientEvent('chatMessage', player, 'SISTEMA', { 0, 0x99, 255 }, "Aguenta firme ai ^115 ^0Minutos, e a favela é sua!")
		  TriggerClientEvent('es_favela:currentlydomming', player, domm)
		  favelas[domm].lastdommed = os.time()
		  dommers[player] = domm
		  local savedSource = player
		  SetTimeout(cfg.seconds*1000, function()
			  if(dommers[savedSource])then
				  if(user_id)then
					  vRP.giveInventoryItem(user_id,"dirty_money",favela.reward,true)
					  TriggerClientEvent('chatMessage', -1, 'TWITTER', { 0, 0x99, 255 }, "A invasão terminou: ^2" .. favela.nameoffavela .. "^0!")	
					  TriggerClientEvent('es_favela:dommerycomplete', savedSource, favela.reward)
				  end
			  end
		  end)
	  end
    else
      vRPclient._notify(player,"~r~Não tem bandido suficiente.")
    end
end)