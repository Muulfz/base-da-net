--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

--police weapons // comment out the weapons if you dont want to give weapons.
policia_militar = {}
policia_militar["Recruta"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
    if vRP.hasPermission(user_id,"pm.recruta") then	
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250}  
	}, true)
	BMclient.setArmour(player,1000,true)
	vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Recruta")
	end
end}

policia_militar["Soldado"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.soldado") then		
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250} 
	}, true)
	BMclient.setArmour(player,1000,true)
	vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Soldado")
	end
end}

policia_militar["Cabo"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.cabo") then		
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250} 
	}, true)
	BMclient.setArmour(player,1000,true)
vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Cabo")
	end
end}

policia_militar["Sargento"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.sargento") then	
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_SMG"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250}
	}, true)
	BMclient.setArmour(player,1000,true)
vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Sargento")
	end
end}

policia_militar["Tenente"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.tenente") then	
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_CARBINERIFLE"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250}
	}, true)
	BMclient.setArmour(player,1000,true)
vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Tenente")
	end
end}


policia_militar["Capitao"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.capitao") then		
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_CARBINERIFLE"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250}
	}, true)
	BMclient.setArmour(player,1000,true)
vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Capitao")
	end
end}

policia_militar["Major"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.major") then	
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_CARBINERIFLE"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250}
	}, true)
	BMclient.setArmour(player,1000,true)
vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Major")
	end
end}

policia_militar["Coronel"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.coronel") then		
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_SPECIALCARBINE"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250}
	}, true)
	BMclient.setArmour(player,1000,true)
vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Coronel")
	end
end}


policia_militar["Aguia"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.aguia") then		
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_SPECIALCARBINE"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250}
	}, true)
	BMclient.setArmour(player,1000,true)
vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Aguia")
	end
end}


policia_militar["Rota"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.rota") then		
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_SPECIALCARBINE"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250}
	}, true)
	BMclient.setArmour(player,1000,true)
vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Rota")
	end
end}


policia_militar["Comandante Rota"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
if vRP.hasPermission(user_id,"pm.rotacm") then	
     vRPclient.giveWeapons(player,{
	  ["WEAPON_COMBATPISTOL"] = {ammo=250},
	  ["WEAPON_SPECIALCARBINE"] = {ammo=250},
	  ["WEAPON_PUMPSHOTGUN"] = {ammo=250},
	  ["WEAPON_NIGHTSTICK"] = {ammo=250},
	  ["WEAPON_STUNGUN"] = {ammo=250}
	}, true)
	BMclient.setArmour(player,1000,true)
vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Policiais")
	else
	vRPclient._notify(player,"~r~ Você não pertence ao cargo Comandante Rota")
	end
end}



samu_equipamentos = {}
samu_equipamentos["Equipamentos Medicos"] = {function(player,choice)
    local user_id = vRP.getUserId(player)	
     if  vRP.tryPayment(user_id,100,false)then	
     vRP.giveInventoryItem(user_id,"medkit",10,true)
	-- vRP.giveInventoryItem(user_id,"pills",1,true) -- Caso queira remover apenas comente na frente do vrp com --
	 --vRP.giveInventoryItem
	 vRPclient._notify(player,"Você recebeu ~ g ~ seus equipamentos Medicos")
    else 
      vRPclient._notify(player," ~r~ Você não tem dinheiro suficiente para essa compra")
    end
end}

samu_cura = {}
samu_cura["Cura"] = {function(player,choice)
    local user_id = vRP.getUserId(player)	
     if vRP.tryPayment(user_id,1000,false)then	
    vRPclient.setHealth(player,1000)
	 vRPclient._notify(player,"Você ~ g ~ se curou por 100")
    else 
      vRPclient._notify(player," ~r~ Você não tem dinheiro suficiente para essa compra")
    end
end}


--======================
dpentrar = {}
dpentrar["Entrar no Arsenal"] = {function(player,choice)
    vRPclient.teleport(player,453.5442199707,-982.6171875,30.689594268799)
end}

dpsair = {}
dpsair["Sair"] = {function(player,choice)
    vRPclient.teleport(player,452.60632324219,-982.48846435547,30.68959236145)
end}
---===============================
hpentrar = {}
hpentrar["Entrar no Hospital"] = {function(player,choice)
    vRPclient.teleport(player,275.03616333008,-1360.9791259766,24.537801742554)
end}

hpsair = {}
hpsair["Sair"] = {function(player,choice)
    vRPclient.teleport(player,294.63272094727,-1448.5067138672,29.966617584229) --entra
end}