--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]


Citizen.CreateThread(function()



vRP.addStaticMenuChoices("policia_militar", policia_militar)
vRP.addStaticMenuChoices("samu_equipamentos", samu_equipamentos) 
vRP.addStaticMenuChoices("samu_cura", samu_cura) 
vRP.addStaticMenuChoices("dpentrar", dpentrar) 
vRP.addStaticMenuChoices("dpsair", dpsair)
vRP.addStaticMenuChoices("hpentrar", hpentrar) 
vRP.addStaticMenuChoices("hpsair", hpsair)  



end)