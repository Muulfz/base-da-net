
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local Luang = module("vrp", "lib/Luang")
local cfg = module("vrp_basic_mission", "cfg/missions")

-- load global and local languages
Luang = module("vrp", "lib/Luang")
lcfg = module("vrp", "cfg/base")
Lang = Luang()
--Lang:loadLocale(cfg.lang, module("vrp", "cfg/lang/"..cfg.lang) or {})
--Lang:loadLocale(cfg.lang, module("vrp_basic_mission", "cfg/lang/"..cfg.lang) or {})
Lang:loadLocale(lcfg.lang, module("vrp", "cfg/lang/"..lcfg.lang) or {})
Lang:loadLocale(lcfg.lang, module("vrp_basic_mission", "cfg/lang/"..lcfg.lang) or {})
lang = Lang.lang[lcfg.lang]
vRPbm = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Mclient = Tunnel.getInterface("vrp_basic_mission")
BMclient = Tunnel.getInterface("vrp_basic_menu")

print(1)
print('task_mission')
function task_mission()
  -- REPAIR
  for k,v in pairs(cfg.repair) do -- each repair perm def
    -- add missions to users
    local users = vRP.getUsersByPermission(k)
    for l,w in pairs(users) do
      local user_id = w
      local player = vRP.getUserSource(user_id)
      if vRP.getSpawns(user_id) > 0 and not vRP.hasMission(player) then
        if math.random(1,v.chance) == 1 then -- chance check
          -- build mission
          local mdata = {}
          mdata.name = lang.repair({v.title})
          mdata.steps = {}

          -- build steps
          for i=1,v.steps do
            local step = {
              text = lang.repair({v.title}).."<br />"..lang.reward({v.reward}),
              onenter = function(player, area)
                if vRP.tryGetInventoryItem(user_id,"repairkit",1,true) then
                  vRPclient._playAnim(player,false,{task="WORLD_HUMAN_WELDING"},false)
                  SetTimeout(15000, function()
                    vRP.nextMissionStep(player)
                    vRPclient._stopAnim(player,false)

                    -- last step
                    if i == v.steps then
                      vRP.giveMoney(user_id,v.reward)
                      vRPclient._notify(player, glang.money.received({v.reward}))
                    end
                  end)
                end
              end,
              position = v.positions[math.random(1,#v.positions)]
            }

            table.insert(mdata.steps, step)
          end

          vRP.startMission(player,mdata)
        end
      end
    end
  end
  --------------brinks
  for k,v in pairs(cfg.brinks) do -- each repair perm def
    -- add missions to users
    local users = vRP.getUsersByPermission(k)
    for l,w in pairs(users) do
      local user_id = w
      local player = vRP.getUserSource(user_id)
      if vRP.getSpawns(user_id) > 0 and not vRP.hasMission(player) then
          -- build mission
          local mdata = {}
          mdata.name = lang.bankdriver({v.title})
          mdata.steps = {}

          -- build steps
          for i=1,v.steps do
            local step = {
              text = lang.bankdriver({v.title}).."<br />"..lang.reward({v.reward}),
              onenter = function(player, area)
                    Mclient.freezePedVehicle(player,true)
					 if i == 1 then
					  vRPclient._playAnim(player,false,{task="CODE_HUMAN_POLICE_INVESTIGATE"},false)
                      vRPclient.notify(player,"~g~ ~g~ Aguarde carregando o dinheiro.")
                    end
                    if i >= 2 and i <= 10 then
					vRPclient._playAnim(player,false,{task="CODE_HUMAN_POLICE_INVESTIGATE"},false)
                      vRPclient.notify(player,"~g~ Aguarde descarregar o dinheiro no cofre.")
                    end
                    SetTimeout(20000, function()
                      vRP.nextMissionStep(player)
                      Mclient.freezePedVehicle(player,false) 
                      vRPclient._stopAnim(player,false)					  
					  if i == 1 then
					        vRP.giveInventoryItem(user_id,"bank_money",9000,false)
							vRPclient.notify(player,"~g~ Dinheiro carregado no carro forte.")
                      end
					  
					  if i == 2 then
					  vRP.tryGetInventoryItem(user_id,"bank_money",1000,false)
							vRPclient.notify(player,"~g~ Dirija até o próximo Banco.")
                      end
					  
					   if i == 3 then
					   vRP.tryGetInventoryItem(user_id,"bank_money",1000,false)
							vRPclient.notify(player,"~g~ Dirija até o próximo Banco.")
                      end
					  
					   if i == 4 then
					   vRP.tryGetInventoryItem(user_id,"bank_money",1000,false)
							vRPclient.notify(player,"~g~ Dirija até o próximo Banco.")
                      end
					  
					   if i == 5 then
					  vRP.tryGetInventoryItem(user_id,"bank_money",1000,false)
							vRPclient.notify(player,"~g~ Dirija até o próximo Banco.")
                      end
					  
					   if i == 6 then
					  vRP.tryGetInventoryItem(user_id,"bank_money",1000,false)
							vRPclient.notify(player,"~g~ Dirija até o próximo Banco.")
                      end
					  
					   if i == 7 then
					  vRP.tryGetInventoryItem(user_id,"bank_money",1000,false)
							vRPclient.notify(player,"~g~ Dirija até o próximo Banco.")
                      end
					  
					   if i == 8 then
					  vRP.tryGetInventoryItem(user_id,"bank_money",1000,false)
							vRPclient.notify(player,"~g~ Dirija até o próximo Banco.")
                      end
					  
					   if i == 9 then
					  vRP.tryGetInventoryItem(user_id,"bank_money",1000,false)
							vRPclient.notify(player,"~g~ Dirija até o próximo Banco.")
                      end
					  
                    if i == 10 then
					vRP.tryGetInventoryItem(user_id,"bank_money",1000,false)
                        local deleted = Mclient.isPlayerInVehicleModel(player,"stockade",-5.0)
					   if deleted then
                            vRPclient.notify(player,"~y~ Por hoje e so isso.")
                            vRP.giveMoney(user_id,v.reward)
						  vRPclient.notify(player,lang.money.received({v.reward}))
                        else
                           vRPclient.notify(player,"~r~ Voce precisa de um Carro Forte")
                   end
				  end
                  end)
              end,
              position = v.positions[i]
            }

            table.insert(mdata.steps, step)
          end
           local is_in = Mclient.isPlayerInVehicleModel(player,"stockade")
            if is_in then
              vRP.startMission(player,mdata)
            end
      end
    end
  end

   for k,v in pairs(cfg.trem) do -- each repair perm def
    -- add missions to users
    local users = vRP.getUsersByPermission(k)
    for l,w in pairs(users) do
      local user_id = w
      local player = vRP.getUserSource(user_id)
      if vRP.getSpawns(user_id) > 0 and not vRP.hasMission(player) then
          -- build mission
          local mdata = {}
          mdata.name = v.title
          mdata.steps = {}

          -- build steps
          for i=1,v.steps do
            local step = {
              text = v.text.."<br />"..lang.reward({v.reward}),
              onenter = function(player, area)
				local is_in = Mclient.isPlayerInVehicleModel(player,"freight")
                  if is_in then
				 Mclient.freezePedVehicle(player,true)
					if i == 1 then
                      vRPclient.notify(player,"~g~ aguarde os passageiros entrarem no trem.")
                    end
					if i == 2 then
                      vRPclient.notify(player,"~g~ aguarde os passageiros entrarem no trem.")
                    end
					if i == 3 then
                      vRPclient.notify(player,"~g~ aguarde os passageiros entrarem no trem.")
                    end
					if i == 4 then
                      vRPclient.notify(player,"~g~ aguarde os passageiros entrarem no trem.")
                    end
					if i == 5 then
                      vRPclient.notify(player,"~g~ aguarde os passageiros Sairem no trem.")
                    end
					if i == 6 then
                      vRPclient.notify(player,"~g~ aguarde os passageiros Sairem no trem.")
                    end
                    SetTimeout(9000, function()
                      vRP.nextMissionStep(player)
					  Mclient.freezePedVehicle(player,false)  
					   if i == 1 then
							vRPclient.notify(player,"~g~ Va a proxima estação.")
                      end
					  if i == 2 then
							vRPclient.notify(player,"~g~ Va a proxima estação.")
                      end
					  if i == 3 then
							vRPclient.notify(player,"~g~ Va a proxima estação.")
                      end
					  if i == 4 then
							vRPclient.notify(player,"~g~ Va a proxima estação.")
                      end
					  if i == 5 then
							vRPclient.notify(player,"~g~ Va a proxima estação.")
                      end
					  
						  if i == 6 then
                            vRPclient.notify(player,"~y~ Estação Final.")
                            vRP.giveMoney(user_id,v.reward)
						   vRPclient.notify(player,lang.money.received({v.reward}))
                        else
                    end
                   end)
                  end
              end,
              position = v.positions[i]
            }

            table.insert(mdata.steps, step)
          end
          local is_in = Mclient.isPlayerInVehicleModel(player,"freight")
            if is_in then
              vRP.startMission(player,mdata)
            end
      end
    end
  end
	for k,v in pairs(cfg.busurb1) do -- each repair perm def
    -- add missions to users
    local users = vRP.getUsersByPermission(k)
    for l,w in pairs(users) do
      local user_id = w
      local player = vRP.getUserSource(user_id)
      if vRP.getSpawns(user_id) > 0 and not vRP.hasMission(player) then
          -- build mission
          local mdata = {}
          mdata.name = v.title
          mdata.steps = {}

          -- build steps
          for i=1,v.steps do
            local step = {
              text = v.text.."<br />"..lang.reward({v.reward}),
              onenter = function(player, area)
				local is_in = Mclient.isPlayerInVehicleModel(player,"bus")
                  if is_in then
                    Mclient.freezePedVehicle(player,true)
                    vRPclient.notify(player,"~g~ Aguarde os Passageiros subirem ou descerem!")
                    SetTimeout(8000, function()
                      vRP.nextMissionStep(player)
                      Mclient.freezePedVehicle(player,false)
                      vRPclient.notify(player,"~y~ Dirija até a próxima parada.")
                      -- last step
                     if i == v.steps then
                        Mclient.deleteVehiclePedIsIn(player,{})
                        vRPclient.notify(player,"~y~ Por hoje e so isso.")
                        vRP.giveMoney(user_id,v.reward)
					    vRPclient.notify(player,lang.money.received({v.reward}))
                      end
                    end)
                  else
                    vRPclient.notify(player,"~r~ Voce precisa de ser um Motorista de Onibus")
				  end
              end,
              position = v.positions[i]
            }

            table.insert(mdata.steps, step)
          end
          local is_in = Mclient.isPlayerInVehicleModel(player,"bus")
            if is_in then
              vRP.startMission(player,mdata)
            end
      end
    end
  end

end

Citizen.CreateThread(function()
  while true do
    Wait(60000)
	print(2)
    print('task_mission')
    task_mission()
  end
end)
