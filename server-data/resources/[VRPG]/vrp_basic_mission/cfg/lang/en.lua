
local lang = {
  repair = "Reparar {1}.",
  reward = "Recompensa: {1} $.",
  reward3 = "Recompensa: Mandado",
  peixeen = "Pescar.",
  reward2 = "Recompensa: Peixes.",
  bad = "Quimico test.",
  reward18 = "Recompensa: Orgaos.",
  reward19 ="Recompensa : Metanfetamina",
  colet = "Coletar Lixo.",
  bankdriver = "{1}.",
  romero = "Busque o corpo e leve para ser enterrado.",
  Ferro = "Colete os Minerios.",
  delivery = {
    title = "Delivery",
    item = "- {2} {1}"
  }
}

return lang
