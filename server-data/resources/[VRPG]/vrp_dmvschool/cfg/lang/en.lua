--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

local lang = {
  dmv = {
    police = {
	  check = "CNH | Checar",
	  take = "CNH | Cassar",
	  perm_ask = "police.vehicle",
      perm_take = "police.vehicle",
	  check_desc = "Verifique a licença da pessoa mais próximo.",
	  take_desc = "Casse a licença da pessoa mais próximo.",
	  ask = "Pedindo CNH...",
	  request = "Você deseja apresentar sua CNH?",
	  request_hide = "Ocultar CNH.",
	  confirm = "Tem certeza de que deseja obter a CNH?",
	  license = "<em>Npme: </em>{1}<br /><em>Sobrenome: </em>{2}<br /><em>Idade: </em>{3}<br /><em>RG n°: </em>{4}<br /><em>Telefone: </em>{5}<br /><em>Data: </em>{6}",
	  no_license = "~r~Esta pessoa não possui CNH!",
	  took_license = "~g~A CNH foi cassada.",
	  license_taken = "~r~Sua CNH foi cassada!"
    },
  },
  client = {
    locked = "~r~Bloqueado",
	failed = "Você falhou\nPor ter acumulado {1} ~r~erros",
	passed = "You passou\nTeve {1} ~r~erros",
	proceed = "~b~Passou no teste teórico, pode seguir para o teste prático",
	try_again = "~r~Você falhou no teste, você pode tentar novamente outro dia",
	no_license = "~r~Você está dirigindo sem CNH",
	interact = "Pressione ~INPUT_CONTEXT~ para falar com o funcionário da ~y~Auto Escola",
	welcome = "~b~Bem vindo a ~h~Auto Escola!",
	menu = {
	  obtain = "Obter CNH",
	  intro = "Introdução    FREE",
	  theory = "Teste Teórico	R$ 500",
	  practical = "Teste Prático	R$ 1300",
	  mreturn = "Voltar",
	  mclose = "Sair",
	},
	control = {
	  enabled = "~y~Cruise Control: ~g~ligado\n~s~velocidade MÁXIMA {1}kmh",
	  disabled = "~y~Cruise Control: ~r~desligado",
	  help = "Ajuste sua velocidade máxima com ~INPUT_CELLPHONE_UP~ ~INPUT_CELLPHONE_DOWN~",
	  speed = "Velocidade Máxima ajusatada para {1}kmh",
	  cant_leave = "Você não pode deixar o veículo durante o teste",
	  not_dirving = "Você precisa estar dirigindo para realizar esta ação",
	},
    pnotify = {
      speeding = "Você está correndo! <b style='color:#B22222'>Vá devagar!</b><br /><br />Você está dirigindo em uma zona de <b style='color:#DAA520'>{1} km/h</b>!",
	  damaged = "Você  <b style='color:#B22222'>bateu o veículo!</b><br /><br />cuidado!",
	  area = "Area: ~y~{1}\n~s~Limite de velocidade: ~y~{2} km/h\n~s~Pontos de erro: ~y~{3}/{4}",
	},
  },
}

return lang