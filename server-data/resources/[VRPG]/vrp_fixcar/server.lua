local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")

AddEventHandler('chatMessage', function(source, name, msg)
	if msg == "/fix" then
	  local user_id = vRP.getUserId(source)
	  local player = vRP.getUserSource(user_id)
	  if vRP.hasPermission(user_id,"police.vehicle") then
		CancelEvent();
		TriggerClientEvent('murtaza:fix', source);	  
	  end
	end
end)