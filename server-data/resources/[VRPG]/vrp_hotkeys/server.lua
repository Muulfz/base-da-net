local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")
vRPhk = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
HKclient = Tunnel.getInterface("vrp_hotkeys")
Tunnel.bindInterface("vrp_hotkeys",vRPhk)
-- load global and local languages
lcfg = module("vrp", "cfg/base")
Luang = module("vrp", "lib/Luang")
Lang = Luang()
Lang:loadLocale(lcfg.lang, module("vrp", "cfg/lang/"..lcfg.lang) or {})
lang = Lang.lang[lcfg.lang]
-- USE FOR NECESSARY SERVER FUNCTIONS

function vRPhk.toggleHandcuff()
  Citizen.CreateThread(function()
    local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
    if vRP.hasPermission(user_id,"hotkey.handcuff") then
      local nplayer = vRPclient.getNearestPlayer(player,10)
	  if nplayer then
        vRPclient.toggleHandcuff(nplayer)
        vRP.closeMenu(nplayer)
      else
        vRPclient.notify(player,lang.common.no_player_near())
      end
    end
  end)
end

function vRPhk.docsOnline()
  local docs = vRP.getUsersByPermission("emergency.revive")
  return #docs
end

function vRPhk.canSkipComa()
  local user_id = vRP.getUserId(source)
  return vRP.hasPermission(user_id,"coma.skipper"), vRP.hasPermission(user_id,"coma.caller")
end


function vRPhk.helpComa(x,y,z)
  vRP.sendServiceAlert(source,"emergency",x,y,z,"Help! I've fallen and can't get up!") -- people will change this message anyway haha
end

local player_lists = {}
function vRPhk.openUserList()
  local player = source
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if player_lists[player] then -- hide
      player_lists[player] = nil
      vRPclient.removeDiv(player,"user_list")
    else -- show
      local content = "<img src=\"https://i.imgur.com/BWJkeD5.png\" style=\"height:80px;widht:100px;margin-left:210px;\"><br />            <span class=\"id\">RG</span>             <span class=\"name\">NOME COMPLETO</span>"
      local count = 0
	  local users = vRP.getUsers()
      for k,v in pairs(users) do
        count = count+1
        local user_source = vRP.getUserSource(k)
        local identity = vRP.getUserIdentity(k)
		  if user_source ~= nil and vRP.getPlayerName(user_source) ~= "unknown" then
            content = content.."            <br />            <span class=\"id\">"..k.."</span>"
            if identity then
              content = content.."                  <span class=\"name\">"..htmlEntities.encode(identity.firstname).." "..htmlEntities.encode(identity.name).."</span>"
            end
          end
		  
          -- check end
          count = count-1
          if count == 0 then
            player_lists[player] = true
            local css = [[
              .div_user_list{ 
                margin: auto; 
				        text-align: left;
                padding: 8px; 
                width: 650px; 
                margin-top: 100px; 
                background: rgba(36, 40, 45,0.7); 
                color: white; 
                font-weight: bold; 
                font-size: 1.1em;

              } 
              .div_user_list span{ 
				display: inline-block;
				text-align: center;
              } 
              .div_user_list .id{ 
                color: #fff;
                width: 45px; 
              }
              .div_user_list .pseudo{ 
                color:#fff;
                width: 145px; 
              }
              .div_user_list .name{ 
                color:#fff;
                width: 295px; 
              }
            ]]
            vRPclient.setDiv(player,"user_list", css, content)
          end
      end
    end
  end
end