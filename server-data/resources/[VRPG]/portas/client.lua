local LockHotkey = {0,182}

doorList = {
    -- Mission Row Armory
    [1] = { ["ishash"] = 1, ["objName"] = 749848321, ["x"]= 461.2865, ["y"]= -985.3206,["z"]= 30.83926,["locked"]= true,["txtX"]=461.507,["txtY"]=-985.9053,["txtZ"]=31.6895},
    -- Mission Row Captain Office
    [2] = { ["ishash"] = 0, ["objName"] = "v_ilev_ph_gendoor002", ["x"]= 447.23818969727, ["y"]= -980.63006591797,["z"]= 30.689598083496,["locked"]= true,["txtX"]=447.200,["txtY"]=-980.010,["txtZ"]=31.739},    
}

local firstLoad = false
function DrawText3d(x,y,z, text)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    
    if onScreen then
        SetTextScale(0.2, 0.2)
        SetTextFont(0)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, 255)
        SetTextDropshadow(0, 0, 0, 0, 55)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
         SetDrawOrigin(x,y,z, 0)
         DrawText(0.0, 0.0)
         ClearDrawOrigin()
    end
end

Citizen.CreateThread(function()
    

    while firstLoad == false do
	TriggerServerEvent('door:load')
	Citizen.Wait(1000)
	end
	
end)

RegisterNetEvent('door:statusSend')
AddEventHandler('door:statusSend', function(i, status)
doorList[i]["locked"]=status
local closeDoor = GetClosestObjectOfType(doorList[i]["x"], doorList[i]["y"], doorList[i]["z"], 1.0, hash, false, false, false)
locked, heading = GetStateOfClosestDoorOfType(hash, doorList[i]["x"], doorList[i]["y"], doorList[i]["z"], locked, heading)
if heading < -0.01 or heading > 0.01 then
			syncFail = true
			else
			syncFail = false
			end
	if doorList[i]["locked"] == true and syncFail == true then
				FreezeEntityPosition(closeDoor, false)
				else
			FreezeEntityPosition(closeDoor, doorList[i]["locked"])		
				end	
end)


RegisterNetEvent('door:loadSend')
AddEventHandler('door:loadSend', function(doorL)
for i,v in pairs(doorL) do
doorList[i]["locked"]=v.freeze
local closeDoor = GetClosestObjectOfType(doorList[i]["x"], doorList[i]["y"], doorList[i]["z"], 1.0, hash, false, false, false)
locked, heading = GetStateOfClosestDoorOfType(hash, doorList[i]["x"], doorList[i]["y"], doorList[i]["z"], locked, heading)
if heading < -0.01 or heading > 0.01 then
			syncFail = true
			else
			syncFail = false
			end
	if doorList[i]["locked"] == true and syncFail == true then
				FreezeEntityPosition(closeDoor, false)
				else
			FreezeEntityPosition(closeDoor, doorList[i]["locked"])		
				end			
end
firstLoad = true
end)

Citizen.CreateThread(function()
    

    while true do
        for i = 1, #doorList do
            local playerCoords = GetEntityCoords( GetPlayerPed(-1) )
			if doorList[i]["ishash"] == 0 then
			hash = GetHashKey(doorList[i]["objName"])
			else
			hash = doorList[i]["objName"]
			end
            local closeDoor = GetClosestObjectOfType(doorList[i]["x"], doorList[i]["y"], doorList[i]["z"], 1.0, hash, false, false, false)
            --SetEntityMaxHealth(closeDoor, 1000)
            local objectCoordsDraw = GetEntityCoords( closeDoor )            
            local playerDistance = GetDistanceBetweenCoords(playerCoords.x, playerCoords.y, playerCoords.z, doorList[i]["x"], doorList[i]["y"], doorList[i]["z"], true)
                  locked, heading = GetStateOfClosestDoorOfType(hash, doorList[i]["x"], doorList[i]["y"], doorList[i]["z"], locked, heading)       
            if(playerDistance < 1.15) then
                
                SetEntityHealth(closeDoor, 1000)
				ClearEntityLastDamageEntity(closeDoor)
				SetEntityCanBeDamaged(closeDoor, false)
				if doorList[i]["locked"] == true then
                    DrawText3d(doorList[i]["txtX"], doorList[i]["txtY"], doorList[i]["txtZ"], "~b~Porta ~r~Fechada")
                else
                    DrawText3d(doorList[i]["txtX"], doorList[i]["txtY"], doorList[i]["txtZ"], "~b~Porta ~g~Aberta")
                end
				
                
                if IsControlJustPressed(table.unpack(LockHotkey)) then
                    if doorList[i]["locked"] == true then
                        --FreezeEntityPosition(closeDoor, false)
						
							if(i==10 or i==11) then
                            TriggerServerEvent('door:status', 10, false)
							TriggerServerEvent('door:status', 11, false)
                        elseif(i==14 or i==15) then
                            TriggerServerEvent('door:status', 14, false)
							TriggerServerEvent('door:status', 15, false)
							 elseif(i==12 or i==13) then
                            TriggerServerEvent('door:status', 12, false)
							TriggerServerEvent('door:status', 13, false)
                        elseif(i==9 or i==21) then
                            TriggerServerEvent('door:status', 9, false)
							TriggerServerEvent('door:status', 21, false)
                        else
                            TriggerServerEvent('door:status', i, false)
                        end
                    else
                        --FreezeEntityPosition(closeDoor, true)
						if(i==10 or i==11) then
                            TriggerServerEvent('door:status', 10, true)
							TriggerServerEvent('door:status', 11, true)
                        elseif(i==14 or i==15) then
                            TriggerServerEvent('door:status', 14, true)
							TriggerServerEvent('door:status', 15, true)
							 elseif(i==12 or i==13) then
                            TriggerServerEvent('door:status', 12, true)
							TriggerServerEvent('door:status', 13, true)
                        elseif(i==9 or i==21) then
                            TriggerServerEvent('door:status', 9, true)
							TriggerServerEvent('door:status', 21, true)
                        else
                            TriggerServerEvent('door:status', i, true)
                        end

                    end
                end
			if heading < -0.01 or heading > 0.01 then
			syncFail = true
			else
			syncFail = false
			end
			
			
			
			
			if firstLoad == true then
				if doorList[i]["locked"] == true and syncFail == true then
				FreezeEntityPosition(closeDoor, false)
				else
			FreezeEntityPosition(closeDoor, doorList[i]["locked"])		
				end			
            end
			else
			if heading < -0.01 or heading > 0.01 then
			syncFail = true
			else
			syncFail = false
			end
			if doorList[i]["locked"] == true and syncFail == true then
				FreezeEntityPosition(closeDoor, false)
				else
			FreezeEntityPosition(closeDoor, doorList[i]["locked"])		
				end	
			
            end
			
        end

        Citizen.Wait(0)
    end
end)


