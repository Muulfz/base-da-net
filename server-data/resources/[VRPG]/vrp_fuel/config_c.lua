lang = "en"
-- lang = "fr"

settings = {}
settings["en"] = {
	openMenu = "Pressione/Press ~g~E~w~ Abrir o menu/to open the menu.",
	electricError = "~r~Carro eletrico/You have an electric vehicle.",
	fuelError = "~r~estacione corretamente/You're not in the good place.",
	buyFuel = "Comprar Combustivel/buy fuel",
	liters = "Litros/liters",
	percent = "porcentagem/percent",
	confirm = "Confirmar/Confirm",
	fuelStation = "Posto/Fuel station",
	boatFuelStation = "/Posto de Barco/Fuel station | Boat",
	avionFuelStation = "Posto aviao/Fuel station | Plane ",
	truckFuelStation = "Posto Truck/Fuel station | Truck ",
	heliFuelStation = "Posto helis/Fuel station | Helicopter",
	price = "Preço/price"
}

settings["fr"] = {
	openMenu = "Appuyez sur ~g~E~w~ pour ouvrir le menu.",
	electricError = "~r~Vous avez une voiture électrique.",
	fuelError = "~r~Vous n'êtes pas au bon endroit.",
	buyFuel = "acheter de l'essence",
	liters = "litres",
	percent = "pourcent",
	confirm = "Valider",
	fuelStation = "Station essence",
	boatFuelStation = "Station d'essence | Bateau",
	avionFuelStation = "Station d'essence | Avions",
	heliFuelStation = "Station d'essence | Hélicoptères",
	price = "prix"
}



hud_form = 1 -- 1 : Vertical | 2 = Horizontal
hud_x = 0.175 
hud_y = 0.885

text_x = 0.2575
text_y = 0.975


electricityPrice = 1 -- NOT RANDOMED !!!
truckPrice = 6 -- NOT RANDOMED !!!