blacklistedModels = { -- Vehicles which doesn't need fuel
	"BMX",
	"CRUISER",
	"TRIBIKE2",
	"FIXTER",
	"SCORCHER",
	"TRIBIKE3",
	"TRIBIKE",
	"tornew",
	"blazerrp",
	"space",
	"trailrd",
	"hiluxf",
	"motocop",
	"rota",
	"mototor",
	"pai",
	"policiatrail",
	"poli",
	"ntrailtor2",
	"tornew1",
	"a380",
	"Torcar",
	"tigerrocam1",
	"freight",
	"voyagetor",
	"nhiluxrota",
	"ntrailrota",
	"etiostor",
	"corollator1",
	"cprotection",
	"tug",
	"rotam"
}


electric_model = {
	"VOLTIC",
	"SURGE",
	"DILETTANTE",
	"KHAMELION",
	"freight",
	"CADDY",
	"CADDY2",
	"AIRTUG"
}

plane_model = {
	"BESRA",
	"CARGOPLANE",
	"CUBAN800",
	"DODO",
	"DUSTER",
	"HYDRA",
	"JET",
	"a380",
	"LUXOR",
	"LUXOR2",
	"STUNT",
	"MAMMATUS",
	"MILJET",
	"NIMBUS",
	"LAZER",
	"SHAMAL",
	"TITAN",
	"VELUM",
	"VELUM2",
	"VESTRA"
}


heli_model = {
	"ANNIHILATOR",
	"BUZZARD",
	"BUZZARD2",
	"CARGOBOB",
	"CARGOBOB2",
	"CARGOBOB3",
	"as350",
	"heliems",
	"FROGGER",
	"FROGGER2",
	"MAVERICK",
	"POLMAV",
	"SAVAGE",
	"SKYLIFT",
	"SVOLITO",
	"SVOLITO2",
	"SWIFT",
	"SWIFT2",
	"VALKYRIE",
	"VALKYRIE2",
	"VOLATUS"
}