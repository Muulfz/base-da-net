
local cfg = {}

cfg.on_sound = "sounds/radio_on.ogg" 
cfg.off_sound = "sounds/radio_off.ogg" 

-- list of list of groups (each list define a channel of speaker/listener groups, an ensemble)
cfg.channels = {
  {"Recruta PMESP", "Soldado PMESP", "Cabo PMESP", "3°Sargento PMESP", "2°Sargento PMESP", "1°Sargento PMESP", "2°Tenente PMESP", "1°Tenente PMESP", "T.Coronel PMESP", "Coronel PMESP", "Rocam PMESP", "Águia PMESP"}
}

return cfg
