-- define items, see the Inventory API on github

local cfg = {}

-- see the manual to understand how to create parametric items
-- idname = {name or genfunc, description or genfunc, genfunc choices or nil, weight or genfunc}
-- a good practice is to create your own item pack file instead of adding items here
cfg.items = {
  ["folha"] = {"Planta de maconha", "Para processar.", nil, 1.00}, -- no choices
  ["cannabis"] = {"Maconha", "Para vender.", nil, 1.00},
  ["crystalmelamine"] = {"Cristal de metanfetamina", "Para processar.", nil, 1.00}, -- no choices
  ["metanfetamina"] = {"Metanfetamina", "Para vender.", nil, 0.50}, -- no choices  
  ["folhadecoca"] = {"Folha de Coca", "Para processar.", nil, 1.00},
  ["cocaina"] = {"Cocaina", "Para vender.", nil, 0.50}, -- no choices 
  ["folhadecoca"] = {"Folha de Coca", "Para processar.", nil, 1.00},
  ["cocaina"] = {"Cocaina", "Para vender.", nil, 0.50}, -- no choices
  ["rede"] = {"Rede", "Para pescar Tartaruga", nil, 0.50},
  ["Tartaruga"] = {"Tartaruga", "Para vender.", nil, 5.00},
  ["ak"] = {"Peças para ak", "Para fabricar armas.", nil, 5.00},
  ["m4"] = {"Peças para m4", "Para fabricar armas.", nil, 10.00},
  ["ak1"] = {"Ak para venda", "Para vender.", nil, 5.00},
  ["m41"] = {"M4 para venda", "Para vender.", nil, 10.00},
  ["osso"] = {"Ossos Humanos", "Para limpar.", nil, 1.00},
  ["corpo"] = {"Pedaço de corpo Humano", "Para vender.", nil, 3.00},
  ["osso1"] = {"Ossos Humanos limpos", "Para limpar.", nil, 1.00},
  ["corpo1"] = {"Pedaço de corpo Humano limpos", "Para vender.", nil, 3.00},
  ["oab"] = {"OAB", "Carteira de Advogado.", nil, 0.01}, -- no choices
  ["bank_money"] = {"Dinheiro do Banco", "$.", nil, 0}, -- no choices
  ["picareta"] = {"Picareta", "Para minerar diamante ou ouro.", nil, 1.00},
  ["lsdpro"] = {"LSD", "Para vender.", nil, 0.50}
}

-- load more items function
local function load_item_pack(name)
  local items = module("cfg/item/"..name)
  if items then
    for k,v in pairs(items) do
      cfg.items[k] = v
    end
  else
    print("[vRP] item pack ["..name.."] not found")
  end
end

-- PACKS
load_item_pack("required")
load_item_pack("food")
load_item_pack("drugs")
load_item_pack("police")

return cfg
