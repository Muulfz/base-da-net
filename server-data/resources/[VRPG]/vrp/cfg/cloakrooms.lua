-- this file configure the cloakrooms on the map

local cfg = {}

-- prepare surgeries customizations
local surgery_male = { model = "mp_m_freemode_01" }
local surgery_female = { model = "mp_f_freemode_01" }
-- elite policia fardas
local uniforme_pm = { model = "s_m_y_hwaycop_01" }
local uniforme_pmf = { model = "s_m_m_armoured_01" }
local uniforme_tatica = { model = "s_m_m_prisguard_01" } 
local uniforme_capitao = { model = "s_m_m_armoured_02" }
local uniforme_aguia = { model = "s_m_y_cop_01" }
local uniforme_rota = { model = "s_m_y_sheriff_01" }
local uniforme_rocam = { model = "s_m_y_pilot_01" }
local uniforme_bope = { model = "s_m_y_ranger_01" }
local uniforme_tatica = { model = "s_m_y_sheriff_01" }
local uniforme_tatica2 = { model = "s_m_y_swat_01" }
local uniforme_tatica3 = { model = "s_m_y_blackops_01" }
local uniforme_conven = { model = "s_m_y_blackops_02" }
local uniforme_conven2 = { model = "s_m_y_blackops_03" }
local uniforme_federal = { model = "s_m_m_fibsec_01" }
-- samu fardas
local uniforme_samu = { model = "s_m_m_paramedic_01" }
local uniforme_medica = { model = "s_f_y_scrubs_01" }
local uniforme_samufamale = { model = "samuFem" }
local uniforme_mai = { model = "Mai" }
local uniforme_samufem = { model = "samuFem" }
--Skins addons exemplo local skin1 = { model = "nomedaskinexemplo" }
-- pasta skinsaddons/stream/nome da skin 
local arqueiroverde = { model = "CWArrow" }

local cfg = {}

-- prepare surgeries customizations
local surgery_male = { model = "mp_m_freemode_01" }
local surgery_female = { model = "mp_f_freemode_01" }

for i=0,19 do
  uniforme_pm[i] = {0,0}
  uniforme_pmf[i] = {0,0}
  uniforme_capitao[i] = {0,0}
  uniforme_aguia[i] = {0,0}
  uniforme_samu[i] = {0,0}
  uniforme_rocam[i] = {0,0}
  uniforme_bope[i] = {0,0}
  uniforme_tatica[i] = {0,0}
  uniforme_rota[i] = {0,0}
  uniforme_tatica[i] = {0,0}
  surgery_female[i] = {0,0}
  surgery_male[i] = {0,0}
  uniforme_samu[i] = {0,0}
  uniforme_medica[i] = {0,0}
  uniforme_samufamale = {0,0}
  surgery_female[i] = {0,0}
  surgery_male[i] = {0,0}
end

-- cloakroom types (_config, map of name => customization)
--- _config:
---- permissions (optional)
---- not_uniform (optional): if true, the cloakroom will take effect directly on the player, not as a uniform you can remove
cfg.cloakroom_types = {
  ["Policia"] = {
    _config = { permissions = {"policia.cloakroom"} },
    --[[["Uniform"] = {
      [3] = {30,0},
      [4] = {25,2},
      [6] = {24,0},
      [8] = {58,0},
      [11] = {55,0},
      ["p2"] = {2,0}
    },]]
	["ROCAM 01"] = uniforme_pm,
    ["PM 01"] = uniforme_conven,
    ["PM 02"] = uniforme_conven2,
    ["TATICA 00"] = s_m_m_armoured_02,
    ["TATICA 01"] = uniforme_pmf,
    ["TATICA 02"] = uniforme_capitao,
    ["TATICA 03"] = uniforme_aguia,
	["Recruta"] = uniforme_recruta,
	["> Retirar Farda 2"] = surgery_male
	
  },
  
  ["Águia"] = {
    _config = { permissions = {"aguia.cloakroom"} },
    --[[["Uniform"] = {
      [3] = {30,0},
      [4] = {25,2},
      [6] = {24,0},
      [8] = {58,0},
      [11] = {55,0},
      ["p2"] = {2,0}
    },]]
	["Farda do Águia"] = uniforme_aguia,
	["> Retirar Farda 2"] = surgery_male
	
  },
  
  ["Rocam"] = {
    _config = { permissions = {"rocam.cloakroom"} },
    --[[["Uniform"] = {
      [3] = {30,0},
      [4] = {25,2},
      [6] = {24,0},
      [8] = {58,0},
      [11] = {55,0},
      ["p2"] = {2,0}
    },]]
	["Rocam"] = uniforme_rocam,
	["> Retirar Farda 2"] = surgery_male
	
  },
    
  
    ["Vestuario Samu"] = {
    _config = { permissions = {"samu.cloakroom"} },
    --[[["Male uniform"] = {
      [3] = {92,0},
      [4] = {9,3},
      [6] = {25,0},
      [8] = {15,0},
      [11] = {13,3},
      ["p2"] = {2,0}
    }]]
    ["Uniforme Homem"] = uniforme_samu,
	["Uniforme Mai"] = uniform_mai,
	["Uniforme Samu"] = uniforme_samufem,
    ["Uniforme Mulher"] = uniforme_samuf,
	["> Retirar Farda H"] = surgery_male,
	["> Retirar Farda M"] = surgery_female
  },
  
   ["Jornalista"] = {
    _config = { permissions = {"jornal.roupa"} },
    ["Uniforme Jornalista"] = uniforme_jornal,
	["> Retirar Roupa"] = surgery_male
  },
  
   ["Loja de Skins Platina"] = {
   _config = { permissions = {"platina.roupa"} },
    ["Arqueiro Verde"] = arqueiroverde,
	["> Retirar Roupa(H)"] = surgery_male,
	["> Retirar Roupa(M)"] = surgery_female
  },
  
  ["Loja de Skins Ouro"] = {
   _config = { permissions = {"ouro.roupa"} },
    ["Arqueiro Verde"] = arqueiroverde,
	["> Retirar Roupa(H)"] = surgery_male,
	["> Retirar Roupa(M)"] = surgery_female
  },
  
  
  ["Escolher Sexo"] = {
    _config = { not_uniform = true },
  ["Masculino"] = surgery_male,
  ["Feminino"] = surgery_female
  }
}

cfg.cloakrooms = {
  {"Policia",455.61999511719,-988.75787353516,30.689582824707},
  {"Águia",454.30078125,-993.05206298828,30.689582824707},
  {"Escolher Sexo",-1044.9356689453,-2744.7219238281,21.359405517578},
  {"Jornalista",-597.90808105469,-937.57763671875,23.864540100098},
  {"Loja de Skins Ouro",-31.633459091187,-1110.7105712891,26.42234992981},
  {"Skins Masculino",75.3451766967773,-1392.86596679688,29.3761329650879},---skinsshops
  {"Vestuario Samu",269.77987670898,-1363.4407958984,24.537780761719}
}

return cfg

