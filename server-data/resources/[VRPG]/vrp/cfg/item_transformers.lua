
local cfg = {}

-- define static item transformers
-- see https://github.com/ImagicTheCat/vRP to understand the item transformer concept/definition

cfg.item_transformers = {
  {
    name="Academia", -- menu name
    r=255,g=125,b=0, -- color
    max_units=10000,
    units_per_minute=10000,
    x=-1202.96252441406,y=-1566.14086914063,z=4.61040639877319, -- pos
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Força"] = { -- action name
        description="Aumente sua força e stamina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={}, -- items given per unit
        aptitudes={ -- optional
          ["physical.strength"] = 1 -- "group.aptitude", give 1 exp per unit
        }
      }
    }
  },
 
    {
    name="Campo de Maconha", -- menu name
    permissions = {"maconha.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=1928.3912353516,y=5094.52734375,z=42.179828643799, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Colher"] = { -- action name
        description="Colher Folhas de Maconha.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
      },
        products={ -- items given per unit
          ["folha"] = 1
        }
      }
    }
  },
  {
    name="Processamento de Maconha", -- menu name
    permissions = {"maconha.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=1037.5177001953,y=-3205.3498535156,z=-38.170272827148, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Cultivar"] = { -- action name
        description="Cultivar Folhas de Cannabis.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		  ["folha"] = 1
      },
        products={ -- items given per unit
          ["cannabis"] = 2
        }
      }
    }
  },
  {
    name="Venda de Maconha", -- menu name
    permissions = {"maconha.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=1385.1929931641,y=3659.4025878906,z=34.928833007813, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Vender"] = { -- action name
        description="Vender Maconha.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		  ["cannabis"] = 1
      },
        products={ -- items given per unit
          ["dirty_money"] = 120
        }
      }
    }
  },
  
     {
    name="Fabrica de Cristal", -- menu name
    permissions = {"meta.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=821.01416015625,y= -787.12261962891,z= 26.22559928894, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Fabricar"] = { -- action name
        description="Fabricar Cristal.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
      },
        products={ -- items given per unit
          ["crystalmelamine"] = 1
        }
      }
    }
  },
  {
    name="Laboratório de Metanfetamina", -- menu name
    permissions = {"meta.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=1003.5936889648,y=-3199.7741699219,z=-38.99312210083, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Produzir"] = { -- action name
        description="Produzir metanfetamina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		  ["crystalmelamine"] = 1
      },
        products={ -- items given per unit
          ["metanfetamina"] = 2
        }
      }
    }
  },
  {
    name="Venda de Metanfetamina", -- menu name
    permissions = {"meta.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=1366.234375,y=-755.63543701172,z=71.127288818359, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Vender"] = { -- action name
        description="Vender Metanfetamina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		  ["metanfetamina"] = 1
      },
        products={ -- items given per unit
          ["dirty_money"] = 145
        }
      }
    }
  },
  

  
   {
    name="Laboratorio de Cocaina", -- menu name
    permissions = {"coca.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=263.11465454102,y=4491.0258789063,z=63.075012207031, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Misturar"] = { -- action name
        description="Misturar ingredientes.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
      },
        products={ -- items given per unit
          ["folhadecoca"] = 1
        }
      }
    }
  },
  {
    name="Laboratorio de Producao de Cocaina", -- menu name
    permissions = {"coca.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=1090.8579101563,y=-3196.623046875,z=-38.993476867676, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Produzir"] = { -- action name
        description="Produzir Cocaina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		  ["folhadecoca"] = 1
      },
        products={ -- items given per unit
          ["cocaina"] = 2
        }
      }
    }
  },
  {
    name="Venda de Cocaina", -- menu name
    permissions = {"coca.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=380.94216918945,y=2575.9016113281,z=43.521034240723, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Vender"] = { -- action name
        description="Vender cocaina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		  ["cocaina"] = 1
      },
        products={ -- items given per unit
          ["dirty_money"] = 142
        }
      }
    }
  },
  
  
   -- PESCADOR DE TARTARUGA
  {
    name="Pescador de Tartaruga", -- menu name
    permissions = {"tartaruga.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=4096.7158203125,y=4468.9829101563,z=3.2716977596283, -- pos
    radius=7.0, height=2.5, -- area
    recipes = { -- items do menu
      ["Pescar"] = { -- action name
        description="Pescar Tartaruga com Rede.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["rede"] = 1
      },
        products={ -- items given per unit
          ["Tartaruga"] = 2
        }
      }
    }
  },
  
  {
    name="Vender Tartaruga", -- menu name
    permissions = {"tartaruga.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=-114.03987121582,y=-1579.4752197266,z=34.17712020874, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Vender Tartaruga"] = { -- action name
        description="", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["Tartaruga"] = 1
      },
        products={ -- items given per unit
          ["dirty_money"] = 450
        }
      }
    }
  },
  {
      name="Lavagem de Dinheiro",
      r=0,g=200,b=0, ---cores
      max_units=250, 
      units_per_minute=5,
      x=-1056.0727539063,y=-241.78691101074,z=44.021076202393, -- coordenas mudas aqui
      radius=2.5, height=1.5, -- area
      recipes = {
        ["Lavar Dinheiro"] = { -- action name
          description="Lavagem de Dinheiro", -- action description
          in_money=0, -- money taken per unit
          out_money=750, -- money earned per unit
          reagents={
            ["dirty_money"] = 1000
          }, -- items taken per unit
          products={}, -- items given per unit
      }
    }
  },
  
  {
    name="OAB", -- menu name
	permissions = {"advogado.oab"},
    r=255,g=125,b=0, -- color
    max_units=1,
    units_per_minute=1,
    x=-77.134468078613,y=-802.86267089844,z=243.40579223633,
    radius=2, height=1.0, -- area
    recipes = {
      ["OAB"] = { -- action name
       description="Carteira de Advogado.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
		["oab"] = 1
		}, -- items given per unit
        aptitudes={} -- optional
      }
    }
  },

  {
    name="Cemiterio", -- menu name
    permissions = {"orgaos.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x= -1763.0767822266,y=-262.72061157227,z=48.20885848999, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Procurar Orgaos"] = { -- action name
        description="Procurar Orgaos.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
      },
        products={ -- items given per unit
          ["osso"] = 1,
		  ["corpo"] = 1
        }
      }
    }
  },
  {
    name="Lavagem de Orgaos", -- menu name
    permissions = {"orgaos.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x= 983.04241943359,y=-2125.6850585938,z=30.475383758545, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Limpar orgaos"] = { -- action name
        description="Limpar Orgaos.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		  ["osso"] = 1,
		  ["corpo"] = 1
      },
        products={ -- items given per unit
          ["osso1"] = 1,
		  ["corpo1"] = 1
        }
      }
    }
  },
  {
    name="Venda de Orgaos", -- menu name
    permissions = {"orgaos.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=772.65124511719,y=-260.07208251953,z=68.945831298828, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Vender"] = { -- action name
        description="Vender Orgaos.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		 ["osso1"] = 1,
		 ["corpo1"] = 1
      },
        products={ -- items given per unit
          ["dirty_money"] = 210
         }
      }
    }
  },
  
  {
    name="Deposito de Peças de Armas", -- menu name
    permissions = {"arma.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x= -342.63458251953,y=6097.8623046875,z=31.311056137085, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Procurar"] = { -- action name
        description="Procurar peças.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
      },
        products={ -- items given per unit
          ["ak"] = 1,
		  ["m4"] = 1
        }
      }
    }
  },
  {
    name="Laboratorio de Registro de Armas", -- menu name
    permissions = {"arma.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x= 2434.560546875,y=4968.1840820313,z=42.34761428833, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Limpar"] = { -- action name
        description="Limpar Numero de serie.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		  ["ak"] = 1,
		  ["m4"] = 1
      },
        products={ -- items given per unit
          ["ak1"] = 1,
		  ["m41"] = 1
        }
      }
    }
  },
  {
    name="Venda de Armas", -- menu name
    permissions = {"arma.ilegal"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=250, 
    units_per_minute=5,
    x=-1146.5826416016,y=4940.3427734375,z=222.26866149902, -- pos -119.17678833008,-1486.1040039063,36.98205947876
    radius=2.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Vender"] = { -- action name
        description="Vender Armas.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
		 ["ak1"] = 1,
		 ["m41"] = 1
      },
        products={ -- items given per unit
          ["dirty_money"] = 320
        }
      }
    }
  }
}
  
-- define transformers randomly placed on the map
cfg.hidden_transformers = {
  ["weed plant"] = {
    def = {
      name="Weed Plant", -- menu name
      r=0,g=200,b=0, -- color
      max_units=50,
      units_per_minute=0,
      x=0,y=0,z=0, -- pos
      radius=0, height=0, -- area
      recipes = {
      }
    },
    positions = weedplants
  },
  ["gun warehouse"] = {
    def = {
      name="Gun Warehouse", -- menu name
      r=0,g=200,b=0, -- color
      max_units=50,
      units_per_minute=0,
      x=0,y=0,z=0, -- pos
      radius=0, height=0, -- area
      recipes = {
      }
    },
    positions = warehouses
  }
}

-- time in minutes before hidden transformers are relocated (min is 5 minutes)
cfg.hidden_transformer_duration = 30-- 12 hours -- 5*24*60 -- 5 days

-- configure the information reseller (can sell hidden transformers positions)
cfg.informer = {
  infos = {
    ["weed plant"] = 10000,
    ["gun warehouse"] = 25000
  },
  positions = {
    {1821.12390136719,3685.9736328125,34.2769317626953},
    {1804.2958984375,3684.12280273438,34.217945098877}
  },
  interval = 30, -- interval in minutes for the reseller respawn
  duration = 10, -- duration in minutes of the spawned reseller
  blipid = 133,
  blipcolor = 2
}

return cfg
