
local cfg = {}

-- define market types like garages and weapons
-- _config: blipid, blipcolor, permissions (optional, only users with the permission will have access to the market)

cfg.market_types = {
  ["Mercadinho"] = {
    _config = {blipid=52, blipcolor=2},

    -- list itemid => price
    -- Drinks
    ["leite"] = 2,
    ["agua"] = 2,
    ["Vinho"] = 4,
    ["Vinho"] = 8,
    ["sucodelaranja"] = 8,
    ["limonada"] = 14,
    ["vodka"] = 30,

    --Food
	["pao"] = 2,
    ["Pipoca"] = 2,
    ["sanduiche"] = 2,
    ["Espaguete"] = 8,
    ["Pizza"] = 65,
	["rosquinha"] = 4
  },
  ["drugstore"] = {
    _config = {blipid=51, blipcolor=2},
    ["medkit"] = 75
  },
  ["Loja de Materiais"] = {
    _config = {blipid=478, blipcolor=47},
    ["picareta"] = 80,
	["isca"] = 50,
	["rede"] = 125
  },
  ["Equipamentos Mecanicos"] = {
    _config = {blipid=402, blipcolor=2},
    ["repairkit"] = 5

  }
}

-- list of markets {type,x,y,z}

cfg.markets = {
  {"Loja de Materiais",841.1220703125,-1031.0849609375,28.194864273071},
  {"Loja de Materiais",22.106534957886,-1110.1981201172,29.797025680542},
  {"Loja de Materiais",-660.62573242188,-937.91595458984,21.829217910767},
  {"Loja de Materiais",-1308.5821533203,-395.20498657227,36.695774078369},
  {"Loja de Materiais",249.24723815918,-50.527931213379,69.941055297852},
  {"Loja de Materiais",-3169.0581054688,1087.947265625,20.838748931885},
  {"Loja de Materiais",-1115.0887451172,2697.7473144531,18.554132461548},
  {"Loja de Materiais",-327.87289428711,6083.1767578125,31.45477104187},
  {"Loja de Materiais",1696.3393554688,3759.3461914063,34.705318450928},
  {"Loja de Materiais",808.87762451172,-2155.171875,29.61901473999},
  {"Mercadinho",-47.522762298584,-1756.85717773438,29.4210109710693},
  {"Mercadinho",25.7454013824463,-1345.26232910156,29.4970207214355}, 
  {"Mercadinho",1135.57678222656,-981.78125,46.4157981872559}, 
  {"Mercadinho",1163.53820800781,-323.541320800781,69.2050552368164}, 
  {"Mercadinho",374.190032958984,327.506713867188,103.566368103027}, 
  {"Mercadinho",2555.35766601563,382.16845703125,108.622947692871}, 
  {"Mercadinho",2676.76733398438,3281.57788085938,55.2411231994629}, 
  {"Mercadinho",1960.50793457031,3741.84008789063,32.3437385559082},
  {"Mercadinho",1166.18151855469,2709.35327148438,38.15771484375}, 
  {"Mercadinho",547.987609863281,2669.7568359375,42.1565132141113}, 
  {"Mercadinho",1698.30737304688,4924.37939453125,42.0636749267578}, 
  {"Mercadinho",1729.54443359375,6415.76513671875,35.0372200012207}, 
  {"Mercadinho",-3243.9013671875,1001.40405273438,12.8307056427002}, 
  {"Mercadinho",-2967.8818359375,390.78662109375,15.0433149337769}, 
  {"Mercadinho",-3041.17456054688,585.166198730469,7.90893363952637}, 
  {"Mercadinho",-1820.55725097656,792.770568847656,138.113250732422}, 
  {"Mercadinho",-1486.76574707031,-379.553985595703,40.163387298584}, 
  {"Mercadinho",-1223.18127441406,-907.385681152344,12.3263463973999},
  {"Equipamentos Mecanicos",472.08847045898,-1310.7618408203,29.218572616577},
  {"Mercadinho",-707.408996582031,-913.681701660156,19.2155857086182}
}

return cfg
