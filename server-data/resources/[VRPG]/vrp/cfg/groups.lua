
local cfg = {}

-- define each group with a set of permissions
-- _config property:
--- title (optional): group display name
--- gtype (optional): used to have only one group with the same gtype per player (example: a job gtype to only have one job)
--- onspawn (optional): function(player) (called when the player spawn with the group)
--- onjoin (optional): function(player) (called when the player join the group)
--- onleave (optional): function(player) (called when the player leave the group)
--- (you have direct access to vRP and vRPclient, the tunnel to client, in the config callbacks)

 cfg.groups = {
  ["superadmin"] = {
    _config = {onspawn = function(player) vRPclient._notify(player,"Você é um ~p~SuperAdmin.") end},
    "player.group.add",
    "player.group.remove",
    "player.givemoney",
    "player.giveitem",
	"police.dv",
	"admin.deleteveh",
	"admin.spawnveh",
    "emergency.revive",
	"police.vehicle",
    "emergency.shop",
	"admin.teleporte",
	"emergency_heal"
  },
  ["admin"] = {
  _config = {onspawn = function(player) vRPclient._notify(player,"Você é um ~p~Administrador.") end},
    "admin.tickets",
    "admin.announce",
    "player.list",
    "player.whitelist",
    "player.unwhitelist",
    "player.kick",
    "player.ban",
	"police.dv",
    "player.unban",
    "player.noclip",
    "player.custom_emote",
	"armas.traficante",
    "player.custom_sound",
    "player.display_custom",
	"police.menu_interaction",
	"police.announce",
    "player.coords",
    "player.tptome",
	"radar.pass",
	"admin.godmode",
	"player.tptowaypoint",
	"player.cmd_mask",
	"admin.easy_unjail",
	"police.door",
	"police.license",
	"police.licensearm",
	"admin.spikes",
	"police.vehicle",
    "player.tpto"
  },
  ["moderador"] = {
  _config = {onspawn = function(player) vRPclient._notify(player,"Você é um ~g~Moderador.") end},
    "admin.tickets",
    "player.ban",
    "player.unban",
	"police.dv",
    "player.group.add",
    "player.group.remove",
	"player.whitelist",
	"player.unwhitelist",
    "player.noclip",
	"radar.pass",
	"police.door",
    "player.tptome",
    "player.list",
	"police.license",
	"police.licensearm",
	"police.menu_interaction",
    "player.kick",
	"police.vehicle",
    "player.tpto",
  },
  ["suporte"] = {
  _config = {onspawn = function(player) vRPclient._notify(player,"Você é um ~y~Suporte.") end},
    "admin.tickets",
	"player.whitelist",
	"player.unwhitelist",
    "player.noclip",
	"radar.pass",
	"police.door",
    "player.tptome",
    "player.list",
	"police.license",
	"police.licensearm",
	"police.menu_interaction",
    "player.kick",
	"police.vehicle",
    "player.tpto"
  },   
  -- the group user is auto added to all logged players
 ["user"] = {
    "police.menu",
    "player.phone",
	"player.loot",
	"player.store_armor",
	"player.fix_haircut",
	"player.emotes",
    "player.player_menu",
    "player.store_money",
    "player.store_weapons",
    "player.check",
	"player.menu",
	"player.fixhaircut",
	"player.inspect",
	"store.weapons",
    "police.check",
    "player.calladmin",
	"emergency_heal",
	"player.skip_coma",
	"favela.dom",
    "police.seizable" -- can be seized
  }, 
  ["streamer"] = {
   _config = {onspawn = function(player) vRPclient._notify(player,"Você é um ~p~Streamer.") end},
   gtype = "job",
    "streamer.paycheck",
	"streamer.vehicle"
  },
  ["divulgador"] = {
   _config = {onspawn = function(player) vRPclient._notify(player,"Você é um ~g~Divulgador.") end},
   gtype = "job",
    "divulgador.paycheck",
	"divulgador.vehicle"

  }, 
  ["vipgold"] = {
  _config = {onspawn = function(player) vRPclient._notify(player,"Você é um ~y~VIP Gold.") end},
   gtype = "job",
    "vipgold.paycheck",
	"police.dv",
	"radar.pass",
	"ouro.roupa",
	"vipgold.garagem",
	"donaterplatina.loadshop",
	"platina.roupa"
	
  }, 
  ["vipdiamond"] = {
  _config = {onspawn = function(player) vRPclient._notify(player,"Você é um ~p~VIP Diamond.") end},
   gtype = "job",
    "vipdiamond.paycheck",
	"police.dv",
	"radar.pass",
	"vipdiamond.garagem",
	"donaterouro.loadshop",
	"ouro.roupa"
	
  }, 
  ["vipplatinum"] = {
  _config = {onspawn = function(player) vRPclient._notify(player,"Você é um ~p~VIP Diamond.") end},
   gtype = "job",
    "vipplatinum.paycheck",
	"police.dv",
	"radar.pass",
	"vipplatinum.garagem",
	"donaterouro.loadshop",
	"ouro.roupa"
	
  }, 
  ["Prefeito"] = {
    _config = { gtype = "job" },
	"pref.loadshop",
	"faccao.loadshop",
	"tb.garagem",
	"faccao.loadshop",
	"donaterouro.vehicle",
	"donaterouro.loadshop",
    "donateplatina.paycheck",
	"donaterplatina.vehicle",
	"donaterplatina.loadshop",
	"prefeito.paycheck"
	
  },
    ["Recruta PMESP"] = {
    _config = { 
      gtype = "job",
      onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
	"police.drag",
	"police.bmfine",
	"police.bmjail",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
    "-police.store_weapons",
    "-police.seizable"
  },
  
  ["Soldado PMESP"] = {
    _config = { 
      gtype = "job",
       onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.soldado",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
	"police.drag",
	"police.bmfine",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
  ["Cabo PMESP"] = {
    _config = { 
      gtype = "job",
      onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.cabo",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
	"police.drag",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
	"police.bmfine",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
  ["3°Sargento PMESP"] = {
    _config = { 
      gtype = "job",
       onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.sargento",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
    "police.announce",
	"police.drag",
	"police.bmfine",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
    ["2°Sargento PMESP"] = {
    _config = { 
      gtype = "job",
       onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.sargento",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
    "police.announce",
	"police.drag",
	"police.bmfine",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
    ["1°Sargento PMESP"] = {
    _config = { 
      gtype = "job",
       onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.sargento",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
    "police.announce",
	"police.drag",
	"police.bmfine",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
  ["2°Tenente PMESP"] = {
    _config = { 
      gtype = "job",
      onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.tenente",
	"player.list",
	"police.dv",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
	"police.drag",
	"police.bmfine",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
  ["1°Tenente PMESP"] = {
    _config = { 
      gtype = "job",
       onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.capitao",
	 "player.list",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"police.dv",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
    "police.announce",
	"police.drag",
	"police.bmfine",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
  ["T.Coronel PMESP"] = {
    _config = { 
      gtype = "job",
      onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.major",
	"player.list",
	"bank.police",
	"police.dv",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
    "police.announce",
	"police.drag",
	"police.bmfine",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
  ["Coronel PMESP"] = {
    _config = { 
      gtype = "job",
       onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.coronel",
    "police.dv",
	"player.list",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
	"police.drag",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
	"ft.garagem",
	"ope.garagem",
	"aguia.garagem",
	"police.bmfine",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
    ["Rocam PMESP"] = {
    _config = { 
      gtype = "job",
       onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.coronel",
    "police.dv",
	"player.list",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
	"police.drag",
	"pm.garagem",
	"radar.pass",
	"policia.cloakroom",
	"ft.garagem",
	"ope.garagem",
	"aguia.garagem",
	"police.bmfine",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
  ["Águia PMESP"] = {
    _config = { 
      gtype = "job",
      onjoin = function(player) vRPclient.setCop(player,true) end,
      onspawn = function(player) vRPclient.setCop(player,true) end,
      onleave = function(player) vRPclient.setCop(player,false) end
    },
	"pm.aguia",
    "player.list",
	"bank.police",
	"police.bmcuff",
	"pm.recruta",
    "police.menu",
	"aguia.cloakroom",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
	"aguia.garagem",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
	"police.drag",
	"police.bmfine",
	"police.bmjail",
    "-police.store_weapons",
    "-police.seizable"
  },
  ["Paramedico"] = {
    _config = { gtype = "job" },
    "emergency.revive",
    "emergency.shop",
	"emergency_heal",
    "emergency.service",
	"hospital.garagem",
	"medic.weapons",
	"emergency.medkit",
	"samu.paycheck",
	"emscheck.revive",
	"samu.loadshop",
	"police.dv",
	"radar.pass",
	"samu.announce",
	"samu.cloakroom"
  },
  ["Águia SAMU"] = {
    _config = { gtype = "job" },
    "emergency.revive",
    "emergency.shop",
	"emergency_heal",
    "emergency.service",
	"aguiasamu.garagem",
	"hospital.garagem",
	"emergency.medkit",
	"samu.paycheck",
	"samu.loadshop",
	"police.dv",
	"emscheck.revive",
	"radar.pass",
	"samu.cloakroom",
	"samu.announce"
  },
  ["Comandante SAMU"] = {
    _config = { gtype = "job" },
	"player.group.add",
    "player.group.remove",
    "emergency.revive",
    "emergency.shop",
	"emergency_heal",
    "emergency.service",
	"hospital.garagem",
	"aguiasamu.garagem",
	"police.dv",
	"emergency.medkit",
	"cmdsamu.paycheck",
	"samu.loadshop",
	"emscheck.revive",
	"radar.pass",
	"samu.announce",
	"samu.cloakroom"
  },
  ["Mecânico"] = {
    _config = { gtype = "job"},
    "vehicle.repair",
    "vehicle.replace",
	"police.dv",
	"mecanico.caminhao",
	"repair.paycheck",
	"mission.repair.satellite_dishes",
	"mission.repair.wind_turbines",
    "repair.service",
	"repair.garagem"
  },
  ["Uber"] = {
    _config = { gtype = "job" },
    "taxi.service",
	"taxi.paycheck",
	"mission.taxi.passenger",
	"taxi.garagem"
  },
  ["Jornalista"] = {
    _config = { gtype = "job" },
	"jornalista.paycheck",
	"jornal.garagem",
	"jornal.roupa",
	"emprego.jornalista"
  },  
  ["Advogado"] = {
    _config = { gtype = "job" },
	"advogado.oab",
	"advogado.service",
	"emprego.Advogado"
  },  
  
  ["Motorista de Onibus"] = {
    _config = { gtype = "job" },
	"bus.garagem"
  },  
 
  ["Desempregado"] = {
    _config = { gtype = "job" },
	"player.paycheck"
  },
    ["Sedex"] = {
    _config = { gtype = "job" },
	"sedex.entrega",
	"sedex.garagem",
	"sedex.paycheck"
  },
  
   ["Traficante de Maconha"] = {
    _config = { gtype = "job" },
	"maconha.ilegal"
  },
  ["Traficante de Metanfetamina"] = {
    _config = { gtype = "job" },
	"meta.ilegal"
  },  
  ["Traficante de Cocaina"] = {
    _config = { gtype = "job" },
	"coca.ilegal"
  },  
  
  ["Traficante de Tartaruga"] = {
    _config = { gtype = "job" },
    "tartaruga.ilegal",
  }, 
  ["Traficante de Armas"] = {
    _config = { gtype = "job" },
	"arma.ilegal"
  }, 
  
  ["Traficante de Orgaos"] = {
    _config = { gtype = "job" },
	"orgaos.ilegal"
  }, 
  
  ["Pescador"] = {
    _config = { gtype = "job" },
	"vender.peixes",
	"pescar.peixes",
    "fisher.service",
	"fisher.vehicle",
	"pescador.paycheck",
	"traje.pescador"
  }
}

-- groups are added dynamically using the API or the menu, but you can add group when an user join here
cfg.users = {
  [1] = { -- give superadmin and admin group to the first created user on the database
    "superadmin",
    "admin"
  }
}

-- group selectors
-- _config
--- x,y,z, blipid, blipcolor, permissions (optional)

cfg.selectors = {
 -- ["Agencia de Empregos"] = {
 --   _config = {x = -268.363739013672, y = -957.255126953125, z = 31.22313880920410, blipid = 351, blipcolor = 47},
--	"Uber",
--	"Transportador de Valores",
 --   "Mecânico",
--	"Entregador",
 --   "Minerador",
--	"Pescador",
--	"Sedex",
 --   "Desempregado"
 -- },
 -- ["Jornalista"] = {
  --  _config = {x = -599.79998779297,y = -929.90466308594,z = 23.864562988281, blipid = 440, blipcolor = 49 ,permissions = {"emprego.jornalista"} },
--	"Jornalista"
 -- } 
}

return cfg

