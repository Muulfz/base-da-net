
local cfg = {}

-- PCs positions
cfg.pcs = {
  {1853.21, 3689.51, 34.2671},
  {442.030609130859, -978.72705078125, 30.6896057128906},
  {-448.97076416016,6012.4208984375,31.71639251709}
}

-- vehicle tracking configuration
cfg.trackveh = {
  min_time = 300, -- min time in seconds
  max_time = 600, -- max time in seconds
  service = "police" -- service to alert when the tracking is successful
}

-- wanted display
cfg.wanted = {
  blipid = 458,
  blipcolor = 38,
  service = "police"
}

-- illegal items (seize)
cfg.seizable_items = {
  "dirty_money",
  "cannabis",
  "metanfetamina",
  "cacaina",
  "clonedcards",
  "folhadecoca",
  "Tartaruga",
  "driver",
  "weed"
}

-- jails {x,y,z,radius}
cfg.jails = {
  {459.485870361328,-1001.61560058594,24.914867401123,2.1},
  {459.305603027344,-997.873718261719,24.914867401123,2.1},
  {459.999938964844,-994.331298828125,24.9148578643799,1.6}
}

-- fines
-- map of name -> money
cfg.fines = {
  ["Desacato."] = 500,
  ["Fuga."] = 2000,
  ["Sem habilitação."] = 2500,  
  ["Assalto a Civil."] = 4500,
  ["Porte ilegal de arma."] = 10000,
  ["Tráfico de drogas."] = 7500,
  ["Tráfico de tartarugas."] = 10000,
  ["Alta Velocidade."] = 4000,
  ["Direção Perigosa."] = 5000,
  ["Ultrapassar sinal vermelho."] = 300,
  ["Furto/Roubo de veículos."] = 7500,
  ["Poluição Sonora."] = 2500,
  ["Assasinato."] = 75000,
  ["Tentativa de homicídio."] = 50000,
  ["Roubo a Banco."] = 50000,
  ["Roubo a Loja."] = 12000
}--]]

return cfg
