
local cfg = {}

-- size of the sms history
cfg.sms_history = 15

-- maximum size of an sms
cfg.sms_size = 500

-- duration of a sms position marker (in seconds)
cfg.smspos_duration = 300

-- phone sounds (playAudioSource)
cfg.dialing_sound = "sounds/phone_dialing.ogg" -- loop
cfg.ringing_sound = "sounds/phone_ringing.ogg" -- loop
cfg.sms_sound = "sounds/phone_sms.ogg"

-- define phone services
-- blipid, blipcolor (customize alert blip)
-- alert_time (alert blip display duration in seconds)
-- alert_permission (permission required to receive the alert)
-- alert_notify (notification received when an alert is sent)
-- notify (notification when sending an alert)
cfg.services = {
  ["Policia"] = {
    blipid = 304,
    blipcolor = 38,
    alert_time = 300, -- 5 minutes
    alert_permission = "police.service",
    alert_notify = "~r~[COPOM] Denúncia, aceite para receber a QTH:~n~~s~",
    notify = "~b~Você fez um chamado para a policia! Aguarde.",
    answer_notify = "~b~A Policia esta indo ate voce!"
  },
  ["SAMU"] = {
    blipid = 153,
    blipcolor = 1,
    alert_time = 300, -- 5 minutes
    alert_permission = "emergency.service",
    alert_notify = "~r~[SAMU] Emergência, aceite para receber a QTH:~n~~s~",
    notify = "~b~Você chamou um paramédico! Aguarde..",
    answer_notify = "~b~O Paramedico este indo ate voce!"
  },
  ["Advogado"] = {
    blipid = 269,
    blipcolor = 68,
    alert_time = 300, -- 5 minutes
    alert_permission = "advogado.service",
    alert_notify = "~r~[Advogado] Alguém ligou para você solicitando serviço:~n~~s~",
    notify = "~b~Você chamou um advogado.",
    answer_notify = "~b~O Advogado está a caminho!"
  },  
  ["Uber"] = {
    blipid = 198,
    blipcolor = 5,
    alert_time = 300,
    alert_permission = "taxi.service",
    alert_notify = "~y~[UBER] Chamado para corrida! Deseja aceitar? :~n~~s~",
    notify = "~y~Você chamou um UBER.",
    answer_notify = "~y~O UBER está a caminho. Aguarde!"
  },
  ["Mecanico"] = {
    blipid = 446,
    blipcolor = 5,
    alert_time = 300,
    alert_permission = "repair.service",
    alert_notify = "~y~[Mecânico] Chamado de reparo! Deseja aceitar?:~n~~s~",
    notify = "~y~Você chamou um mecanico.",
    answer_notify = "~y~Mecânico está a caminho. Aguarde!"
  },
  ["Assassino de Aluguel"] = {
    blipid = 310,
    blipcolor = 76,
    alert_time = 300,
    alert_permission = "assassino.service",
    alert_notify = "~y~[Assassino] Missao de assassinato! Deseja aceitar?:~n~~s~",
    notify = "~y~Você chamou um assassino.",
    answer_notify = "~y~O assassino aceitou seu chamado."
	}
}

-- define phone announces
-- image: background image for the announce (800x150 px)
-- price: amount to pay to post the announce
-- description (optional)
-- permission (optional): permission required to post the announce
cfg.announces = {
  ["Admin"] = {
    --image = "nui://vrp_mod/announce_admin.png",
    image = "http://i.imgur.com/HnM1D8w.png",
    price = 0,
    description = "Uso exclusivo para administrador",
    permission = "admin.announce"
  },
  ["Policial"] = {
    --image = "nui://vrp_mod/announce_police.png",
    image = "http://i.imgur.com/cvzroo4.png",
    price = 0,
    description = "Uso exclusivo para policial.",
    permission = "police.announce"
  },
  ["Samu"] = {
    --image = "nui://vrp_mod/announce_police.png",
    image = "https://imgur.com/0vf4PKc.png",
    price = 0,	
    description = "Uso exclusivo para SAMU.",
    permission = "samu.announce"
  },
  ["Comercial"] = {
    --image = "nui://vrp_mod/announce_commercial.png",
    image = "http://i.imgur.com/QTDgtjF.png",
    description = "Anuncio Comercial (comprar, vender, trabalhar).",
    price = 2500
  },
  ["Festa"] = {
    --image = "nui://vrp_mod/announce_party.png",
    image = "http://i.imgur.com/mpsZ8SD.png",
    description = "Organizando uma festa? Convide todos para ir.",
    price = 2500
  }
}

return cfg
