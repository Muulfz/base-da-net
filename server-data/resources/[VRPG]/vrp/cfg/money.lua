
local cfg = {}

-- start wallet/bank values
cfg.open_wallet = 1000
cfg.open_bank = 5000

-- money display css
cfg.display_css = [[
.div_money{
	position: absolute;
	top: 113px;
	right: 1px;
	padding: 4px;
    width: 170px;
	font-family: 'Arial', sans-serif;
	font-size: 15px;
	font-weight: bold;
    max-width: 170px;
	text-align: center;
	display: inline-block;
	color: #262626;
	background-color: rgba(255, 255, 255,0.85);
    box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 1), 0px 1px 1px 0px rgba(0, 0, 0, 1);  
}
.div_bmoney{
	position: absolute;
	top: 138px;
	right: 1px;
	padding: 4px;
	display: inline-block;
	width: 170px;
    max-width: 170px;
	font-family: 'Arial', sans-serif;
	font-size: 15px;
	font-weight: bold;
	text-align: center;
	color: #262626;
	background-color: rgba(255, 255, 255,0.85);
    box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 1), 0px 1px 1px 0px rgba(0, 0, 0, 1); 
}
.div_money .symbol{
    content: url('https://i.imgur.com/nU7z8fN.png');
	max-height: 12px;
}
.div_bmoney .symbol{
    content: url('https://i.imgur.com/BfmjTrx.png');
	max-height: 12px;
}
]]

return cfg
