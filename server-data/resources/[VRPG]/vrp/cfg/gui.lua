
-- gui config file

local cfg = {}

-- additional css loaded to customize the gui display (see gui/design.css to know the available css elements)
-- it is not recommended to modify the vRP core files outside the cfg/ directory, create a new resource instead
-- you can load external images/fonts/etc using the NUI absolute path: nui://my_resource/myfont.ttf
-- example, changing the gui font (suppose a vrp_mod resource containing a custom font)
cfg.css = [[
@font-face {
  font-family: "Custom Font";
  src: url(nui://vrp_mod/customfont.ttf) format("truetype");
}

body{
  font-family: "Custom Font";
}

]]

cfg.static_menu_types = {
  ["policia_militar"] = {
      title = "Armario de Armas Basico", 
      blipid = 188, 
     blipcolor = 3,
    },
	["samu_equipamentos"] = {
      title = "Armario da samu", 
      blipid = 188, 
     blipcolor = 3,
      permissions = {
        "medic.weapons"
      }
    },
  ["dpentrar"] = {
    title = "Armas"
  },
  ["dpsair"] = { 
   title = "Armas"
   },
   ["hpentrar"] = {
    title = "Hospital"
  },
  ["hpsair"] = { 
   title = "Hospital"
   },
  ["samu_cura"] = {
    title = "Cura"
  }
}

cfg.static_menus = {
{"policia_militar", 458.73049926758,-979.09045410156,30.689580917358},
{"dpsair",453.5442199707,-982.6171875,30.689594268799}, 
{"dpentrar",451.98654174805,-982.60418701172,30.689580917358},
{"hpentrar",294.63272094727,-1448.5067138672,29.966617584229}, -- entra
{"hpsair",275.03616333008,-1360.9791259766,24.537801742554},
{"samu_cura",260.71044921875,-1357.9705810547,24.537790298462},
{"samu_equipamentos",268.33361816406,-1365.0842285156,24.537792205811}
}

cfg.voip_peer_configuration = {
  iceServers = {
    {urls = {"stun:stun.l.google.com:19302", "stun:stun1.l.google.com:19302", "stun:stun2.l.google.com:19302"}}
  }
}

return cfg
