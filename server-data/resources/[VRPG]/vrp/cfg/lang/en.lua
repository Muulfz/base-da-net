
-- define all language properties

local lang = {
  common = {
    welcome = "Bem vindo. Aperte K para abrir o menu.~n~ultimo login: {1}",
    no_player_near = "~r~Nenhum jogador perto de você.",
    invalid_value = "~r~Valor inválido.",
    invalid_name = "~r~Nome inválido.",
    not_found = "~r~Não encontrado.",
    request_refused = "~r~Pedido recusado.",
    wearing_uniform = "~g~Customização do Personagem.",
    not_allowed = "~r~Não permitido."
  },
  weapon = {
    pistol = "Pistola"
  },
  survival = {
    starving = "",
    thirsty = "",
  },
  money = {
    display = "<span class=\"symbol\">R$</span>{1}",
	bdisplay = "<span class=\"symbol\">R$</span>{1}",
    given = "Dar~r~{1}R$.",
    received = "Recebeu~g~{1}R$.",
    not_enough = "~r~Dinheiro insuficiente.",
    paid = "Pagou~r~{1}R$.",
    give = {
      title = "Dar dinheiro",
      description = "Quantidade para dar:.",
      prompt = "Quantidade para dar:"
    }
  },
  inventory = {
    title = "Inventário",
    description = "Abra o inventário.",
    iteminfo = "({1})<br /><br />{2}<br /><em>{3} kg</em>",
    info_weight = "peso {1}/{2} kg",
    give = {
      title = "Dar",
      description = "Dar um item para o jogador mais próximo.",
      prompt = "Quantidade para dar (max {1}):",
      given = "Dar ~r~{1} ~s~{2}.",
      received = "Recebeu ~g~{1} ~s~{2}.",
    },
    trash = {
      title = "Lixo",
      description = "Destruir item.",
      prompt = "Quantidade para destruir (max {1}):",
      done = "Destruido ~r~{1} ~s~{2}."
    },
    missing = "~r~Está Faltando {2} {1}.",
    full = "~r~Inventário cheio.",
    chest = {
      title = "Baú",
      already_opened = "~r~Este baú já foi aberto por outra pessoa.",
      full = "~r~Baú cheio..",
      take = {
        title = "Pegar",
        prompt = "Quantidade para pegar (max {1}):"
      },
      put = {
        title = "Colocar",
        prompt = "Quantidade para colocar (max {1}):"
      }
    }
  },
  atm = {
    title = "Caixa Eletrônico",
    info = {
      title = "Informações",
      bank = "Banco: {1} R$"
    },
    deposit = {
      title = "Depositar",
      description = "Carteira para o banco",
      prompt = "Insira o valor do depósito:",
      deposited = "~r~{1}R$~s~ Depositado."
    },
    withdraw = {
      title = "Retirar",
      description = "Banco para Carteira",
      prompt = "Insira o valor da retirada:",
      withdrawn = "~g~{1}R$ ~s~Retirou.",
      not_enough = "~r~Você não tem dinheiro suficiente no banco."
    }
  },
  business = {
    title = "Câmara de Comércio",
    directory = {
      title = "Diretoria",
      description = "Lista de Empresas",
      dprev = "> Ante",
      dnext = "> Prox",
      info = "<em>capital: </em>{1} $<br /><em>owner: </em>{2} {3}<br /><em>registration n°: </em>{4}<br /><em>phone: </em>{5}"
    },
    info = {
      title = "Business info",
      info = "<em>name: </em>{1}<br /><em>capital: </em>{2} $<br /><em>capital transfer: </em>{3} $<br /><br/>Capital transfer is the amount of money transfered for a business economic period, the maximum is the business capital."
    },
    addcapital = {
      title = "Add capital",
      description = "Add capital para sua empresa.",
      prompt = "Quantidade a adicionar ao capital de negócios:",
      added = "~r~{1}$ ~s~adicionado ao capital de sua empresa."
    },
    launder = {
      title = "Lavagem de dinheiro",
      description = "Use sua empresa para lavar dinheiro sujo.",
      prompt = "Quantidade de dinheiro sujo para lavar (max {1} $): ",
      laundered = "~g~{1}$ ~s~lavado.",
      not_enough = "~r~Não há dinheiro sujo."
    },
    open = {
      title = "Abrir empresa",
      description = "Para abrir uma empresa, mínimo de capital é {1} $.",
      prompt_name = "Nome da empresa (não da para mudar depois, maximo {1} chars):",
      prompt_capital = "Capital inicial (min {1})",
      created = "~g~Empresa criada."
      
    }
  },
  cityhall = {
    title = "Prefeitura",
    identity = {
      title = "Nova identidade",
      description = "Crie uma nova identidade, custa = {1} R$.",
      prompt_firstname = "Inserir seu Primeiro Nome:",
      prompt_name = "Inserir seu Primeiro Nome:",
      prompt_age = "Inserir sua idade:",
    },
    menu = {
      title = "Identidade",
      info = "<em>Name: </em>{1}<br /><em>Nome:: </em>{2}<br /><em>Idade: </em>{3}<br /><em>Registro n°: </em>{4}<br /><em>Telefone: </em>{5}<br /><em>Endereço: </em>{7}, {6}"
    }
  },
  police = {
    title = "Policia",
    wanted = "classificação de procurado {1}",
    not_handcuffed = "~r~Não algemado",
    cloakroom = {
      title = "vestiário",
      uniform = {
        title = "Uniforme",
        description = "Vestir Uniforme."
      }
    },
    pc = {
      title = "PC",
      searchreg = {
        title = "Pesquisa de registro",
        description = "Procurar identidade por registro.",
        prompt = "Insira o número de registro:"
      },
      closebusiness = {
        title = "Fechar Empresa",
        description = "Fechar o Empresa do jogador mais próximo.",
        request = "Você tem certeza de fechar a empresa? {3} propriedade de {1} {2} ?",
        closed = "g~Empresa Fechada."
      },
      trackveh = {
        title = "Localizar Veículo",
        description = "Localizar um veículo pelo seu número de registro.",
        prompt_reg = "Inserir numero de registro:",
        prompt_note = "inserir nota / razão de localizamento:",
        tracking = "~b~Localização iniciada.",
        track_failed = "~b~Localização do {1}~s~ ({2}) ~n~~r~Falhou.",
        tracked = "Localizado {1} ({2})"
      },
      records = {
        show = {
          title = "Mostrar registros",
          description = "Mostrar os registros da polícia por número de registro."
        },
        delete = {
          title = "Limpar Registros",
         description = "Limpar registros policiais por número de registro.",
          deleted = "~b~Registro da Policia deletado"
        }
      }
    },
    menu = {
      handcuff = {
        title = "Algemar",
        description = "Algemar/Desalgemar Jogador mais próximo."
      },
	  drag = {
        title = "Arrastar",
        description = "Arraste o player proximo."
      },
      putinveh = {
       title = "Colocar no veículo",
        description = "Colocar o jogador mais próximo algemado no veículo, como passageiro."
      },
      getoutveh = {
        title = "Tirar do veículo",
        description = "tirar do veículo o jogador algemado mais próximo."
      },
      askid = {
        title = "Pedir identidade",
        description = "Pedir identidade do jogador mais próximo.",
        request = "Identidade por favor ?",
        request_hide = "Pode Guardar.",
        asked = "Pedindo identidade..."
      },
      check = {
         title = "Verificar jogador",
        description = "Verifique dinheiro, estoque e armas do jogador mais próximo.",
        request_hide = "Ocultar o relatório.",
        info = "<em>Dinheiro: </em>{1} R$<br /><br /><em>Inventário: </em>{2}<br /><br /><em>Armas: </em>{3}",
        checked = "Você está sendo revistado."
      },
      seize = {
        seized = "Apreender {2} ~r~{1}",
        weapons = {
          title = "Apreender armas",
          description = "apreender armas do jogador mais próximo.",
          seized = "~b~Suas armas foram apreendidas."
        },
        items = {
          title = "Apreender ilegais",
          description = "apreender itens ilegais",
          seized = "~b~Seus itens ilegais foram apreendidos."
        }
      },
      jail = {
        title = "Prender",
        description = "prender/soltar jogador mais próximo dentro da delegacia mais próxima.",
        not_found = "~r~Nenhuma prisão encontrada.",
        jailed = "~b~Preso.",
        unjailed = "~b~Solto.",
        notify_jailed = "~b~Você foi preso.",
        notify_unjailed = "~b~Você foi solto."
      },
      fine = {
        title = "Multar",
        description = "Multar jogador mais próximo.",
        fined = "~b~Multado ~s~{2} R$ por ~b~{1}.",
        notify_fined = "~b~Você foi multado ~s~ {2} $ por ~b~{1}.",
        record = "[Multa] {2} R$ por {1}"
      },
      store_weapons = {
        title = "Guardar Armas",
        description = "Guarde suas armas no baú."
      }
    },
     identity = {
      info = "<em>Nome: </em>{1}<br /><em>Primeiro Nome: </em>{2}<br /><em>Idade: </em>{3}<br /><em>Registro n°: </em>{4}<br /><em>Telefone: </em>{5}<br /><em>Empresa: </em>{6}<br /><em>Renda: </em>{7} $<br /><em>Endereco: </em>{9}, {8}"
    }
},
  emergency = {
    menu = {
      revive = {
        title = "Reanimar",
        description = "Reanimar jogador mais próximo.",
        not_in_coma = "~r~Não está em coma."
      }
    }
  },
  phone = {
    title = "Telefone",
    directory = {
     title = "Lista telefônica",
      description = "Abrir lista telefônica.",
      add = {
        title = "> Adicionar",
        prompt_number = "adicionar:",
        prompt_name = "Inserir nome:",
        added = "~g~Adicionado com sucesso."
      },
      sendsms = {
        title = "Enviar SMS",
        prompt = "Inserir mensagem (max {1} Caracteres/chars):",
        sent = "~g~ Enviar para n°{1}.",
        not_sent = "~r~ n°{1} indisponível."
      },
      sendpos = {
        title = "Enviar posição",
      },
      remove = {
        title = "Remover"
      },
      call = {
        title = "Chamada",
        not_reached = "~r~ n°{1} não atendida."
      }
    },
    sms = {
      title = "SMS histórico",
      description = "SMS historico.",
      info = "<em>{1}</em><br /><br />{2}",
      notify = "SMS~b~ {1}:~s~ ~n~{2}"
    },
    smspos = {
      notify = "SMS-posição~b~ {1}"
    },
    service = {
      title = "Serviço",
      description = "Ligar para Serviço ou numero de emergência.",
      prompt = "Se precisar, inserir mensagem para o serviço:",
      ask_call = "Recebeu {1} ligação, você quer atender ? <em>{2}</em>",
      taken = "~r~Esta chamada já está sendo atendida."
    },
    announce = {
      title = "Anunciar/Announce",
      description = "Poste um anuncio para todos por tempo limitido.",
      item_desc = "{1} R$<br /><br/>{2}",
      prompt = "conteúdo do anúncio (10-1000 caracteres): "
    },
    call = {
      ask = "Aceitar chamada de {1} ?",
      notify_to = "Chamando~b~ {1}...",
      notify_from = "Chamada recebida ~b~ {1}...",
      notify_refused = "chamada para ~b~ {1}... ~r~ recusada."
    },
    hangup = {
      title = "Desligar",
      description = "Desligue o telefone (desligue a chamada atual)."
    }
  },
  emotes = {
    title = "Animações",
    clear = {
      title = "> Limpar",
      description = "Limpar todas as animações."
    }
  },
  home = {
    buy = {
      title = "Comprar",
      description = "Compre um Imovel aqui, preço {1} R$.",
      bought = "~g~Comprado.",
      full = "~r~Está casa já tem dono.",
      have_home = "~r~ Você já tem um Imovel."
    },
    sell = {
      title = "Vender",
      description = "Vender sua casa por {1} R$",
      sold = "~g~Vendido.",
      no_home = "r~Você não tem Imovel aqui."
    },
    intercom = {
      title = "Interfone",
      description = "Use o interfone para entrar.",
      prompt = "Número:",
      not_available = "~r~Não disponível.",
      refused = "Diga quem você é.",
      prompt_who = "Diga quem você é:",
      asked = "Perguntando...",
      request = "Alguém está batendo na sua porta: <em>{1}</em>"
    },
    slot = {
      leave = {
        title = "Sair"
      },
      ejectall = {
        title = "Retirar todos",
        description = "Ejetar todos os visitantes domésticos, incluindo você, e fechar a casa."
      }
    },
    wardrobe = {
      title = "Guarda Roupa",
      save = {
        title = "> Salvar",
        prompt = "Nome para salvar:"
      }
    },
    gametable = {
      title = "Jogos de Tabela",
      bet = {
        title = "Aposta inicial",
        description = "Comece uma aposta com jogadores perto de você, o vencedor será selecionado aleatoriamente.",
        prompt = "Valor da aposta:",
        request = "[BET] Você quer apostar {1} R$ ?",
        started = "~g~Aposta iniciada."
      }
    },
    radio = {
      title = "Radio",
      off = {
        title = "> Desligar radio"
      }
    }
  },
  garage = {
    title = "({1})",
    owned = {
     title = "Retirar Veiculo",
      description = "Seus Veículos."
    },
    buy = {
      title = "Comprar",
      description = "Comprar Veículo.",
      info = "{1} R$<br /><br />{2}"
    },
    sell = {
      title = "Vender",
      description = "Vender Veículo."
    },
    rent = {
      title = "Alugar",
      description = "Alugue um veículo para a sessão."
    },
    store = {
     title = "Guardar Veiculo",
      description = "Coloque seu veículo atual na garagem."
    }
  },
  vehicle = {
   title = "Veículos",
    no_owned_near = "~r~Nenhum veículo pessoal perto.",
    trunk = {
      title = "Porta Malas",
      description = "Abrir Porta Malas"
    },
    detach_trailer = {
      title = "Retirar trailer",
      description = "Retirar trailer."
    },
    detach_towtruck = {
      title = "Soltar reboque",
      description = "Soltar reboque."
    },
    detach_cargobob = {
      title = "Desanexar carga",
      description = "Desanexar carga."
    },
    lock = {
      title = "Trancar/Destrancar",
      description = "Trancar ou destrancar."
    },
    engine = {
      title = "Motor Ligar/Desligar",
      description = "Ligar ou desligar motor."
    },
    asktrunk = {
      title = "Pedir para abrir Porta mala",
      asked = "~g~Pedindo...",
      request = "Abra o porta mala por favor ?"
    },
	portas = {
      title = "Abrir Todas as Portas"
    },
	capoabrir = {
      title = "Abrir Porta Malas",
      description = "Porta Malas."
    },
	capofechar = {
      title = "Fechar Porta Malas",
      description = "Porta Malas."
    },
	
	portasaberta = {
      title = "Abrir Todas as Portas",
      description = "Abre todas as Portas."
    },
	
	portasfechadas = {
      title = "Fechar Todas as Portas",
      description = "Fecha todas as Portas."
    },
	porta1 = {
      title = "Abrir Porta Esquerda",
      description = "Porta Esquerda."
    },
	porta2 = {
      title = "Fechar Porta Esquerda",
      description = "Porta Esquerda."
    },
	porta3 = {
      title = "Abrir Porta Direita",
      description = "Porta Direita."
    },
	porta4 = {
      title = "Fechar Porta Direita",
      description = "Porta Direita."
    },
	porta5 = {
      title = "Abrir Porta de traz Esquerda",
      description = "Porta de traz Esquerda."
    },
	porta6 = {
      title = "Fechar Porta de traz Esquerda",
      description = "Porta de traz Esquerda."
    },
	porta7 = {
      title = "Abrir Porta de traz Direita",
      description = "Porta de traz Direita."
    },
	porta8 = {
      title = "Fechar Porta de traz Direita",
      description = "Porta de traz Direita."
    },
	porta9 = {
      title = "Abrir Capo",
      description = "Capo."
    },
	porta10 = {
      title = "Fechar Capo",
      description = "Capo."
    },
    replace = {
       title = "Substituir veículo",
      description = "Substituir No solo o veículo mais próximo."
    },
    repair = {
      title = "Reparar veículo",
      description = "Reparar veículo mais próximo."
    }
  },
  gunshop = {
    title = "Gunshop ({1})",
    prompt_ammo = "Amount of ammo to buy for the {1}:",
    info = "<em>body: </em> {1} $<br /><em>ammo: </em> {2} $/u<br /><br />{3}"
  },
  market = {
   title = "({1})",
    prompt = "Quantidade de {1} para comprar:",
    info = "{1} R$<br /><br />{2}"
  },
  skinshop = {
     title = "Loja de roupas"
  },
  cloakroom = {
    title = "guarda-roupa ({1})",
    undress = {
      title = "> Retirar"
    }
  },
  itemtr = {
    not_enough_reagents = "~r~Not enough reagents.",
    informer = {
      title = "Informante ilegal",
      description = "{1} R$",
      bought = "~g~Posição mandada para o seu GPS."
    }
  },
  mission = {
    blip = "Missão/Mission ({1}) {2}/{3}",
    display = "<span class=\"symbol\">M</span> <span class=\"name\">{1}</span> <span class=\"step\">{2}/{3}</span><br /><br />{4}",
    cancel = {
      title = "Cancelar Trabalho"
    }
  },
  aptitude = {
    title = "Habilidades",
    description = "Mostra suas Habilidades.",
    lose_exp = "Habilidades ~b~{1}/{2} ~r~-{3} ~s~exp.",
    earn_exp = "Habilidades ~b~{1}/{2} ~g~+{3} ~s~exp.",
    level_down = "Habilidades ~b~{1}/{2} ~r~Perdeu nível ({3}).",
    level_up = "Habilidades ~b~{1}/{2} ~g~Subiu de nível ({3}).",
    display = {
      group = "{1}: ",
      aptitude = "{1} LVL {3} EXP {2}"
    }
  },
  radio = {
    title = "Radio ON/OFF"
  }
}

return lang
