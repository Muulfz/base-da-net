
local lang = {
  garage = {
    buy = {
	  item = "{2} {1}<br /><br />{3}",
	  request = "Tem certeza de que deseja comprar este veículo?",
	},
    keys = {
	  title = "Chaves",
	  key = "Chave ({1})",
	  description = "Verifica as chaves do seu veículo",
	  sell = {
	    title = "Vender",
		prompt = "Valor de venda:",
	    owned = "O veículo já tem dono",
		request = "Você aceita a oferta de comprar por {1} para {2}?",
		description = "Somente jogadores perto"
	  }
	},
    personal = {
	  client = {
	    locked = "~r~Veiculo trancado",
		unlocked = "~g~Veiculo aberto"
	  },
	  out = "Já existe um veiculo desse na rua",
	  bought = "Veiculo enviado a sua garagem",
	  sold = "Veiculo vendido",
	  stored = "Veiculo guardado",
	  toofar = "Veículo muito distante"	  
	},
	showroom = {
	  title = "Showroom",
	  description = "Para visualizar o carro, aperte a SETA PARA A DIREITA. Se quiser comprar, aperte ENTER."
	},
    shop = {
	  title = "Loja",
	  description = "Use as setas para escolher as modificações do veiculo",
	  client = {
	    nomods = "~r~Nenhuma modificação deste tipo para este veículo",
		maximum = "Você alcançou o ~y~maximu~w~ Level para essa modificação",
		minimum = "Você alcançou o ~r~minimu~w~ Level para essa modificação",
	    toggle = {
		  applied = "~g~Modificação aplicada",
		  removed = "~r~Modificação removida"
		}
	  },
	  mods = {
	    title = "Modificações",
		info = "Use as setas para escolher as modificaões",
	  },
	  repair = {
	    title = "Reparar",
		info = "Arrumar danos no veiculo",
	  },
	  colour = {
	    title = "Cores",
		info = "Use as setas para escolher as cores",
		primary = "Cor Primaria",
		secondary = "Cor Secundaria",
	    extra = {
		  title = "Cores Extras",
		  info = "Use as setas para escolher as cores extras",
	      pearlescent = "",
	      wheel = "Cores das rodas",
	      smoke = "Cor da fumaça",
		},
		custom = {
		  primary = "Sistema de cores por RGB",
		  secondary = "Cores secundaria custom",
		},
	  },
	  neon = {
	    title = "Luzes de Neon",
		info = "Modifica as luzes de neon",
	    front = "Neon da frente",
	    back = "neon de tras",
	    left = "Neon da esquerda",
	    right = "Neon da direita",
	    colour = "Cores do Neon"
	  }
	}
  }
}

return lang
