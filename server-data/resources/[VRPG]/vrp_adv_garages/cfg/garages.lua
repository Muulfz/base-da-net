--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]


local cfg = {}
-- define garage types with their associated vehicles
-- (vehicle list: https://wiki.fivem.net/wiki/Vehicles)

-- each garage type is an associated list of veh_name/veh_definition 
-- they need a _config property to define the blip and the vehicle type for the garage (each vtype allow one vehicle to be spawned at a time, the default vtype is "default")
-- this is used to let the player spawn a boat AND a car at the same time for example, and only despawn it in the correct garage
-- _config: gtype, vtype, blipid, blipcolor, ghome, permissions (optional, only users with the permission will have access to the shop)
-- vtype: identifies the "type" of vehicle for the personal garages and vehicles (you can create new ones)
-- gtype: there are 5 gtypes> personal, showroom, shop, store and rental (you cant create new ones, one garage can have many gtypes)
   -- personal: allow you to get any personal vehicle of the same vtype of the garage
   -- showroom: allows you to see the vehicle model before purchasing it
   -- shop: allows you to modify your vehicle
   -- store: allows you to purchase and sell vehicles
   -- rental: allows you to rent vehicles for that session for a part of the price
-- ghome: links the garage with an address, only owners of that address will have see the garage
-- gpay: bank or wallet
-- Car/Mod: [id/model] = {"Display Name", price/amount, "", (optional) item}, -- when charging items, price becomes amount

cfg.lang = "en" -- lenguage file

cfg.rent_factor = 0.1 -- 10% of the original price if a rent
cfg.sell_factor = 0.75 -- sell for 75% of the original price

cfg.price = {
  repair = 500, -- value to repair the vehicle
  colour = 100, -- value will be charged 3 times for RGB
  extra = 100, -- value will be charged 3 times for RGB
  neon = 100 -- value will be charged 3 times for RGB
}

-- declare any item used on purchase that doesnt exist yet (name,description,choices,weight}
cfg.items = {
  ["issi2key"] = {"Issi 2 Key","Buys an Issi",nil,0.5} -- example
}

-- configure garage types
cfg.adv_garages = {
    ["Elite Shop"]  = {
    _config = {gpay="bank",gtype={"showroom","rental"},vtype="car",blipid=225,blipcolor=4},
    ["718caymans"] = {"718", 1308500, ""},
	["acsr"] = {"Aguera", 818500, ""},
	["casco"] = {"Chevette", 35000, ""},
	["tampa"] = {"Opala SS4", 218500, ""},
	["vwpolo"] = {"Polo", 32000, ""},
	["pajero4"] = {"Pajero TR4", 88500, ""},
	["santafe"] = {"Santa Fe", 100500, ""},
	["718boxster"] = {"Boxster", 820000, ""},
	["mk7"] = {"Golf GTI", 378500, ""},
	["cavalcade"] = {"Saveiro Cross", 31500, ""},
	["escade"] = {"Escalade", 288500, ""},
    ["a45"] = {"Mercedes A45", 318500, ""},
	["c63coupe"] = {"Mercedes AMG", 598500, ""},
	["440i"] = {"BMW M4 Sport", 418500, ""},
	["kuruma"] = {"Lancer evo", 158500, ""},
	["mgt"] = {"Mustang GT", 880500, ""},
	["ttrs"] = {"Audi TT", 718500, ""},
	["m4"] = {"BMW M4", 318500, ""},
	["zl12017"] = {"Camaro", 198500, ""},
	["czl1"] = {"Camaro ZL11", 418500, ""},
	["demon"] = {"Dodge F", 618500, ""}
  },
  
  ["Aluguel de Bike"] = {
    _config = {gpay="bank",gtype={"rental"},vtype="bike",blipid=50,blipcolor=4},
    ["fixter"] = {"Bike de Aluguel",100, ""}
  },
  
  ["Loja de Motos"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=226,blipcolor=4},
    ["dm1200"] = {"Ducatti",705000, ""},
	["bros60"] = {"Bros",35000, ""},
	["xt66"] = {"XT 660",71000, ""},
	["r1"] = {"R1",905000, ""},
	["r6"] = {"R6",715000, ""},
	["rc"] = {"RC",515000, ""},
	["cbrr"] = {"CB RR",718000, ""},
	["zc10r"] = {"ZX 10R",325000, ""},
	["AKUMA"] = {"Akuma",85000, ""},
	["bagger"] = {"Bagger",45000, ""},
	["bati"] = {"Bati",65000, ""},
	["bati2"] = {"Bati2",55000, ""},
	["bf400"] = {"BF 400",35000, ""},
	["carbonrs"] = {"Carbon RS",115000, ""},
	["cliffhanger"] = {"Cliff Hanger",95000, ""},
	["double"] = {"Double",75000, ""},
	["faggio2"] = {"Faggio 2",7100, ""},
	["gargoyle"] = {"Gargoyle",45000, ""},
	["hexer"] = {"Hexer",55000, ""},
	["innovation"] = {"Innovation",35000, ""},
	["lectro"] = {"Lectro",45000, ""},
	["nemesis"] = {"Nemesis",65000, ""},
	["pcj"] = {"Pcj",28000, ""},
	["ruffian"] = {"Ruffian",48000, ""},
	["sanchez"] = {"Sanchez",28000, ""},
	["thrust"] = {"Thrust ",80000, ""},
	["vader"] = {"Vader",49000, ""},
	["vindicator"] = {"Vindicator",115000, ""}
  },
  
    ["Loja de Barcos"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="barcos",blipid=455,blipcolor=50},
    ["squalo"] = {"Squalo",196621, ""},
    ["marquis"] = {"Marquis",413990, ""},
    ["speeder"] = {"Speeder",325000, ""},
    ["dinghy"] = {"Dinghy",166250, ""},
    ["jetmax"] = {"Jetmax",299000, ""},
    ["predator"] = {"Predator",825710, ""},
    ["tropic"] = {"Tropic",22000, ""},
    ["seashark"] = {"Seashark",16899, ""},
    ["submersible"] = {"Submersible",1150000, ""},
    ["submersible2"] = {"Kraken",1325000, ""},
    ["suntrap"] = {"Suntrap",25160, ""},
    ["toro"] = {"Toro",1750000, ""}
  },
  ["Loja de Caminhao"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="caminhao",blipid=318,blipcolor=52},
    ["hauler"] = {"Hauler",80000, ""},
    ["mule"] = {"Mule",70000, ""},
    ["packer"] = {"Packer",78600, ""},
    ["phantom"] = {"Phantom",115200, ""}
  },
  
  ["Loja de Aerea"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="aerea",blipid=90,blipcolor=38},
    ["nimbus"] = {"Nimbus",1900000, ""},
    ["vestra"] = {"Vestra",950000, ""},
    ["miljet"] = {"Miljet",1700000, ""},
    ["dodo"] = {"Dodo",500000, ""},
    ["velum"] = {"Velum",450000, ""},
    ["cuban800"] = {"Cuban 800",240000, ""},
    ["duster"] = {"Duster",275000, ""},
    ["stunt"] = {"Stuntplane",150000, ""},
    ["mammatus"] = {"Mammatus",300000, ""},
    ["jet"] = {"Jet",1500000, ""},
    ["shamal"] = {"Shamal",1150000, ""},
    ["luxor"] = {"Luxor",1625000, ""},
	["supervolito"] = {"Super Volito",2113000, ""},
    ["volatus"] = {"Volatus",2295000, ""},
    ["swift"] = {"Swift",1500000, ""},
    ["skylift"] = {"Skylift",1745000, ""},
    ["maverick"] = {"Maverick",780000, ""},
    ["frogger"] = {"Frogger",1300000, ""}
  },

  ["PMESP"] = {
   _config = {gpay="wallet",gtype={"rental"},vtype="pmesp",blipid=50,blipcolor=1,permissions={"pm.garagem"}},
	["police3"] = {"Blazer",0, ""},
	["police4"] = {"Hylux",0, ""},
	["sheriff"] = {"TRAILBlazer",0, ""}
  },    
  ["Força Tática"] = {
	 _config = {gpay="wallet",gtype={"rental"},vtype="ft",blipid=50,blipcolor=1,permissions={"ft.garagem"}},
    ["hiluxft"] = {"Hilux",0, ""}
  },
     
  ["Operações Especiais"] = {
	 _config = {gpay="wallet",gtype={"rental"},vtype="ft",blipid=50,blipcolor=1,permissions={"ope.garagem"}},
    ["riot"] = {"Camburão",0, ""},
	["coach"] = {"Ônibus",0, ""}
  },      

  ["Heliponto PM"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="aguia",blipid=50,blipcolor=1,permissions={"aguia.garagem"}},
    ["as350"] = {"Águia",0, ""}
  },    
  
--Doadores loja exemplo
  ["VIP Gold"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="doador",blipid=50,blipcolor=46,permissions={"vipgold.garagem"}},
    ["passat"] = {"Passat 2018",0, ""},
    ["A4"] = {"Audi A4",0, ""},
	["gsxr"] = {"GSXR1000",0, ""}
  
  },
  ["VIP Diamond"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="doador",blipid=50,blipcolor=3,permissions={"vipdiamond.garagem"}},
    ["a45"] = {"Mercedes A45",0, ""},
    ["psc18"] = {"Porsche Cayenne",0, ""},
	["f4rr"] = {"Agusta F4RR",0, ""}
  
  },
  ["VIP Platinum"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="doador",blipid=50,blipcolor=4,permissions={"vipplatinum.garagem"}},
    ["gtr"] = {"GTR R35",0, ""},
    ["c7"] = {"Corvette C7",0, ""},
	["r8ppi"] = {"Audi R8",0, ""},
	["bmws"] = {"S1000RR",0, ""}
  
  },
--Doadores fim
  ["Hospital"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="samu",blipid=50,blipcolor=1,permissions={"hospital.garagem"}},
	["sw4samu"] = {"SW4 Samu",0, ""},
    ["ambulance"] = {"Ambulancia",0, ""},
	["tsamu"] = {"Tiger SAMU",0, ""}
	
  },
  ["Heliponto SAMU"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="samu1",blipid=50,blipcolor=1,permissions={"aguiasamu.garagem"}},
    ["samumav"] = {"Helicóptero Samu",0, ""}

  },   
  ["Uber"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="uber",blipid=56,blipcolor=46,permissions={"taxi.garagem"}},
    ["a45"] = {"Uber",0, ""}
  },
  
  ["Garagem de Onibus"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="bus",blipid=513,blipcolor=38,permissions={"bus.garagem"}},
    ["bus"] = {"Ônibus",0, ""}
  },
  
    ["Mecânico"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="bote",blipid=402,blipcolor=52,permissions={"repair.garagem"}},
    ["towtruck2"] = {"Mecânico",0, ""}
  },
  
  ["Jornalista"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="jornal",blipid=184,blipcolor=58,permissions={"jornal.garagem"}},
    ["Burrito5"] = {"Burrito5",0, ""}
  },
  
  ["Bote de Pescador"] = {
	_config = {gpay="wallet",gtype={"rental"},vtype="bote",blipid=410,blipcolor=46,permissions={"fisher.garagem"}},
    ["suntrap"] = {"Bote de Pescador",0, ""}
  },

  ["Garagem Publica"]  = {
    _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3},
    ["blista"] = {"Blista", 15000, ""},
    ["brioso"] = {"Brioso R/A", 155000, ""},
    ["dilettante"] = {"Dilettante", 25000, ""},
    ["issi2"] = {"Issi", 1, "", "issi2key"}, -- vehicle sold by item as example
    ["panto"] = {"Panto", 85000, ""},
    ["prairie"] = {"Prairie", 30000, ""},
    ["rhapsody"] = {"Rhapsody", 120000, ""},
    ["cogcabrio"] = {"Cognoscenti Cabrio",180000, ""},
    ["exemplar"] = {"Exemplar", 200000, ""},
    ["f620"] = {"F620", 80000, ""},
    ["felon"] = {"Felon", 90000, ""},
    ["felon2"] = {"Felon GT", 95000, ""},
    ["jackal"] = {"Jackal", 60000, ""},
    ["oracle"] = {"Oracle", 80000, ""},
    ["oracle2"] = {"Oracle XS",82000, ""},
    ["sentinel"] = {"sentinel", 90000, ""},
    ["sentinel2"] = {"Sentinel XS", 60000, ""},
    ["windsor"] = {"Windsor",800000, ""},
    ["windsor2"] = {"Windsor Drop",850000, ""},
    ["zion"] = {"Zion", 60000, ""},
    ["zion2"] = {"Zion Cabrio", 65000, ""},
    ["ninef"] = {"9F",120000, ""},
    ["ninef2"] = {"9F Cabrio",130000, ""},
    ["alpha"] = {"Alpha",150000, ""},
    ["banshee"] = {"Banshee",105000, ""},
    ["bestiagts"] = {"Bestia GTS",610000, ""},
    ["blista"] = {"Blista Compact",42000, ""},
    ["buffalo"] = {"Buffalo",35000, ""},
    ["buffalo2"] = {"Buffalo S",96000, ""},
    ["carbonizzare"] = {"Carbonizzare",195000, ""},
    ["comet2"] = {"Comet",100000, ""},
    ["coquette"] = {"Coquette",138000, ""},
    ["tampa2"] = {"Drift Tampa",995000, ""},
    ["feltzer2"] = {"Feltzer",130000, ""},
    ["furoregt"] = {"Furore GT",448000, ""},
    ["fusilade"] = {"Fusilade",36000, ""},
    ["jester"] = {"Jester",240000, ""},
    ["jester2"] = {"Jester (Racecar)",350000, ""},
    ["kuruma"] = {"Kuruma",95000, ""},
    ["lynx"] = {"Lynx",1735000, ""},
    ["massacro"] = {"Massacro",275000, ""},
    ["massacro2"] = {"Massacro (Racecar)",385000, ""},
    ["omnis"] = {"Omnis",701000, ""},
    ["penumbra"] = {"Penumbra",24000, ""},
    ["rapidgt"] = {"Rapid GT",140000, ""},
    ["rapidgt2"] = {"Rapid GT Convertible",150000, ""},
    ["schafter3"] = {"Schafter V12",140000, ""},
    ["sultan"] = {"Sultan",12000, ""},
    ["surano"] = {"Surano",110000, ""},
    ["tropos"] = {"Tropos",816000, ""},
    ["verlierer2"] = {"Verkierer",695000,""},
    ["casco"] = {"Casco",680000, ""},
    ["coquette2"] = {"Coquette Classic",665000, ""},
    ["jb700"] = {"JB 700",350000, ""},
    ["pigalle"] = {"Pigalle",400000, ""},
    ["stinger"] = {"Stinger",850000, ""},
    ["stingergt"] = {"Stinger GT",875000, ""},
    ["feltzer3"] = {"Stirling",975000, ""},
    ["ztype"] = {"Z-Type",950000,""},
    ["adder"] = {"Adder",1000000, ""},
    ["banshee2"] = {"Banshee 900R",565000, ""},
    ["bullet"] = {"Bullet",155000, ""},
    ["cheetah"] = {"Cheetah",650000, ""},
    ["entityxf"] = {"Entity XF",795000, ""},
    ["sheava"] = {"ETR1",199500, "4 - (smaller number better car"},
    ["fmj"] = {"FMJ",1750000, "10 - (smaller number better car"},
    ["infernus"] = {"Infernus",440000, ""},
    ["osiris"] = {"Osiris",1950000, "8 - (smaller number better car"},
    ["le7b"] = {"RE-7B",5075000, "1 - (smaller number better car"},
    ["reaper"] = {"Reaper",1595000, ""},
    ["sultanrs"] = {"Sultan RS",795000, ""},
    ["t20"] = {"T20",2200000,"7 - (smaller number better car"},
    ["turismor"] = {"Turismo R",500000, "9 - (smaller number better car"},
    ["tyrus"] = {"Tyrus",2550000, "5 - (smaller number better car"},
    ["vacca"] = {"Vacca",240000, ""},
    ["voltic"] = {"Voltic",150000, ""},
    ["prototipo"] = {"X80 Proto",2700000, "6 - (smaller number better car"},
    ["zentorno"] = {"Zentorno",725000,"3 - (smaller number better car"},
    ["blade"] = {"Blade",160000, ""},
    ["buccaneer"] = {"Buccaneer",29000, ""},
    ["Chino"] = {"Chino",225000, ""},
    ["coquette3"] = {"Coquette BlackFin",695000, ""},
    ["dominator"] = {"Dominator",35000, ""},
    ["dukes"] = {"Dukes",62000, ""},
    ["gauntlet"] = {"Gauntlet",32000, ""},
    ["hotknife"] = {"Hotknife",90000, ""},
    ["faction"] = {"Faction",36000, ""},
    ["nightshade"] = {"Nightshade",585000, ""},
    ["picador"] = {"Picador",9000, ""},
    ["sabregt"] = {"Sabre Turbo",15000, ""},
    ["tampa"] = {"Tampa",375000, ""},
    ["virgo"] = {"Virgo",195000, ""},
    ["vigero"] = {"Vigero",21000, ""},
    ["bifta"] = {"Bifta",75000, ""},
    ["blazer"] = {"Blazer",8000, ""},
    ["brawler"] = {"Brawler",715000, ""},
    ["dubsta3"] = {"Bubsta 6x6",249000, ""},
    ["dune"] = {"Dune Buggy",20000, ""},
    ["rebel2"] = {"Rebel",22000, ""},
    ["sandking"] = {"Sandking",38000, ""},
    ["monster"] = {"The Liberator",550000, ""},
    ["trophytruck"] = {"Trophy Truck",550000, ""},
    ["baller"] = {"Baller",90000, ""},
    ["cavalcade"] = {"Cavalcade",60000, ""},
    ["granger"] = {"Grabger",35000, ""},
    ["huntley"] = {"Huntley",195000, ""},
    ["landstalker"] = {"Landstalker",58000, ""},
    ["radi"] = {"Radius",32000, ""},
    ["rocoto"] = {"Rocoto",85000, ""},
    ["seminole"] = {"Seminole",30000, ""},
    ["xls"] = {"XLS",253000, ""},
    ["bison"] = {"Bison",30000, ""},
    ["bobcatxl"] = {"Bobcat XL",23000, ""},
    ["gburrito"] = {"Gang Burrito",65000, ""},
    ["journey"] = {"Journey",15000, ""},
    ["minivan"] = {"Minivan",30000, ""},
    ["paradise"] = {"Paradise",25000, ""},
    ["rumpo"] = {"Rumpo",13000, ""},
    ["surfer"] = {"Surfer",11000, ""},
    ["youga"] = {"Youga",16000, ""},
    ["asea"] = {"Asea",1000000, ""},
    ["asterope"] = {"Asterope",1000000, ""},
    ["cognoscenti"] = {"Cognoscenti",1000000, ""},
    ["cognoscenti2"] = {"Cognoscenti(Armored)",1000000, ""},
    ["cog55"] = {"Cognoscenti 55",1000000, ""},
    ["cog552"] = {"Cognoscenti 55(Armored)",1500000, ""},
    ["fugitive"] = {"Fugitive",24000, ""},
    ["glendale"] = {"Glendale",200000, ""},
    ["ingot"] = {"Ingot",9000, ""},
    ["intruder"] = {"Intruder",16000, ""},
    ["premier"] = {"Premier",10000, ""},
    ["primo"] = {"Primo",9000, ""},
    ["primo2"] = {"Primo Custom",9500, ""},
    ["regina"] = {"Regina",8000, ""},
    ["schafter2"] = {"Schafter",65000, ""},
    ["stanier"] = {"Stanier",10000, ""},
    ["stratum"] = {"Stratum",10000, ""},
    ["stretch"] = {"Stretch",30000, ""},
    ["superd"] = {"Super Diamond",250000, ""},
    ["surge"] = {"Surge",38000, ""},
    ["tailgater"] = {"Tailgater",55000, ""},
    ["warrener"] = {"Warrener",120000, ""},
    ["washington"] = {"Washington",15000, ""},
    ["AKUMA"] = {"Akuma",9000, ""},
    ["bagger"] = {"Bagger",5000, ""},
    ["bati"] = {"Bati 801",15000, ""},
    ["bati2"] = {"Bati 801RR",15000, ""},
    ["bf400"] = {"BF400",95000, ""},
    ["carbonrs"] = {"Carbon RS",40000, ""},
    ["cliffhanger"] = {"Cliffhanger",225000, ""},
    ["daemon"] = {"Daemon",5000, ""},
    ["double"] = {"Double T",12000, ""},
    ["enduro"] = {"Enduro",48000, ""},
    ["faggio2"] = {"Faggio",4000, ""},
    ["gargoyle"] = {"Gargoyle",120000, ""},
    ["hakuchou"] = {"Hakuchou",82000, ""},
    ["hexer"] = {"Hexer",15000, ""},
    ["innovation"] = {"Innovation",90000, ""},
    ["lectro"] = {"Lectro",700000, ""},
    ["nemesis"] = {"Nemesis",12000, ""},
    ["pcj"] = {"PCJ-600",9000, ""},
    ["ruffian"] = {"Ruffian",9000, ""},
    ["sanchez"] = {"Sanchez",7000, ""},
    ["sovereign"] = {"Sovereign",90000, ""},
    ["thrust"] = {"Thrust",75000, ""},
    ["vader"] = {"Vader",9000, ""},
    ["vindicator"] = {"Vindicator",600000,""}
  },
    ["LS Customs"]  = {
    _config = {gpay="wallet",gtype={"shop"},vtype="car",blipid=72,blipcolor=7},
	_shop = {
	  [0] = {"Aerofolio",500,""},
	  [1] = {"Para-choque dianteiro",500,""},
      [2] = {"Amortecedor traseiro",500,""}, 
      [3] = {"Saia lateral",500,""},  
      [4] = {"Escapamento",500,""},     
      [5] = {"Chassi",500,""},       
      [6] = {"Grade",500,""},      
      [7] = {"Capo",500,""},        
      [8] = {"Pára-choque",500,""},      
      [9] = {"Para-lamas direito",500,""},
      [10] = {"Teto",500,""},        
      [11] = {"Motor",500,""},      
      [12] = {"Freios",500,""},      
      [13] = {"Transmissao",500,""},
      [14] = {"Buzinas",500,""},       
      [15] = {"Suspensão",500,""},  
      [16] = {"blindagem",500,""},      
      [18] = {"Turbo",500,""},
      [20] = {"Cor da Fumaça do Pneu",500,""},
      [22] = {"Luzes de Xenon",500,""},
      [23] = {"Rodas",500,"Pressione enter para mudar o tipo"},
      [24] = {"Back Wheels (Bike)",500,""}, 
      [25] = {"Suportes De Placa",500,""},
      [27] = {"Guarnições",500,""},       
      [28] = {"Enfeites",500,""},   
      [29] = {"Painel de instrumentos",500,""},  
      [30] = {"Mostradores",500,""},       
      [31] = {"Alto Falantes na Porta",500,""},
      [32] = {"Bancos",500,""},       
      [33] = {"Volante",500,""},
      [34] = {"H Shift",500,""},     
      [35] = {"Placas Personalizadas",500,""},      
      [36] = {"Alto falantes",500,""},    
      [37] = {"Som no Porta Malas",500,""},      
      [38] = {"Hidráulica",500,""},  
      [39] = {"Bloco do motor",500,""},
      [40] = {"Filtro de ar",500,""},  
      [41] = {"Suportes",500,""},      
      [42] = {"Capas de arco",500,""}, 
      [43] = {"Antenas",500,""},      
      [44] = {"Guarnições Extra",500,""}, 
      [45] = {"Tanque",500,""},       
      [46] = {"Vidros",500,""},     
      [48] = {"Pinturas Personalizadas",500,""},      
	}
  },
}

cfg.garages = {
  {"Loja de Caminhao",1378.1372070313,-2065.5737304688,51.998565673828},
  {"Loja de Caminhao",195.31614685059,6588.541015625,31.794353485107},
  {"Loja de Barcos",3847.0751953125,4471.3549804688,0.97042548656464},
  {"Loja de Barcos",-1638.7075195313,-1157.0296630859,2.0702977180481},
  {"Aluguel de Bike",-1032.7554931641,-2730.7731933594,20.076156616211},
  {"Loja de Aerea",-955.75549316406,-2995.7282714844,13.945084571838},
  {"Loja de Aerea",1723.6307373047,3244.1508789063,41.152359008789},
  {"Elite Shop",-46.113430023193,-1096.4566650391,26.422330856323},
  {"Loja de Motos",-40.726333618164,-1098.5036621094,26.422330856323},
  {"Garagem Publica",215.124,-791.377,30.646},
  {"Garagem Publica",-334.685,289.773,85.705},
  {"Garagem Publica",-55.272,-1838.71,26.442},
  {"Garagem Publica",126.434,6610.04,31.750}, 
{"Garagem Publica",-2142.8828125,-437.17086791992,14.059371948242},
{"Garagem Publica",-2147.9475097656,-434.65875244141,14.06137752533},
{"Garagem Publica",-2151.2485351563,-430.37326049805,14.060933113098},
{"Garagem Publica",-2155.5234375,-427.50015258789,14.062129020691},
{"Garagem Publica",-2117.478515625,-414.0192565918,14.025656700134},
{"Garagem Publica",-2120.8837890625,-410.30358886719,14.025660514832},
{"Garagem Publica",-2124.2880859375,-406.22689819336,14.025433540344},
{"Garagem Publica",-2127.9787597656,-402.5966796875,14.02569770813},
{"Garagem Publica",-1043.9970703125,-2674.3498535156,13.830760955811},
{"Garagem Publica",436.73303222656,-1014.6417236328,28.706298828125},
{"Garagem Publica",106.52588653564,-1063.3723144531,29.19236946106},
{"Garagem Publica",-59.212127685547,-1117.0632324219,26.433883666992},
{"Garagem Publica",383.76849365234,-1452.2218017578,29.362821578979},
{"Garagem Publica",-1072.87109375,-1704.6433105469,4.5162243843079},
{"Garagem Publica",-1461.8520507813,-494.97378540039,32.911235809326},
{"Garagem Publica",-875.79602050781,-364.43124389648,36.700782775879},
{"Garagem Publica",-637.69097900391,56.664379119873,43.849876403809},
{"Garagem Publica",-796.26123046875,305.10079956055,85.700416564941},
{"Garagem Publica",-818.80639648438,184.92616271973,72.248893737793},
{"Garagem Publica",-306.48699951172,-710.04302978516,28.906768798828},
{"Garagem Publica",-7.2519254684448,-574.83728027344,37.745059967041},
{"Garagem Publica",13.694809913635,549.03521728516,176.1901550293},
{"Garagem Publica",-123.32816314697,509.28018188477,142.96369934082},
{"Garagem Publica",-555.30377197266,664.82403564453,145.12464904785},
{"Garagem Publica",782.1171875,-295.98962402344,59.881511688232},
{"Garagem Publica",1384.4333496094,-579.45196533203,74.339218139648},
{"Garagem Publica",1161.7631835938,-1496.6418457031,34.692569732666},
{"Garagem Publica",-219.28231811523,-2037.9093017578,27.620416641235},
{"Garagem Publica",1298.7927246094,-1733.9859619141,53.87911605835},
{"Garagem Publica",498.75103759766,-1703.5963134766,29.352087020874},
{"Garagem Publica",1420.6450195313,3623.5383300781,34.861667633057},
{"Garagem Publica",1981.1186523438,3828.4946289063,32.417922973633},
{"Garagem Publica",2480.6252441406,4953.98046875,45.025783538818},
{"Garagem Publica",33.025840759277,6606.9223632813,32.450828552246},
{"Garagem Publica",-355.71557617188,6223.7060546875,31.489231109619},
{"Garagem Publica",-449.92971801758,6053.46875,31.340530395508},
{"Garagem Publica",1412.5218505859,1118.9274902344,114.83792114258},
{"Garagem Publica",284.48852539063,-338.9423828125,44.919841766357},
{"Garagem Publica",-301.45770263672,-989.27166748047,31.080612182617},
{"Garagem Publica",143.4083404541,-1281.9693603516,29.284015655518},
{"PMESP",453.24819946289,-1019.3926391602,28.396326065063},
  --{"Força Tática",446.9436340332,-1024.6547851563,28.626346588135},
  {"Heliponto PM",449.32727050781,-981.21728515625,43.691673278809},
  {"Operações Especiais",449.32727050781,-981.21728515625,43.691673278809},
  {"Heliponto SAMU",313.24923706055,-1465.1434326172,46.509502410889},
  {"Uber",-339.02719116211,-1023.7808837891,30.380908966064},
  {"Delivery",141.74586486816,-1458.0601806641,29.141620635986},
  {"Mecanico",491.99514770508,-1333.4526367188,29.331750869751},
  {"Transporte de Valores",232.78034973145,117.23257446289,102.60144042969},
  {"Jornalista",-543.63989257813,-894.08587646484,24.463968276978}, 
  {"Sedex",70.452079772949,117.02716064453,79.131782531738},
  {"Hospital",299.75296020508,-1442.3692626953,29.79154586792},
  {"VIP Gold",-51.264266967773,-1116.3255615234,26.434644699097},
  {"VIP Diamond",-47.662586212158,-1116.4074707031,26.434259414673},
  {"VIP Platinum",-44.892528533936,-1116.0046386719,26.43436050415},
  {"Garagem de Onibus",424.15634155273,-614.98345947266,28.499914169312},
  {"LS Customs",-337.3863,-136.9247,39.0737},
  {"LS Customs",-1155.536,-2007.183,13.244},
  {"LS Customs",731.8163,-1088.822,22.233},
  {"LS Customs",1175.04,2640.216,37.82177},
  {"LS Customs",110.8406,6626.568,32.287}
  
}

return cfg
